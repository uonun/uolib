﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace uoLib
{
    public partial class FAbout : Form
    {
        public FAbout()
        {
            InitializeComponent();

            lbVersion.Text = uoLib.Info.Version.ToString();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(uoLib.Info.HelpLink);
        }
    }
}
