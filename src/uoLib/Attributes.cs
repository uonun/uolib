﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace uoLib
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    class HelpAttribute : System.Attribute
    {
        private string _url;

        public string Url
        {
            get { return _url; }
        }
        public HelpAttribute(string url)
        {
            _url = url;
        }
    }

    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    class AuthorAttribute : System.Attribute
    {
        private string _author;
        private string _email;
        private string _homePage;

        public string Author
        {
            get { return _author; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string HomePage
        {
            get { return _homePage; }
            set { _homePage = value; }
        }

        public AuthorAttribute(string author)
        {
            _author = author;
        }
    }

    /// <summary>
    /// 标识此模块的适用范围
    /// </summary>
    /// <remarks>
    /// 这些标识用于大致上区分类、方法、属性等模块适用的项目类型。
    /// 比如<see cref="uoLib.Common.Functions.ReportMsg(string)"/>方法中使用了<see cref="AvailableTypeAttribute.Type.WebForm"/>所特有的<see cref="System.Web.HttpResponse.Write(string)"/>方法，显然这个方法无法在 <see cref="AvailableTypeAttribute.Type.WinForm"/> 中正常执行，
    /// 因此<see cref="uoLib.Common.Functions.ReportMsg(string)"/>被标识为<see cref="AvailableTypeAttribute.Type.WebForm"/>的特有方法。
    /// </remarks>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class AvailableTypeAttribute : System.Attribute
    {
        private Type _type;

        public AvailableTypeAttribute(Type type)
        {
            _type = type;
        }

        /// <summary>
        /// 模块的适用类型
        /// </summary>
        public enum Type
        {
            /// <summary>
            /// 任意
            /// </summary>
            Any,
            /// <summary>
            /// 应使用于C/S架构的WinForm项目中。
            /// </summary>
            WinForm,
            /// <summary>
            /// 应使用于B/S架构的WebForm项目中。
            /// </summary>
            WebForm
        }
    }

}
