﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using uoLib.Data.Factories;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Diagnostics;

namespace uoLib.Data
{
    /// <summary>
    /// 数据库对象基类。可继承使用。
    /// </summary>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public abstract partial class BaseProvider : IBaseProvider, IConnectionFactory
    {
        /// <summary>
        /// 数据库连接字符串的名称
        /// </summary>
        protected string connStringName;

        internal BaseProvider(ConnectionType connType)
        {
            this.connType = connType;
            this.InitConnFactory(connType);
        }

        #region 属性
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }
        private string _connectionString;

        #endregion

        #region 公共方法
        /// <summary>
        /// 执行DbCommand.ExecuteNonQuery()。
        /// </summary>
        /// <param name="cmdStr">要执行的 SQL 语句</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteScalar"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// </remarks>
        public int ExecuteNonQuery(string cmdStr)
        {
            DbCommand cmd = this.CreateCommand(cmdStr);
            return ExecuteNonQuery(cmd);
        }
        /// <summary>
        /// 执行DbCommand.ExecuteNonQuery()。
        /// </summary>
        /// <param name="cmdStr">要执行的 SQL 语句</param>
        /// <param name="args">用于参与string.Format(cmdStr,args)的参数</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteScalar"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// </remarks>
        public int ExecuteNonQuery(string cmdStr, params object[] args)
        {
            string sql = string.Format(cmdStr, args);
            return ExecuteNonQuery(sql);
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="sqlStrs">多条SQL语句</param>
        /// <remarks>sqlStrs中的每项只能是一般SQL语句，不能为存储过程。</remarks>
        public void ExecuteWithTransaction(List<string> sqlStrs)
        {
            if (sqlStrs == null) throw new ArgumentNullException("sqlStrs");
            if (sqlStrs.Count < 1) throw new ArgumentException("没有指定要执行的SQL语句！", "sqlStrs");

            List<DbCommand> cmds = new List<DbCommand>();
            foreach (string s in sqlStrs)
            {
                cmds.Add(this.CreateCommand(s));
            }
            ExecuteWithTransaction(cmds);
        }

        /// <summary>
        /// 执行多个SQL命令，实现数据库事务。
        /// </summary>
        /// <param name="cmds">多个SQL命令</param>
        /// <remarks>cmds中的所有命令必须使用相同的DbConnection</remarks>
        /// <exception cref="ArgumentNullException">参数cmds为空引用</exception>
        /// <exception cref="ArgumentException">参数cmds中不包含任何可用的DbCommand</exception>
        /// <exception cref="ArgumentException">参数cmds为中的所有DbCommand必须指定同一个数据库连接实例（DbConnection）</exception>
        public void ExecuteWithTransaction(List<DbCommand> cmds)
        {
            if (cmds == null) throw new ArgumentNullException("cmds");
            if (cmds.Count < 1) throw new ArgumentException("参数cmds中不包含任何可用的DbCommand", "cmds");
            DbConnection conn = cmds[0].Connection;
            foreach (DbCommand c in cmds)
            {
                if (!c.Connection.Equals(conn))
                {
                    throw new ArgumentException("参数cmds为中的所有DbCommand必须指定同一个数据库连接实例（DbConnection）", "cmds");
                }
            }

            DbTransaction tr = null;
            try
            {
                conn.Open();
                tr = conn.BeginTransaction();
                foreach (DbCommand c in cmds)
                {
                    c.Connection = conn;
                    c.Transaction = tr;
                    c.ExecuteNonQuery();
                }
                tr.Commit();
            }
            catch
            {
                if (tr != null) tr.Rollback();
                throw;
            }
            finally
            {
                if (conn != null) conn.Close();
                conn.Dispose();
            }
        }

        /// <summary>
        /// 测试数据库连接是否可用
        /// </summary>
        /// <returns></returns>
        public bool TestConnection()
        {
            DbConnection conn = CreateConnection(this._connectionString);
            try
            {
                conn.Open();
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 对指定表执行 UPDATE [<paramref name="tableName"/>] 操作，返回受影响的行数
        /// </summary>
        /// <param name="tableName">要操作的表名</param>
        /// <param name="columns">要操作的数据列及其值</param>
        /// <param name="where">执行 UPDATE 操作时的筛选条件，如：“ Flag=1 AND ClassID=2 ”</param>
        /// <returns>返回受影响的行数</returns>
        public int Update(string tableName, DbColumn[] columns, string where)
        {
            if (columns == null) throw new ArgumentNullException("columns");
            if (columns.Length < 1) throw new ArgumentException("columns");

            string sql = string.Format("UPDATE [{0}] SET ", tableName);
            DbParameter[] paras = new DbParameter[columns.Length];

            for (int i = 0;i < columns.Length;i++)
            {
                sql += string.Format(" [{0}]=@{0} ", columns[i].ColumnName);
                if (i < columns.Length - 1) sql += ",";

                paras[i] = CreateParameter(string.Format("@{0}", columns[i].ColumnName), columns[i].DbType, columns[i].Value);
            }

            if (!string.IsNullOrEmpty(where))
            {
                sql = sql + " WHERE " + where;
            }

            DbCommand cmd = CreateCommand(sql);
            cmd.Parameters.AddRange(paras);

            try
            {
                int effected = ExecuteNonQuery(cmd);
                return effected;
            }
            catch (Exception ex)
            {
                throw new Exception("执行UPDATE操作失败！当前执行的SQL语句是：" + sql, ex);
            }
        }

        /// <summary>
        /// [MS SqlServer Only!]对指定表进行 INSERT INTP [<paramref name="tableName"/>] 操作，返回新生成的主键值。
        /// </summary>
        /// <param name="tableName">要操作的表名</param>
        /// <param name="columns">要操作的数据列及其值</param>
        /// <param name="newPrimaryValue">新生成的主键值</param>
        /// <returns>操作成功返回true，否则返回false</returns>
        /// <remarks>
        /// <code>
        /// <![CDATA[
        /// Database db = new Database(ConnectionType.SqlClient);
        /// db.ConnectionString = "YOURCONNSTRING";
        ///             
        /// string varString = "var enabled.";
        /// DbColumn[] cols = new DbColumn[]{
        ///     // 构造你要填写的字段。值可以是各种类型的变量/常量。
        ///     new DbColumn(){ ColumnName="col1", DbType= System.Data.DbType.String, Value=varString},
        ///     new DbColumn(){ ColumnName="col2", DbType= System.Data.DbType.Int32, Value=10},
        ///     new DbColumn(){ ColumnName="col3", DbType= System.Data.DbType.Boolean, Value=true},
        ///     // and other columns.
        /// };
        /// 
        /// int newID;
        /// bool isOk = db.Insert("TABLENAME", cols, out newID);
        /// if (isOk)
        /// {
        ///     Response.Write(string.Format("Insert 执行成功，新主键值 = {0}", newID));
        /// }
        /// else
        /// {
        ///     Response.Write("Insert 失败，你可以从异常信息中找到实际执行的Insert语句，从而判断失败原因。");
        /// }
        ///     
        /// //类似的方法，可以对行执行Update操作
        /// int n = db.Update("TABLENAME", cols, " ID=1 ");
        /// ]]></code>
        /// </remarks>
        public bool Insert(string tableName, DbColumn[] columns, out int newPrimaryValue)
        {
            if (columns == null) throw new ArgumentNullException("columns");
            if (columns.Length < 1) throw new ArgumentException("columns");

            string valueName = "", valueObj = "";
            DbParameter[] paras = new DbParameter[columns.Length];

            for (int i = 0;i < columns.Length;i++)
            {
                valueName += string.Format("[{0}]", columns[i].ColumnName);
                valueObj += string.Format("@{0}", columns[i].ColumnName);

                if (i < columns.Length - 1)
                {
                    valueName += ",";
                    valueObj += ",";
                }

                paras[i] = CreateParameter(string.Format("@{0}", columns[i].ColumnName), columns[i].DbType, columns[i].Value);
            }

            string sql = string.Format("INSERT INTO [{0}] ({1}) VALUES ({2});SELECT SCOPE_IDENTITY() AS newID", tableName, valueName, valueObj);

            DbCommand cmd = CreateCommand(sql);
            cmd.Parameters.AddRange(paras);

            try
            {
                object newid = ExecuteScalar(cmd);
                bool isOk = int.TryParse(newid.ToString(), out newPrimaryValue);
                return isOk;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}\r\n\r\nuoLib提示：执行 INSERT 操作失败，请检查 columns 参数的初始化值是否能正常 Insert 到 MS SQL Server！如：数值列的值是否能正常 Parse 为数值并且未超出允许的值范围；布尔列的值是否能正常 Parse 为 MS SQL Server 的布尔值；数据库中不能为 NULL 的列，其值是否为空引用等等。尤其请注意 MS SQL Server 中，System.Data.DbType.DateTime 类型的数据列允许的最大/最小值范围与 C# 中 DateTime 类型所允许的范围并不一致！\r\n\r\n当前执行的 SQL 语句是：{1}", ex.Message, sql));
            }
        }

        #endregion

        /// <summary>
        /// SQL命令执行后的回调
        /// </summary>
        /// <param name="sender">当前数据库实例</param>
        /// <param name="e">回调事件参数</param>
        public delegate void CommandExecuedCallback(object sender, ExecuteEventArgs e);

        #region IBaseProvider 成员
        /// <summary>
        /// 数据库连接所使用的驱动程序类型。
        /// </summary>
        public ConnectionType ConnectionType
        {
            get { return this.connType; }
            set { this.connType = value; }
        }

        /// <summary>
        /// 为数据库实例创建数据连接
        /// </summary>
        /// <remarks>调用此方法之后由此数据库实例所创建的DbCommand、DbDataAdapter、DbDataReader等对象将自动使用此连接。</remarks>
        public DbConnection CreateConnection(string connStr)
        {
            _connectionString = connStr;

            DbConnection _conn = connFactroy.CreateConnection();
            _conn.ConnectionString = connStr;
            return _conn;
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbCommand对象
        /// </summary>
        /// <remarks>
        /// 所创建的DbCommand对象将自动使用数据库实例已有的连接。
        /// </remarks>
        /// <example>
        /// <code>
        /// Database db = new Database();
        /// DbCommand cmd = db.CreateCommand("SELECT * FROM [Table]");</code>
        /// </example>
        public DbCommand CreateCommand(string sqlStrTxt)
        {
            DbCommand cmd = CreateCommand();
            cmd.CommandText = sqlStrTxt;
            cmd.Connection = CreateConnection(_connectionString);
            return cmd;
        }
        /// <summary>
        /// 创建一个System.Data.Common.DbCommand对象
        /// </summary>
        /// <param name="sqlStrTxt">SQL语句</param>
        /// <param name="args">用于参与string.Format(sqlStrTxt,args)的参数</param>
        /// <remarks>
        /// 所创建的DbCommand对象将自动使用数据库实例已有的连接。
        /// </remarks>
        /// <example>
        /// <code>
        /// string tableName = "Table";
        /// Database db = new Database();
        /// DbCommand cmd = db.CreateCommand("SELECT * FROM [{0}]",tableName);</code>
        /// </example>
        public DbCommand CreateCommand(string sqlStrTxt, params object[] args)
        {
            return CreateCommand(string.Format(sqlStrTxt, args));
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbCommand对象
        /// </summary>
        /// <param name="sqlStrTxt">SQL语句</param>
        /// <param name="parameters">参数数组</param>
        /// <remarks>
        /// 所创建的DbCommand对象将自动使用数据库实例已有的连接。
        /// </remarks>
        /// <example>
        /// <code>
        /// Database db = new Database();
        /// string sql = "SELECT * FROM [MYTABLE] WHERE Hit>@hit AND WriteDate>@date";
        /// DbParameter[] paras = {
        ///        db.CreateParameter("@hit", DbType.Int32,100)
        ///        ,db.CreateParameter("@date", DbType.DateTime,DateTime.Parse("2009-8-3"))
        /// };
        /// DbCommand cmd = db.CreateCommand(sql,paras);</code>
        /// </example>
        /// <returns></returns>
        public DbCommand CreateCommand(string sqlStrTxt, DbParameter[] parameters)
        {
            DbCommand cmd = CreateCommand(sqlStrTxt);
            cmd.Parameters.AddRange(parameters);
            return cmd;
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbDataAdapter对象
        /// </summary>
        /// <param name="sqlStrTxt">SQL语句</param>
        /// <returns></returns>
        public DbDataAdapter CreateDataAdapter(string sqlStrTxt)
        {
            DbCommand cmd = CreateCommand(sqlStrTxt);
            return CreateDataAdapter(cmd);
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbDataReader对象
        /// </summary>
        /// <param name="sqlStrTxt">SQL语句</param>
        /// <returns></returns>
        public DbDataReader CreateDataReader(string sqlStrTxt)
        {
            DbCommand cmd = CreateCommand(sqlStrTxt);
            return CreateDataReader(cmd);
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbCreateParameter对象
        /// </summary>
        /// <param name="name">参数名称</param>
        /// <param name="type">参数类型e.g.DbType.Int32, DbType.String</param>
        /// <param name="value">参数值</param>
        /// <returns></returns>
        public DbParameter CreateParameter(string name, DbType type, object value)
        {
            DbParameter para = CreateParameter();
            para.ParameterName = name;
            para.DbType = type;
            para.Value = value;
            return para;
        }
        /// <summary>
        /// 生成一个DbParameter参数实例
        /// </summary>
        /// <param name="name">参数名</param>
        /// <param name="type">参数类型。e.g.DbType.Int32, DbType.String</param>
        /// <param name="parameterSize">获取或设置列中数据的最大大小（以字节为单位）。祥见DbParameter.Size</param>
        /// <returns></returns>
        public DbParameter CreateOutPutParameter(string name, DbType type, int parameterSize)
        {
            DbParameter p = CreateParameter();
            p.ParameterName = name;
            p.DbType = type;
            p.Size = parameterSize;
            p.Direction = ParameterDirection.Output;
            return p;
        }

        #region ReturnValue
        /// <summary>
        /// 执行 ExecuteScalar 查询，返回首行首列的值。若查询结果为空，则返回类型 <typeparamref name="T"/> 的默认值。
        /// </summary>
        /// <typeparam name="T">要返回的类型</typeparam>
        /// <param name="selectCommand">将用于执行 ExecuteScalar 操作的 SQL 语句。例如：SELECT TOP 1 [字段名] FROM [表名]</param>
        /// <returns></returns>
        public T ReturnValue<T>(string selectCommand)
        {
            DbCommand cmd = CreateCommand(selectCommand);
            return ReturnValue<T>(cmd);
        }

        /// <summary>
        /// 执行查询，返回首行 <paramref name="columeIndex"/> 列的值。若查询结果为空，则返回类型 <typeparamref name="T"/> 的默认值。
        /// </summary>
        /// <typeparam name="T">要返回的类型</typeparam>
        /// <param name="selectCommand">将用于执行 ExecuteScalar 操作的 SQL 语句。例如：SELECT TOP 1 [字段名1],[字段名2] FROM [表名]</param>
        /// <param name="columeIndex">列索引，指定要返回哪一列的数据。</param>
        /// <returns></returns>
        public T ReturnValue<T>(string selectCommand, int columeIndex)
        {
            if (columeIndex < 0) throw new ArgumentOutOfRangeException("columeIndex", "列索引必须为非负整数！");

            DbCommand cmd = CreateCommand(selectCommand);
            return ReturnValue<T>(cmd, columeIndex);
        }

        /// <summary>
        /// 执行查询，返回首行 <paramref name="columeName"/> 列的值。若查询结果为空，则返回类型 <typeparamref name="T"/> 的默认值。
        /// </summary>
        /// <typeparam name="T">要返回的类型</typeparam>
        /// <param name="selectCommand">将用于执行 ExecuteScalar 操作的 SQL 语句。例如：SELECT TOP 1 [字段名1],[字段名2] FROM [表名]</param>
        /// <param name="columeName">列名，指定要返回哪一列的数据。</param>
        /// <returns></returns>
        public T ReturnValue<T>(string selectCommand, string columeName)
        {
            if (string.IsNullOrEmpty(columeName)) throw new ArgumentException("columeName", "列名未指定！");

            DbCommand cmd = CreateCommand(selectCommand);
            return ReturnValue<T>(cmd, columeName);
        }

        /// <summary>
        /// 执行 ExecuteScalar 查询，返回首行首列的值。若查询结果为空，则返回类型 <typeparamref name="T"/> 的默认值。
        /// </summary>
        /// <typeparam name="T">要返回的类型</typeparam>
        /// <param name="selectCommand">将用于执行 ExecuteScalar 操作的 SQL 命令。例如：SELECT TOP 1 [字段名] FROM [表名]</param>
        /// <returns></returns>
        public T ReturnValue<T>(DbCommand selectCommand)
        {
            object tmp = ExecuteScalar(selectCommand, null);
            if (tmp == null) return default(T);
            try
            {
                T result = (T)tmp;
                return result;
            }
            catch (System.InvalidCastException ex)
            {
                throw new InvalidCastException("已查询到数据，但数据无法转换为指定的类型 T ：" + ex.Message, ex);
            }
        }

        /// <summary>
        /// 执行查询，返回首行 <paramref name="columeIndex"/> 列的值。若查询结果为空，则返回类型 <typeparamref name="T"/> 的默认值。
        /// </summary>
        /// <typeparam name="T">要返回的类型</typeparam>
        /// <param name="selectCommand">将用于执行 ExecuteScalar 操作的 SQL 命令。例如：SELECT TOP 1 [字段名1],[字段名2] FROM [表名]</param>
        /// <param name="columeIndex">列索引，指定要返回哪一列的数据。</param>
        /// <returns></returns>
        public T ReturnValue<T>(DbCommand selectCommand, int columeIndex)
        {
            if (columeIndex < 0) throw new ArgumentOutOfRangeException("columeIndex", "列索引必须为非负整数！");

            object tmp = null;
            using (selectCommand.Connection)
            {
                selectCommand.Connection.Open();
                using (DbDataReader dr = CreateDataReader(selectCommand, CommandBehavior.CloseConnection | CommandBehavior.SingleRow))
                {
                    if (dr.HasRows)
                    {
                        if (columeIndex > dr.FieldCount - 1)
                        {
                            throw new ArgumentOutOfRangeException("columeIndex", "列索引过大，返回的数据行列数不够");
                        }
                        else
                        {
                            while (dr.Read())
                            {
                                tmp = dr[columeIndex];
                            }
                        }
                    }
                }
            }

            if (tmp == null) return default(T);
            try
            {
                T result = (T)tmp;
                return result;
            }
            catch (System.InvalidCastException ex)
            {
                throw new InvalidCastException("已查询到数据，但数据无法转换为指定的类型 T ：" + ex.Message, ex);
            }
        }

        /// <summary>
        /// 执行查询，返回首行 <paramref name="columeName"/> 列的值。若查询结果为空，则返回类型 <typeparamref name="T"/> 的默认值。
        /// </summary>
        /// <typeparam name="T">要返回的类型</typeparam>
        /// <param name="selectCommand">将用于执行 ExecuteScalar 操作的 SQL 命令。例如：SELECT TOP 1 [字段名1],[字段名2] FROM [表名]</param>
        /// <param name="columeName">列名，指定要返回哪一列的数据。</param>
        /// <returns></returns>
        public T ReturnValue<T>(DbCommand selectCommand, string columeName)
        {
            if (string.IsNullOrEmpty(columeName)) throw new ArgumentException("columeName", "列名未指定！");

            object tmp = null;
            using (selectCommand.Connection)
            {
                selectCommand.Connection.Open();
                using (DbDataReader dr = CreateDataReader(selectCommand, CommandBehavior.CloseConnection | CommandBehavior.SingleRow))
                {
                    if (dr.HasRows)
                    {
                        try
                        {
                            while (dr.Read())
                            {
                                tmp = dr[columeName];
                            }
                        }
                        catch (System.IndexOutOfRangeException ex)
                        {
                            throw new IndexOutOfRangeException("指定的列明无效！", ex);
                        }
                    }
                }
            }

            if (tmp == null) return default(T);
            try
            {
                T result = (T)tmp;
                return result;
            }
            catch (System.InvalidCastException ex)
            {
                throw new InvalidCastException("已查询到数据，但数据无法转换为指定的类型 T ：" + ex.Message, ex);
            }
        }
        #endregion

        /// <summary>
        /// 执行 SQL 语句返回数据集。
        /// </summary>
        /// <param name="sqlStrTxt">要执行的SQL语句</param>
        /// <returns></returns>
        /// <remarks>
        /// <para>用于只产生一个DataTable的单一SELECT语句，当且仅当返回的DataSet有数据行时才返回数据集，否则返回 null。</para>
        /// </remarks>
        public DataSet SelectData(string sqlStrTxt)
        {
            DbCommand cmd = CreateCommand(sqlStrTxt);
            return SelectData(cmd);
        }

        /// <summary>
        /// 执行 SQL 语句返回数据集。
        /// </summary>
        /// <param name="sqlStrTxt">要执行的SQL语句</param>
        /// <param name="args">用于参与string.Format(sqlStrTxt,args)的参数</param>
        /// <returns></returns>
        /// <remarks>
        /// <para>用于只产生一个DataTable的单一SELECT语句，当且仅当返回的DataSet有数据行时才返回数据集，否则返回 null。</para>
        /// </remarks>
        public DataSet SelectData(string sqlStrTxt, params object[] args)
        {
            DbCommand cmd = CreateCommand(string.Format(sqlStrTxt, args));
            return SelectData(cmd);
        }

        /// <summary>
        /// 执行 SQL 语句返回数据集。
        /// </summary>
        /// <param name="sqlStrTxt">要执行的SQL语句</param>
        /// <param name="paras">SQL命令的参数</param>
        /// <returns></returns>
        public DataSet SelectData(string sqlStrTxt, DbParameter[] paras)
        {
            DbCommand selectCommand = CreateCommand(sqlStrTxt);
            selectCommand.Parameters.AddRange(paras);
            return SelectData(selectCommand);
        }

        /// <summary>
        /// 执行数据库查询并返回数据集。
        /// </summary>
        /// <param name="selectCommand">要执行的数据库查询</param>
        /// <returns></returns>
        /// <remarks>
        /// 说明见：<see cref="uoLib.Data.BaseProvider.SelectData(System.Data.Common.DbCommand, uoLib.Data.BaseProvider.CommandExecuedCallback)"/>
        /// </remarks>
        public DataSet SelectData(DbCommand selectCommand)
        {
            return SelectData(selectCommand, null);
        }

        /// <summary>
        /// 执行数据库查询并返回数据集。
        /// </summary>
        /// <param name="selectCommand">要执行的数据库查询</param>
        /// <param name="OnCommandExecuted">执行数据库查询后的回调函数</param>
        /// <returns></returns>
        /// <remarks>
        /// <para>用于只产生一个DataTable的单一SELECT语句，当且仅当返回的DataSet有数据行时才返回数据集，否则返回 null。</para>
        /// <para><strong>注意：执行查询过程中如果产生异常，</strong>当<paramref name="OnCommandExecuted"/>为null时，异常会直接被抛出，
        /// 否则会通过回调事件参数的属性<see cref="uoLib.Data.ExecuteEventArgs.Errors"/>传出。</para>
        /// </remarks>
        public DataSet SelectData(DbCommand selectCommand, CommandExecuedCallback OnCommandExecuted)
        {
            if (selectCommand == null) throw new ArgumentNullException("selectCommand");

            using (DbDataAdapter adp = CreateDataAdapter(selectCommand))
            {
                DataSet ds = new DataSet();
                if (selectCommand.Connection.State != ConnectionState.Closed) { selectCommand.Connection.Close(); }

                //锁定以确保_conn.State的状态安全
                object obj = new object();

                int effectrows = 0;
                if (OnCommandExecuted != null)
                {
                    ExecuteEventArgs e = null;
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    try
                    {
                        lock (obj) { effectrows = adp.Fill(ds); }
                        sw.Stop();
                        e = new ExecuteEventArgs()
                        {
                            Command = selectCommand,
                            ElapsedMilliseconds = sw.ElapsedMilliseconds,
                            Errors = null,
                            RecordsAffected = effectrows
                        };
                        OnCommandExecuted(this, e);
                    }
                    catch (Exception ex)
                    {
                        sw.Stop();
                        e = new ExecuteEventArgs()
                        {
                            Command = selectCommand,
                            ElapsedMilliseconds = sw.ElapsedMilliseconds,
                            Errors = ex,
                            RecordsAffected = effectrows
                        };
                        OnCommandExecuted(this, e);
                    }
                    sw = null;
                }
                else
                {
                    lock (obj) { effectrows = adp.Fill(ds); }
                }

                if (effectrows == 0) return null;
                return ds;
            }
        }

        /// <summary>
        /// 执行DbCommand.ExecuteNonQuery()。
        /// </summary>
        /// <param name="cmd">要执行的数据库查询</param>
        /// <param name="OnCommandExecuted">执行数据库查询后的回调函数</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteNonQuery"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// <para><strong>注意：执行查询过程中如果产生异常，</strong>当<paramref name="OnCommandExecuted"/>为null时，异常会直接被抛出，
        /// 否则会通过回调事件参数的属性<see cref="uoLib.Data.ExecuteEventArgs.Errors"/>传出。</para>
        /// </remarks>
        public int ExecuteNonQuery(DbCommand cmd, CommandExecuedCallback OnCommandExecuted)
        {
            if (cmd == null) throw new ArgumentNullException("cmd");

            if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Close(); cmd.Connection.Open(); }

            int effectrows = 0;
            if (OnCommandExecuted != null)
            {
                ExecuteEventArgs e = null;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    effectrows = cmd.ExecuteNonQuery();
                    sw.Stop();
                    e = new ExecuteEventArgs()
                    {
                        Command = cmd,
                        ElapsedMilliseconds = sw.ElapsedMilliseconds,
                        Errors = null,
                        RecordsAffected = effectrows
                    };
                    OnCommandExecuted(this, e);
                }
                catch (Exception ex)
                {
                    sw.Stop();
                    e = new ExecuteEventArgs()
                    {
                        Command = cmd,
                        ElapsedMilliseconds = sw.ElapsedMilliseconds,
                        Errors = ex,
                        RecordsAffected = effectrows
                    };
                    OnCommandExecuted(this, e);
                }
                finally
                {
                    cmd.Connection.Close();
                    sw = null;
                }
            }
            else
            {
                effectrows = cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }

            return effectrows;
        }
        /// <summary>
        /// 执行数据库查询并返回数据集。
        /// </summary>
        /// <param name="cmd">要执行的数据库查询</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteNonQuery"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// 说明见：<see cref="uoLib.Data.BaseProvider.ExecuteNonQuery(System.Data.Common.DbCommand, uoLib.Data.BaseProvider.CommandExecuedCallback)"/>
        /// </remarks>
        public int ExecuteNonQuery(DbCommand cmd)
        {
            return ExecuteNonQuery(cmd, null);
        }

        /// <summary>
        /// 执行DbCommand.ExecuteScalar()。建议直接使用 <see cref="uoLib.Data.BaseProvider.ReturnValue&lt;T&gt;(string)"/>
        /// </summary>
        /// <param name="cmd">要执行的 SQL 命令</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteScalar"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// 说明见：<see cref="uoLib.Data.BaseProvider.ExecuteScalar(System.Data.Common.DbCommand, uoLib.Data.BaseProvider.CommandExecuedCallback)"/>
        /// </remarks>
        public object ExecuteScalar(DbCommand cmd)
        {
            return ExecuteScalar(cmd, null);
        }
        /// <summary>
        /// 执行DbCommand.ExecuteScalar()。
        /// </summary>
        /// <param name="cmd">要执行的 SQL 命令</param>
        /// <param name="OnCommandExecuted">执行数据库查询后的回调函数</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteScalar"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// <para><strong>注意：执行查询过程中如果产生异常，</strong>当<paramref name="OnCommandExecuted"/>为null时，异常会直接被抛出，
        /// 否则会通过回调事件参数的属性<see cref="uoLib.Data.ExecuteEventArgs.Errors"/>传出。</para>
        /// </remarks>
        public object ExecuteScalar(DbCommand cmd, CommandExecuedCallback OnCommandExecuted)
        {
            if (cmd == null) throw new ArgumentNullException("cmd");

            if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Close(); cmd.Connection.Open(); }

            object result = null;
            if (OnCommandExecuted != null)
            {
                ExecuteEventArgs e = null;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                try
                {
                    result = cmd.ExecuteScalar();
                    sw.Stop();
                    e = new ExecuteEventArgs()
                    {
                        Command = cmd,
                        ElapsedMilliseconds = sw.ElapsedMilliseconds,
                        Errors = null,
                        RecordsAffected = result.Equals(null) ? 0 : 1
                    };
                    OnCommandExecuted(this, e);
                }
                catch (Exception ex)
                {
                    sw.Stop();
                    e = new ExecuteEventArgs()
                    {
                        Command = cmd,
                        ElapsedMilliseconds = sw.ElapsedMilliseconds,
                        Errors = ex,
                        RecordsAffected = 0
                    };
                    OnCommandExecuted(this, e);
                }
                finally
                {
                    cmd.Connection.Close();
                    sw = null;
                }
            }
            else
            {
                result = cmd.ExecuteScalar();
                cmd.Connection.Close();
            }

            return result;
        }
        #endregion
        #region IConnectionFactory 成员

        /// <summary>
        /// 创建一个System.Data.Common.DbCommand对象
        /// </summary>
        /// <remarks>所创建的DbCommand对象将自动使用数据库实例已有的连接。</remarks>
        public DbCommand CreateCommand()
        {
            DbCommand cmd = connFactroy.CreateCommand();
            cmd.Connection = this.CreateConnection(_connectionString);
            return cmd;
        }

        /// <summary>
        /// 为数据库实例创建数据连接
        /// </summary>
        /// <remarks>调用此方法之后由此数据库实例所创建的DbCommand、DbDataAdapter、DbDataReader等对象将自动使用此连接。</remarks>
        public DbConnection CreateConnection()
        {
            //if (_conn == null) _conn = connFactroy.CreateConnection();

            //永远都新构造一个链接，以免出现“阅读器关闭时尝试调用Read无效”的异常。
            //异常产生原因：多个地方的DbDataReader同时使用了同一个连接，而其中一个DbDataReader关闭连接将导致其他DbDataReader的连接也被关闭。

            DbConnection conn = connFactroy.CreateConnection();
            conn.ConnectionString = this._connectionString;
            return conn;
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbDataAdapter对象
        /// </summary>
        /// <returns></returns>
        public DbDataAdapter CreateDataAdapter()
        {
            return connFactroy.CreateDataAdapter();
        }
        /// <summary>
        /// 创建一个System.Data.Common.DbDataAdapter对象
        /// </summary>
        /// <returns></returns>
        public DbDataAdapter CreateDataAdapter(DbCommand cmd)
        {
            return connFactroy.CreateDataAdapter(cmd);
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbDataReader对象
        /// </summary>
        /// <returns></returns>
        public DbDataReader CreateDataReader(DbCommand cmd)
        {
            if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Close(); cmd.Connection.Open(); }
            return connFactroy.CreateDataReader(cmd);
        }
        /// <summary>
        /// 创建一个System.Data.Common.DbDataReader对象
        /// </summary>
        /// <returns></returns>
        public DbDataReader CreateDataReader(DbCommand cmd, CommandBehavior behavior)
        {
            return connFactroy.CreateDataReader(cmd, behavior);
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbCommandBuilder对象
        /// </summary>
        /// <returns></returns>
        public DbCommandBuilder CreateCommandBuilder()
        {
            return connFactroy.CreateCommandBuilder();
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbConnectionStringBuilder对象
        /// </summary>
        /// <returns></returns>
        public DbConnectionStringBuilder CreateConnectionStringBuilder()
        {
            return connFactroy.CreateConnectionStringBuilder();
        }

        /// <summary>
        /// 创建一个System.Data.Common.DbParameter对象
        /// </summary>
        /// <returns></returns>
        public DbParameter CreateParameter()
        {
            return connFactroy.CreateParameter();
        }

        #endregion

        #region 私有成员
        private IConnectionFactory connFactroy;
        private ConnectionType connType = ConnectionType.Unsupported;
        private void InitConnFactory(ConnectionType connType)
        {
            switch (connType)
            {
                case ConnectionType.SqlClient:
                    connFactroy = new SqlClientConnFactory(); break;
                case ConnectionType.OleDb:
                    connFactroy = new OleDbConnFactory(); break;
                case ConnectionType.Odbc:
                    connFactroy = new OdbcConnFactory(); break;
                case ConnectionType.OracleClient:
                    connFactroy = new OracleClientConnFactory(); break;
                case ConnectionType.Unsupported:
                default:
                    throw new Exceptions.NotSupportedConnException(this.connType);
            }
        }
        #endregion
    }

    /// <summary>
    /// 查询执行后的回调参数
    /// </summary>
    public class ExecuteEventArgs : EventArgs
    {
        /// <summary>
        /// 当前执行的查询
        /// </summary>
        public DbCommand Command;
        /// <summary>
        /// 当前查询所耗时间，毫秒。
        /// </summary>
        public long ElapsedMilliseconds;
        /// <summary>
        /// 当前查询影响的行数
        /// </summary>
        public int RecordsAffected;
        /// <summary>
        /// 当前查询执行过程中产生的异常
        /// </summary>
        public Exception Errors;
    }

    /// <summary>
    /// 执行 INSERT、UPDATE 操作时所涉及的字段列信息
    /// </summary>
    public struct DbColumn
    {
        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName;

        /// <summary>
        /// 列的数据类型
        /// </summary>
        public DbType DbType;

        /// <summary>
        /// 列的值
        /// </summary>
        public object Value;
    }
}
