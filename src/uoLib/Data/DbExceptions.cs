/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using uoLib.Data;
//using uoLib.Data.Advanced;

namespace uoLib.Exceptions
{
    /// <summary>
    /// 不支持的连接方式
    /// </summary>
    public class NotSupportedConnException : Exception
    {
        /// <summary>
        /// 不支持的连接方式
        /// </summary>
        public NotSupportedConnException(ConnectionType connType)
            : base(string.Format(@"不支持的连接方式：{0}。[支持：SqlClient,OleDb,Odbc,OracleClient]", connType))
        {
            this.Source = "uoLib.Data.ConnectionType";
        }
        /// <summary>
        /// 不支持的连接方式
        /// </summary>
        public NotSupportedConnException(DatabaseType dbType, ConnectionType connType)
            : base(string.Format(@"{0} 不支持以 {1} 的方式连接。[支持：{2}]", dbType, connType,Database.GetSupportedConnType(dbType)))
        {
            this.Source = "uoLib.Data.ConnectionType";
        }
    }

    /// <summary>
    /// 不支持的数据库类型
    /// </summary>
    public class NotSupportedDataTypeException : Exception
    {
        public NotSupportedDataTypeException(DatabaseType dbType)
            : base(string.Format(@"不支持的数据库类型：{0}。[支持：SQL,SQLite,Access,Access2007,Excel,Excel2007,DSN,Oracle]", dbType))
        {
            this.Source = "uoLib.Data.DatabaseType";
        }
    }

    /// <summary>
    /// 未找到与指定名称匹配的连接字符串
    /// </summary>
    public class NullConnectionStringException : Exception
    {
        public NullConnectionStringException(string connStrName, string errorSource)
            : base(string.Format("请正确设置<connectionStrings>配置节！未找到与名称“{0}”匹配的连接字符串。", connStrName))
        {
            this.Source = errorSource;
        }
    }

    /// <summary>
    /// 指定数据库不存在
    /// </summary>
    public class DatabaseNotExistException : Exception
    {
        public DatabaseNotExistException(string dbName)
            : base(string.Format("指定数据库（{0}）不存在。", dbName))
        {
            this.Source = "uoLib.Data.Singleton.SqlClientSqlServer";
        }
    }
}
