﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/


using System.Configuration;
using System;
using uoLib.Exceptions;
namespace uoLib.Data.Singleton
{
    /// <summary>
    /// 使用 OleDb 链接字符串的 Access 数据库（97/2000/2007均可）。
    /// 链接字符串配置名称：uoLib.OleDbAccess
    /// </summary>
    /// <remarks>
    /// 详细说明请参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer"/>
    /// </remarks>
    /// <seealso cref="SqlClientSqlServer"/>
    /// <seealso cref="OdbcSQLite"/>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public sealed class OleDbAccess : BaseProvider
    {
        #region 单件构造
        private static OleDbAccess db;
        private static readonly object padlock = new object();

        private OleDbAccess()
            : base(ConnectionType.OleDb)
        {
            this.ConnectionType = ConnectionType.OleDb;
            this.connStringName = "uoLib.OleDbAccess";
        }

        //单件模式的 N 种构造方式，参考：http://terrylee.cnblogs.com/archive/2005/12/09/293509.html
        /// <summary>
        /// 返回一个数据库实例（单件模式）
        /// </summary>
        /// <remarks>必须在配置文件中设置名称为“uoLib.OleDbAccess”的数据库连接字符串</remarks>
        public static OleDbAccess Instance
        {
            get
            {
                //确保不是每次访问都加锁
                if (db == null)
                {
                    //加锁以确保线程安全
                    lock (padlock)
                    {
                        if (db == null)
                        {
                            db = new OleDbAccess();
                            try
                            {
                                db.ConnectionString = ConfigurationManager.ConnectionStrings[db.connStringName].ConnectionString;
                            }
                            catch (System.NullReferenceException)
                            {
                                throw new NullConnectionStringException(db.connStringName, db.ToString());
                            }
                            catch
                            {
                                throw;
                            }
                        }
                        return db;
                    }
                }
                return db;
            }
        }
        #endregion

        #region 生成连接字符串
        public static string CreateExampleConnectionString(string accessFilePathAndName, string user, string password)
        {
            return Database.CreateExampleConnectionString(accessFilePathAndName, user, password, DatabaseType.Access, ConnectionType.OleDb);
        }

        public static string GetSupportedConnType()
        {
            return Database.GetSupportedConnType(DatabaseType.Access);
        }
        #endregion
    }
}
