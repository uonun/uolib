﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

//#define SQL2000

using System;
using System.Configuration;
using uoLib.Exceptions;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using uoLib.Common;
using System.Data.Common;

namespace uoLib.Data.Singleton
{
    /// <summary>
    /// 使用 SqlClient 链接字符串的 Sql Server 数据库（2000/2005/2008均可）。
    /// 链接字符串配置名称：uoLib.SqlClientSqlServer
    /// </summary>
    /// <example>
    /// <p>uoLib类库在使用上极其方便。它为三种常用数据库类型分别提供了一个数据库单体实例，它们是：</p>
    /// <ul>
    /// <li>oledb连接的Access：<see cref="OleDbAccess"/></li>
    /// <li>SqlClient连接的SQL Server：<see cref="SqlClientSqlServer"/></li>
    /// <li>odbc连接的SQLite：<see cref="OdbcSQLite"/></li>
    /// </ul>
    /// <p>您可以在程序中直接使用这三个单体实例来访问相应的数据库。下面是示例代码：</p><p><strong>web.Config</strong></p>
    /// <code><![CDATA[
    /// <?xml version="1.0"?>
    /// <configuration>
    ///     <connectionStrings>
    ///         <remove name="*"/>
    ///         <add name="uoLib.SqlClientSqlServer" connectionString="Data Source=127.0.0.1;Initial Catalog=NorthWind;User Id=sa;Password=;"/>
    ///         <add name="uoLib.OleDbAccess" connectionString=""/>
    ///         <add name="uoLib.OdbcSQLite" connectionString="DRIVER=SQLite3 ODBC Driver;Database=files/demo.db3;LongNames=0;Timeout=1000;NoTXN=0;SyncPragma=NORMAL;StepAPI=0;"/>
    ///     </connectionStrings>
    /// </configuration>]]></code>
    /// <p>web.config文件中可以将数据库的连接字符串留空。如上面的配置中，可以不配置uoLib.OleDbAccess的连接字符串，因为在 cs 代码中将另行写入。详见下：</p>
    /// <code><![CDATA[
    /// using System;
    /// using System.Data;
    /// using System.Data.Common;
    /// using uoLib.Data.Singleton;
    /// namespace DemoWeb  {
    ///     public partial class Demo : System.Web.UI.Page      
    ///     {
    ///         protected void Page_Load(object sender, EventArgs e)          
    ///         {               
    ///             DataSet ds;
    ///             // 使用 SqlClient 链接字符串的 Sql Server 数据库
    ///             // 2000/2005/2008均可。
    ///             // 链接字符串配置名称：uoLib.SqlClientSqlServer
    ///             SqlClientSqlServer SQL = SqlClientSqlServer.Instance;
    ///             const string TABLENAME = "Products";
    ///             ds = SQL.SelectData("SELECT TOP 1 ID FROM {0}", TABLENAME);
    ///             if (ds == null){
    ///                 Response.Write("数据库为空！");
    ///             }else{
    ///                 Response.Write(string.Format("共 {0} 行记录。", ds.Tables[0].Rows.Count));
    ///             }
    ///             
    ///             // 使用 OleDb 链接字符串的 Access 数据库
    ///             // 97/2000/2007均可。
    ///             // 链接字符串配置名称：uoLib.OleDbAccess
    ///             OleDbAccess Access = OleDbAccess.Instance;
    ///             Access.CreateConnection(OleDbAccess.CreateExampleConnectionString(Server.MapPath("files/test.mdb"), "uonun", "udnz.com"));
    ///             Response.Write(Access.ConnectionString);
    ///             
    ///             // 使用 Odbc 链接字符串的 SQLite 数据库。
    ///             // 链接字符串配置名称：uoLib.OdbcSQLite
    ///             OdbcSQLite SQLite = OdbcSQLite.Instance;
    ///             DbCommand cmd = SQLite.CreateCommand("SELECT TOP 1 ID FROM {0}", TABLENAME);
    ///             ds = SQLite.SelectData(cmd);
    ///             if (ds == null){
    ///                 Response.Write("数据库为空！");
    ///             }else{
    ///                 Response.Write(string.Format("共 {0} 行记录。", ds.Tables[0].Rows.Count));
    ///             }
    ///         }
    ///     } 
    /// }]]></code>
    /// <p>注意：<ol>
    ///<li>要使用数据库的单体实例，必须在web.config里面配置相应名称的连接字符串节。（可以留空，但必须有这一节，如上例中的uoLib.OleDbAccess）</li>
    ///<li>由于使用了单体模式，因此在整个项目中，将只存在一个数据库实例，在简化了数据库使用的同时也提高了性能。不过如此一来，在整个系统中也将仅有一个相同数据库的实例。以<see cref="SqlClientSqlServer"/>为例，您无法使用<see cref="SqlClientSqlServer.Instance"/>来生成两个不同的数据库。（要在同一系统中使用多个相同数据库实例，请使用<see cref="Database"/>。）</li>
    ///</ol></p>
    /// </example>
    /// <seealso cref="OleDbAccess"/>
    /// <seealso cref="OdbcSQLite"/>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public sealed class SqlClientSqlServer : BaseProvider
    {
        #region 单件构造
        private static SqlClientSqlServer db;
        private static readonly object padlock = new object();

        private SqlClientSqlServer()
            : base(ConnectionType.SqlClient)
        {
            this.ConnectionType = ConnectionType.SqlClient;
            connStringName = "uoLib.SqlClientSqlServer";
        }

        //单件模式的 N 种构造方式，参考：http://terrylee.cnblogs.com/archive/2005/12/09/293509.html
        /// <summary>
        /// 返回一个数据库实例（单件模式）
        /// </summary>
        /// <remarks>必须在配置文件中设置名称为“uoLib.SqlClientSqlServer”的数据库连接字符串</remarks>
        public static SqlClientSqlServer Instance
        {
            get
            {
                //确保不是每次访问都加锁
                if (db == null)
                {
                    //加锁以确保线程安全
                    lock (padlock)
                    {
                        if (db == null)
                        {
                            db = new SqlClientSqlServer();
                            try
                            {
                                db.ConnectionString = ConfigurationManager.ConnectionStrings[db.connStringName].ConnectionString;
                            }
                            catch (System.NullReferenceException)
                            {
                                throw new NullConnectionStringException(db.connStringName, db.ToString());
                            }
                            catch
                            {
                                throw;
                            }
                        }
                        return db;
                    }
                }
                return db;
            }
        }
        #endregion

        /// <summary>
        /// 判断指定数据库是否已存在
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名</param>
        /// <returns></returns>
        /// <remarks>
        /// <para><span style="color:#FF0000">注意：如下需要桥梁数据库的方法均只能用于 MSSQL 数据库。</span>
        /// <ul>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.CreateDatabase(string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.DeleteDatabase(string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.ListDatabases(string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.ListTables(string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.ListTriggers(string, string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.ListViews(string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.GetTableDetail(string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.ListColumns(string, string, string)"/></li>
        /// <li><see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsColumnExists(string, string, string, string)"/></li>
        /// </ul>
        /// </para>
        /// <para><br /></para>
        /// <para><strong>关于桥梁数据库的说明：</strong></para>
        /// <para>
        /// 以<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>为例，
        /// 比如我们要检查的目标数据库是 <strong><i>TARGET_DB</i></strong>，
        /// 则该服务器上必须还有另一个数据库可以供正常连接，并且所使用的账户必须具有 master.dbo.sysdatabases 的 select 权限。
        /// 它首先以某个身份登录数据库服务器，为“检查 <strong><i>TARGET_DB</i></strong> 是否存在”的操作提供执行条件
        /// ——这个数据库即为桥梁数据库。
        /// </para>
        /// <para>
        /// 其他说明：
        /// <ol>
        /// <li>可直接使用 master 数据库作为桥梁数据库。</li>
        /// <li>
        /// <see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>、
        /// <see cref="uoLib.Data.Singleton.SqlClientSqlServer.CreateDatabase(string, string)"/> 的桥梁数据库
        /// <strong>不能</strong>是目标数据库，而其他方法所用的桥梁数据库，可以是该服务器上的任意一个数据库，包括目标数据库。
        /// </li>
        /// <li>桥梁数据库的必要性：要实现上述方法所示的功能，必须先要连接到数据库服务器才能进行进一步查询和操作。</li>
        /// <li>桥梁数据库的可用性：桥梁数据库必须具有 master.dbo.sysdatabases 的 select 权限。</li>
        /// </ol>
        /// </para>
        /// </remarks>
        public static bool IsDatabaseExist(string connStr, string dbName)
        {
            bool Rn = false;

            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            string sql = "select 1 from master.dbo.sysdatabases where name = '{0}'";
            using (DataSet ds = tmpDb.SelectData(sql, dbName))
            {
                if (ds != null)
                {
                    Rn = true;
                }
            }
            return Rn;
        }

        /// <summary>
        /// 创建数据库
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static bool CreateDatabase(string connStr, string dbName)
        {
            bool Rn = false;

            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                tmpDb.ExecuteNonQuery("CREATE DATABASE [{0}]", dbName);
                Rn = true;
            }
            return Rn;
        }

        /// <summary>
        /// 删除数据库
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名</param>
        /// <returns></returns>
        /// <exception cref="DatabaseNotExistException">指定的数据库不存在</exception>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static bool DeleteDatabase(string connStr, string dbName)
        {
            bool Rn = false;

            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                throw new DatabaseNotExistException(dbName);
            }

            tmpDb.ExecuteNonQuery("DROP DATABASE [{0}]", dbName);
            Rn = true;
            return Rn;
        }

        /// <summary>
        /// 查询数据库中已存在的所有数据库
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="hideSystemDatabase">是否隐藏系统数据库。true - 隐藏；false - 显示；默认为 true。</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static DataSet ListDatabases(string connStr, bool hideSystemDatabase = true)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            string sql = string.Format("select * from master.dbo.sysdatabases {0}",
                hideSystemDatabase ? " where name <>'master' and name <>'tempdb' and name<>'model' and name<>'msdb' and name<>'ReportServer' and name<>'ReportServerTempDB'" : string.Empty);
            DataSet ds = tmpDb.SelectData(sql);
            return ds;
        }

        /// <summary>
        /// 获取数据库中的所有表名称
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static DataSet ListTables(string connStr, string dbName)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                throw new DatabaseNotExistException(dbName);
            }

#if SQL2000
            string sql = string.Format("select * from [{0}].dbo.sysobjects where xtype = 'u' order by name asc", dbName);
#else
            string sql = string.Format("select * from [{0}].sys.tables order by name asc", dbName);
#endif
            DataSet ds = tmpDb.SelectData(sql);
            return ds;
        }

        /// <summary>
        /// 获取数据库指定表的所有触发器信息
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">要查询的数据库</param>
        /// <param name="tblname">要查询的表。为空或null时，将查询所有表的触发器</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static DataSet ListTriggers(string connStr, string dbName, string tblname)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                throw new DatabaseNotExistException(dbName);
            }

            string sql;
#if SQL2000
            if (!string.IsNullOrEmpty(tblname))
                sql = string.Format("select * from [{0}].dbo.sysobjects where xtype = 'tr' and parent_obj=OBJECT_ID(N'{0}.dbo.{1}') order by name asc", dbName, tblname);
            else
                sql = string.Format("select * from [{0}].dbo.sysobjects where xtype = 'tr' order by name asc", dbName);
#else
            if (!string.IsNullOrEmpty(tblname))
                sql = string.Format("select * from [{0}].sys.triggers where parent_id=OBJECT_ID(N'{0}.dbo.{1}') order by name asc", dbName, tblname);
            else
                sql = string.Format("select * from [{0}].sys.triggers order by name asc", dbName);
#endif
            DataSet ds = tmpDb.SelectData(sql);
            return ds;
        }

        /// <summary>
        /// 获取指定数据库的所有视图
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static DataSet ListViews(string connStr, string dbName)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                throw new DatabaseNotExistException(dbName);
            }

            string sql;
#if SQL2000
            sql = string.Format("select * from [{0}.dbo.sysobjects] where xtype = 'V' order by name asc", dbName);
#else
            sql = string.Format("select * from [{0}.sys.views] order by name asc", dbName);
#endif
            DataSet ds = tmpDb.SelectData(sql);
            return ds;
        }

        /// <summary>
        /// 获取数据库中指定表的信息
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="tblname">要查询的表</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static DataSet GetTableDetail(string connStr, string tblname)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

#warning 应检查数据库、表是否存在

            string sql = string.Format("select * from master.dbo.sysdatabases where name = '{0}'", tblname);
            DataSet ds = tmpDb.SelectData(sql);
            return ds;
        }

        /// <summary>
        /// 获取数据库指定表的所有列信息
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名</param>
        /// <param name="tblname">要查询的表</param>
        /// <returns></returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static DataSet ListColumns(string connStr, string dbName, string tblname)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                throw new DatabaseNotExistException(dbName);
            }

#warning 应继续检查表是否存在

            StringBuilder sb = new StringBuilder();
            #region 构造SQL语句
            sb.Append(" SELECT");
            sb.Append(" TableName=O.name,");
            sb.Append(" TableDesc=ISNULL(CASE WHEN C.column_id=1 THEN PTB.[value] END,N''),");
            sb.Append(" Column_id=C.column_id,");
            sb.Append(" ColumnName=C.name,");
            sb.Append(" PrimaryKey=ISNULL(IDX.PrimaryKey,N''),");
            sb.Append(" [IDENTITY]=CASE WHEN C.is_identity=1 THEN -1 ELSE 0 END,");
            sb.Append(" Computed=CASE WHEN C.is_computed=1 THEN -1 ELSE 0 END,");
            sb.Append(" Type=T.name,");
            sb.Append(" Length=C.max_length,");
            sb.Append(" Precision=C.precision,");
            sb.Append(" Scale=C.scale,");
            sb.Append(" NullAble=CASE WHEN C.is_nullable=1 THEN -1 ELSE 0 END,");
            sb.Append(" [Default]=ISNULL(D.definition,N''),");
            sb.Append(" ColumnDesc=ISNULL(PFD.[value],N''),");
            sb.Append(" IndexName=ISNULL(IDX.IndexName,N''),");
            sb.Append(" IndexSort=ISNULL(IDX.Sort,N''),");
            sb.Append(" Create_Date=O.Create_Date,");
            sb.Append(" Modify_Date=O.Modify_date");
            sb.Append(" FROM [{0}].sys.columns C");
            sb.Append(" INNER JOIN [{0}].sys.objects O");
            sb.Append(" ON C.[object_id]=O.[object_id]");
            sb.Append("  AND O.type='U'");
            sb.Append("  AND O.is_ms_shipped=0");
            sb.Append("  INNER JOIN [{0}].sys.types T");
            sb.Append("  ON C.user_type_id=T.user_type_id");
            sb.Append(" LEFT JOIN [{0}].sys.default_constraints D");
            sb.Append("  ON C.[object_id]=D.parent_object_id");
            sb.Append("       AND C.column_id=D.parent_column_id");
            sb.Append("        AND C.default_object_id=D.[object_id]");
            sb.Append(" LEFT JOIN [{0}].sys.extended_properties PFD");
            sb.Append("  ON PFD.class=1 ");
            sb.Append("       AND C.[object_id]=PFD.major_id ");
            sb.Append("        AND C.column_id=PFD.minor_id");
            sb.Append("  LEFT JOIN [{0}].sys.extended_properties PTB");
            sb.Append("      ON PTB.class=1 ");
            sb.Append("        AND PTB.minor_id=0 ");
            sb.Append("        AND C.[object_id]=PTB.major_id");
            sb.Append("  LEFT JOIN");
            sb.Append("(");
            sb.Append("    SELECT ");
            sb.Append("      IDXC.[object_id],");
            sb.Append("      IDXC.column_id,");
            sb.Append("      Sort=CASE INDEXKEY_PROPERTY(IDXC.[object_id],IDXC.index_id,IDXC.index_column_id,IDXC.is_descending_key)");
            sb.Append("         WHEN 1 THEN 'DESC' WHEN 0 THEN 'ASC' ELSE N'' END,");
            sb.Append("     PrimaryKey=CASE WHEN IDX.is_primary_key=1 THEN -1 ELSE 0 END,");
            sb.Append("      IndexName=IDX.Name");
            sb.Append("  FROM [{0}].sys.indexes IDX");
            sb.Append(" INNER JOIN [{0}].sys.index_columns IDXC");
            sb.Append("      ON IDX.[object_id]=IDXC.[object_id]");
            sb.Append("        AND IDX.index_id=IDXC.index_id");
            sb.Append("  LEFT JOIN [{0}].sys.key_constraints KC");
            sb.Append("     ON IDX.[object_id]=KC.[parent_object_id]");
            sb.Append("           AND IDX.index_id=KC.unique_index_id");
            sb.Append(" INNER JOIN");
            sb.Append("  (");
            sb.Append("    SELECT [object_id], Column_id, index_id=MIN(index_id)");
            sb.Append("    FROM [{0}].sys.index_columns");
            sb.Append("     GROUP BY [object_id], Column_id");
            sb.Append("  ) IDXCUQ");
            sb.Append("      ON IDXC.[object_id]=IDXCUQ.[object_id]");
            sb.Append("         AND IDXC.Column_id=IDXCUQ.Column_id");
            sb.Append("          AND IDXC.index_id=IDXCUQ.index_id");
            sb.Append(" ) IDX");
            sb.Append("  ON C.[object_id]=IDX.[object_id]");
            sb.Append("     AND C.column_id=IDX.column_id ");
            sb.Append("");
            if (!string.IsNullOrEmpty(tblname))
            {
                sb.Append(string.Format(" WHERE O.name=N'{0}' ", tblname));
            }
            sb.Append("ORDER BY O.name,C.column_id;");
            #endregion

            DataSet ds = tmpDb.SelectData(sb.ToString(), dbName);
            return ds;
        }

        /// <summary>
        /// 判断数据表中是否存在指定字段
        /// </summary>
        /// <param name="connStr">桥梁数据库的连接字符串（请参考 SDK 备注节）</param>
        /// <param name="dbName">数据库名称</param>
        /// <param name="tableName">要查询的表。为空或null时，将查询所有表的触发器</param>
        /// <param name="columnName">列名称</param>
        /// <returns>是否存在</returns>
        /// <remarks>
        /// 关于桥梁数据库的说明参见：<see cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        /// </remarks>
        /// <seealso cref="uoLib.Data.Singleton.SqlClientSqlServer.IsDatabaseExist(string, string)"/>
        public static bool IsColumnExists(string connStr, string dbName, string tableName, string columnName)
        {
            Database tmpDb = new Database(ConnectionType.SqlClient);
            tmpDb.ConnectionString = connStr;

            if (!IsDatabaseExist(connStr, dbName))
            {
                throw new DatabaseNotExistException(dbName);
            }

#warning 应继续检查表是否存在

            string sql;
#if SQL2000
            sql = string.Format("select count(1) from [{0}].dbo.syscolumns where [id]=object_id('{0}.dbo.{1}')  and [name]='{2}'", dbName, tableName, columnName);
#else
            sql = string.Format("select COUNT(1) from [{0}].sys.columns where object_id=object_id('{0}.dbo.{1}') and [name]='{2}'", dbName, tableName, columnName);
#endif

            object res = tmpDb.ExecuteScalar(tmpDb.CreateCommand(sql));
            if (res == null)
            {
                return false;
            }
            return Convert.ToInt32(res) > 0;
        }

        #region 数据库备份、还原
        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="targetDatabaseConn">待备份数据库的连接</param>
        /// <param name="backupToPath">用于保存备份文件的目录的全路径（注意：必须使用数据库服务器上的路径）</param>
        ///<param name="backupFileName"></param>
        /// <returns></returns>
        /// <remarks>
        /// 必须使用目标数据库的连接字符串。如需要备份uoLibDb，则conn中的数据库必须是uoLibDb。
		/// <p style="color:#0000ff">注意：要求具有 diskadmin 固定服务器角色中的成员身份</p>
        /// </remarks>
        public static string BackUp(DbConnection targetDatabaseConn, string backupToPath, string backupFileName)
        {
            string procname;
            string sql;
            string databaseName = targetDatabaseConn.Database;
            //if (string.IsNullOrEmpty(name)) name = string.Format("{0}_{1:yyyyMMddHHmmss}", databaseName, DateTime.Now);

            #region 准备备份路径
            if (uoLib.Common.Functions.IsNullOrEmptyStr(backupFileName)) throw new ArgumentNullException("backupFileName", "参数错误：必须指定备份文件名！");

            //if (!backupToPath.EndsWith("/") && !backupToPath.EndsWith(@"\")) { backupToPath = backupToPath + @"\"; }
            string newFilePath = System.IO.Path.Combine(backupToPath, backupFileName + ".bak");

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(backupToPath);
            if (!di.Exists)
            {
                di.Create();
            }
            else
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(newFilePath);
                if (fi.Exists)
                {
                    throw new Exception(string.Format("数据库服务器上文件已经存在：{0}。", newFilePath));
                }
            }
            #endregion

            if (targetDatabaseConn.State != ConnectionState.Open)
                targetDatabaseConn.Open();        //打开数据库连接

            #region 删除逻辑备份设备，但不会删掉备份的数据库文件
            procname = "sp_dropdevice";
            SqlCommand sqlcmd1 = new SqlCommand(procname, (SqlConnection)targetDatabaseConn);
            sqlcmd1.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlpar = new SqlParameter();
            sqlpar = sqlcmd1.Parameters.Add("@logicalname", SqlDbType.VarChar, 20);
            sqlpar.Direction = ParameterDirection.Input;
            sqlpar.Value = databaseName;

            try        //如果逻辑设备不存在，略去错误
            { sqlcmd1.ExecuteNonQuery(); }
            catch { }
            #endregion

            #region 创建逻辑备份设备
            procname = "sp_addumpdevice";
            SqlCommand sqlcmd2 = new SqlCommand(procname, (SqlConnection)targetDatabaseConn);
            sqlcmd2.CommandType = CommandType.StoredProcedure;

            sqlpar = sqlcmd2.Parameters.Add("@devtype", SqlDbType.VarChar, 20);
            sqlpar.Direction = ParameterDirection.Input;
            sqlpar.Value = "disk";

            sqlpar = sqlcmd2.Parameters.Add("@logicalname", SqlDbType.VarChar, 20);//逻辑设备名
            sqlpar.Direction = ParameterDirection.Input;
            sqlpar.Value = databaseName;

            sqlpar = sqlcmd2.Parameters.Add("@physicalname", SqlDbType.NVarChar, 260);//物理设备名
            sqlpar.Direction = ParameterDirection.Input;
            sqlpar.Value = newFilePath;

            int i = sqlcmd2.ExecuteNonQuery();
            #endregion

            #region 备份数据库到指定的数据库文件(完全备份)
            sql = "BACKUP DATABASE " + databaseName + " TO " + databaseName + " WITH INIT";
            SqlCommand sqlcmd3 = new SqlCommand(sql, (SqlConnection)targetDatabaseConn);
            sqlcmd3.CommandType = CommandType.Text;

            sqlcmd3.ExecuteNonQuery();
            #endregion

            if (targetDatabaseConn.State != ConnectionState.Closed)
                targetDatabaseConn.Close();

            return newFilePath;
        }

        /// <summary>
        /// 还原指定的数据库文件
        /// </summary>
        /// <param name="targetDatabaseConn">待还原数据库的连接</param>
        /// <param name="databaseFile">数据库备份文件及全路径</param>
        /// <returns></returns>
        /// <remarks>
        /// 必须使用目标数据库的连接字符串。如需要还原uoLibDb，则conn中的数据库必须是uoLibDb。
        /// </remarks>
        public static bool Restore(SqlConnection targetDatabaseConn, string databaseFile)
        {
            return Restore(targetDatabaseConn, targetDatabaseConn.Database, databaseFile);
        }
        /// <summary>
        /// 还原指定的数据库文件
        /// </summary>
        /// <param name="conn">数据库连接</param>
        /// <param name="targetDatabaseName">要还原的数据库</param>
        /// <param name="databaseFile">数据库备份文件及全路径</param>
        /// <returns></returns>
        public static bool Restore(SqlConnection conn, string targetDatabaseName, string databaseFile)
        {
            //清空与指定连接关联的连接池。
            SqlConnection.ClearPool(conn);

            //（1）使远程数据库转入单用户模式，断开所有已连接数据库的用户的连接并回退它们的事务。 
            //（2）使用T-SQL中的“restore”命令恢复远程数据库。 
            //（3）使远程数据库转入多用户模式。
            //string sql = string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK immediate;"
            //                            + "USE MASTER RESTORE DATABASE [{0}] FROM DISK='{1}' WITH REPLACE;"
            //                            + "ALTER DATABASE [{0}] SET MULTI_USER;", targetDatabaseName, databaseFile);

            string sql = string.Format("ALTER DATABASE [{0}] Set Offline with Rollback immediate;"
                            + "USE MASTER RESTORE DATABASE [{0}] FROM DISK='{1}' WITH REPLACE;"
                            + "ALTER DATABASE [{0}] Set OnLine With rollback Immediate;", targetDatabaseName, databaseFile);

            SqlCommand sqlcmd = new SqlCommand(sql, conn);
            sqlcmd.CommandType = CommandType.Text;

            try
            {
                conn.Open();
                sqlcmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

            return true;
        }


        #endregion

        #region 生成连接字符串
        /// <summary>
        /// 构造连接字符串
        /// </summary>
        /// <param name="sqlServer">服务器IP地址、域名等</param>
        /// <param name="database">数据库名</param>
        /// <param name="user">登录名</param>
        /// <param name="password">登录密码</param>
        /// <returns></returns>
        public static string CreateExampleConnectionString(string sqlServer, string database, string user, string password)
        {
            return Database.CreateExampleConnectionString(sqlServer, database, user, password, ConnectionType.SqlClient);
        }
        /// <summary>
        /// 返回支持的连接类型
        /// </summary>
        public static string GetSupportedConnType()
        {
            return Database.GetSupportedConnType(DatabaseType.SQL);
        }
        #endregion
    }
}
