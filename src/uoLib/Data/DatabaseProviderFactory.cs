using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.OracleClient;

namespace uoLib.Data
{
    public class DbProviderFactory : IDbProviderFactory
    {
        private ConnectionType connType = ConnectionType.SqlClient;
        private DatabaseType dbType = DatabaseType.SQL;
        private DbConnection _conn = null;

        #region IDbProviderFactory 成员
        /// <summary>
        /// 数据库连接所使用的驱动程序类型。
        /// <remarks>
        /// 各种数据库所能选择的连接驱动有所不同：
        /// SQL 可选 SqlClient、Odbc、OleDb；
        /// Access/Excel/Oracle 可选 Odbc、OleDb；
        /// Access2007/Excel2007 已被强制选择为 OleDb；
        /// SQLite/DSN 已被强制选择为Odbc；
        /// Oracle 已被强制选择为 OracleClient；
        /// </remarks>
        /// </summary>
        public ConnectionType ConnectionType
        {
            get { return this.connType; }
            set { this.connType = value; }
        }
        public DatabaseType DatabaseType
        {
            get { return this.dbType; }
            set { this.dbType = value; }
        }
        public DbConnection Conn
        {
            get { return _conn; }
            set { _conn = value; }
        }

        public DbConnection CreateConnection()
        {
            if (this.connType == ConnectionType.Other) { throw new Exceptions.NotSupportedConnException(this.connType.ToString()); }
            if (this.dbType == DatabaseType.Other) { throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString()); }
            switch (this.dbType)
            {
                case DatabaseType.Access:
                case DatabaseType.Excel:
                    switch (this.connType)
                    {
                        case ConnectionType.Odbc:
                            return new OdbcConnection();
                        case ConnectionType.OleDb:
                            return new OleDbConnection();
                        default: throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                case DatabaseType.Access2007:
                case DatabaseType.Excel2007:
                    return new OleDbConnection();

                case DatabaseType.SQLite:
                case DatabaseType.DSN:
                    return new OdbcConnection();

                case DatabaseType.Oracle:
                    return new OracleConnection();

                case DatabaseType.SQL:
                    switch (this.connType)
                    {
                        case ConnectionType.SqlClient:
                            return new SqlConnection();
                        case ConnectionType.Odbc:
                            return new OdbcConnection();
                        case ConnectionType.OleDb:
                            return new OleDbConnection();
                        default:
                            throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                default: throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString());
            }
        }

        public DbConnection CreateConnection(string connStr)
        {
            if (_conn == null)
            {
                _conn = CreateConnection();
                _conn.ConnectionString = connStr;
            }
            return _conn;
        }

        public DbDataAdapter CreateDataAdapter()
        {
            switch (this.dbType)
            {
                case DatabaseType.Access:
                case DatabaseType.Excel:
                    switch (this.connType)
                    {
                        case ConnectionType.Odbc:
                            return new OdbcDataAdapter();
                        case ConnectionType.OleDb:
                            return new OleDbDataAdapter();
                        default: throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                case DatabaseType.Access2007:
                case DatabaseType.Excel2007:
                    return new OleDbDataAdapter();

                case DatabaseType.SQLite:
                case DatabaseType.DSN:
                    return new OdbcDataAdapter();

                case DatabaseType.Oracle:
                    return new OracleDataAdapter();

                case DatabaseType.SQL:
                    switch (this.connType)
                    {
                        case ConnectionType.SqlClient:
                            return new SqlDataAdapter();
                        case ConnectionType.Odbc:
                            return new OdbcDataAdapter();
                        case ConnectionType.OleDb:
                            return new OleDbDataAdapter();
                        default:
                            throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                default: throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString());
            }
        }

        public DbDataAdapter CreateDataAdapter(string sqlCmdTxt)
        {
            DbCommand cmd = CreateCommand(sqlCmdTxt);
            return CreateDataAdapter(cmd);
        }
        public DbDataAdapter CreateDataAdapter(DbCommand cmd)
        {
            DbDataAdapter adp = CreateDataAdapter();
            switch (this.dbType)
            {
                case DatabaseType.Access:
                case DatabaseType.Excel:
                    switch (this.connType)
                    {
                        case ConnectionType.Odbc:
                            adp = new OdbcDataAdapter((OdbcCommand)cmd); break;
                        case ConnectionType.OleDb:
                            adp = new OleDbDataAdapter((OleDbCommand)cmd); break;
                        default: throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                    break;
                case DatabaseType.Access2007:
                case DatabaseType.Excel2007:
                    adp = new OleDbDataAdapter((OleDbCommand)cmd);
                    break;
                case DatabaseType.SQLite:
                case DatabaseType.DSN:
                    adp = new OdbcDataAdapter((OdbcCommand)cmd);
                    break;
                case DatabaseType.Oracle:
                    adp = new OracleDataAdapter((OracleCommand)cmd);
                    break;
                case DatabaseType.SQL:
                    switch (this.connType)
                    {
                        case ConnectionType.SqlClient:
                            adp = new SqlDataAdapter((SqlCommand)cmd); break;
                        case ConnectionType.Odbc:
                            adp = new OdbcDataAdapter((OdbcCommand)cmd); break;
                        case ConnectionType.OleDb:
                            adp = new OleDbDataAdapter((OleDbCommand)cmd); break;
                        default:
                            throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                    break;
                default: throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString());
            }
            return adp;
        }


        public DbDataReader CreateDataReader(string sqlCmdTxt)
        {
            DbCommand cmd = CreateCommand(sqlCmdTxt);
            return CreateDataReader(cmd);
        }
        public DbDataReader CreateDataReader(DbCommand cmd)
        {
            try
            {
                DbDataReader r = cmd.ExecuteReader();

                switch (this.dbType)
                {
                    case DatabaseType.Access:
                    case DatabaseType.Excel:
                        switch (this.connType)
                        {
                            case ConnectionType.Odbc:
                                r = (OdbcDataReader)r; break;
                            case ConnectionType.OleDb:
                                r = (OleDbDataReader)r; break;
                            default: throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                        }
                        break;
                    case DatabaseType.Access2007:
                    case DatabaseType.Excel2007:
                        r = (OleDbDataReader)r;
                        break;
                    case DatabaseType.SQLite:
                    case DatabaseType.DSN:
                        r = (OdbcDataReader)r;
                        break;
                    case DatabaseType.Oracle:
                        r = (OracleDataReader)r;
                        break;
                    case DatabaseType.SQL:
                        switch (this.connType)
                        {
                            case ConnectionType.SqlClient:
                                r = (SqlDataReader)r; break;
                            case ConnectionType.Odbc:
                                r = (OdbcDataReader)r; break;
                            case ConnectionType.OleDb:
                                r = (OleDbDataReader)r; break;
                            default:
                                throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                        }
                        break;
                    default: throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString());
                }
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DbParameter CreateParameter()
        {
            DbParameter para = null;
            switch (this.dbType)
            {
                case DatabaseType.Access:
                case DatabaseType.Excel:
                    switch (this.connType)
                    {
                        case ConnectionType.Odbc:
                            para = new OdbcParameter(); break;
                        case ConnectionType.OleDb:
                            para = new OleDbParameter(); break;
                        default: throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                    break;
                case DatabaseType.Access2007:
                case DatabaseType.Excel2007:
                    para = new OleDbParameter();
                    break;
                case DatabaseType.SQLite:
                case DatabaseType.DSN:
                    para = new OdbcParameter();
                    break;
                case DatabaseType.Oracle:
                    para = new OracleParameter();
                    break;
                case DatabaseType.SQL:
                    switch (this.connType)
                    {
                        case ConnectionType.SqlClient:
                            para = new SqlParameter(); break;
                        case ConnectionType.Odbc:
                            para = new OdbcParameter(); break;
                        case ConnectionType.OleDb:
                            para = new OleDbParameter(); break;
                        default:
                            throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                    break;
                default: throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString());
            }
            return para;
        }
        public DbParameter CreateParameter(string name, DbType type, object value)
        {
            DbParameter para = CreateParameter();
            para.ParameterName = name;
            para.DbType = type;
            para.Value = value;
            return para;
        }
        public DbParameter CreateOutPutParameter(string name, DbType type, int parameterSize)
        {
            DbParameter p = CreateParameter();
            p.ParameterName = name;
            p.DbType = type;
            p.Size = parameterSize;
            p.Direction = ParameterDirection.Output;
            return p;
        }


        public DbCommand CreateCommand()
        {
            DbCommand cmd = null;
            switch (this.dbType)
            {
                case DatabaseType.Access:
                case DatabaseType.Excel:
                    switch (this.connType)
                    {
                        case ConnectionType.Odbc:
                            cmd = new OdbcCommand(); break;
                        case ConnectionType.OleDb:
                            cmd = new OleDbCommand(); break;
                        default: throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                    break;
                case DatabaseType.Access2007:
                case DatabaseType.Excel2007:
                    cmd = new OleDbCommand();
                    break;
                case DatabaseType.SQLite:
                case DatabaseType.DSN:
                    cmd = new OdbcCommand();
                    break;
                case DatabaseType.Oracle:
                    cmd = new OracleCommand();
                    break;
                case DatabaseType.SQL:
                    switch (this.connType)
                    {
                        case ConnectionType.SqlClient:
                            cmd = new SqlCommand(); break;
                        case ConnectionType.Odbc:
                            cmd = new OdbcCommand(); break;
                        case ConnectionType.OleDb:
                            cmd = new OleDbCommand(); break;
                        default:
                            throw new Exceptions.NotSupportedConnException(this.connType.ToString());
                    }
                    break;
                default: throw new Exceptions.NotSupportedDataTypeException(this.dbType.ToString());
            }
            return cmd;
        }
        public DbCommand CreateCommand(string sqlStrTxt)
        {
            DbCommand cmd = CreateCommand();
            cmd.CommandText = sqlStrTxt;
            cmd.Connection = _conn;
            return cmd;
        }
        #endregion
    }
}
