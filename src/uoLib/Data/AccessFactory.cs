﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.OleDb;

namespace uoLib.Data
{
    class AccessFactory : IDatabase
    {
        private ConnectionType connType = ConnectionType.OleDb;
        private DbConnection _conn = null;
 
        #region IDatabase 成员

        public System.Data.Common.DbConnection Conn
        {
            get
            {
                return _conn;
            }
            set
            {
                _conn = value;
            }
        }

        public ConnectionType ConnectionType
        {
            get
            {
                return connType;
            }
            set
            {
                connType = value;
            }
        }

        public System.Data.Common.DbCommand CreateCommand()
        {
            DbCommand cmd = null;

        }

        public System.Data.Common.DbCommand CreateCommand(string sqlStrTxt)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbConnection CreateConnection(string connStr)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbConnection CreateConnection()
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataAdapter CreateDataAdapter()
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataAdapter CreateDataAdapter(System.Data.Common.DbCommand cmd)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataAdapter CreateDataAdapter(string sqlCmdTxt)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataReader CreateDataReader(System.Data.Common.DbCommand cmd)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataReader CreateDataReader(string sqlCmdTxt)
        {
            throw new NotImplementedException();
        }

        public string CreateExampleConnectionString(string pathWithfileName)
        {
            throw new NotImplementedException();
        }

        public string CreateExampleConnectionString(string server, string database, string user, string password)
        {
            throw new NotImplementedException();
        }

        public string CreateExampleConnectionString(string fileOrDsnPath_Or_DsnName, string user, string password)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbParameter CreateOutPutParameter(string name, System.Data.DbType type, int parameterSize)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbParameter CreateParameter()
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbParameter CreateParameter(string name, System.Data.DbType type, object value)
        {
            throw new NotImplementedException();
        }

        public DatabaseType DatabaseType
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public System.Data.DataSet SelectData(System.Data.Common.DbCommand selectCommand)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataSet SelectData(string selectString)
        {
            throw new NotImplementedException();
        }

        public int Execute(System.Data.Common.DbCommand cmd)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
