/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Data;
using System.Data.Common;
using uoLib.Data;

namespace uoLib.Data
{
    public interface IDbProviderFactory
    {
        ConnectionType ConnectionType
        {
            get;
            set;
        }
        DatabaseType DatabaseType
        {
            get;
            set;
        }

        DbConnection CreateConnection();
        DbConnection CreateConnection(string connStr);

        DbDataAdapter CreateDataAdapter();
        DbDataAdapter CreateDataAdapter(string sqlCmdTxt);
        DbDataAdapter CreateDataAdapter(DbCommand cmd);

        DbDataReader CreateDataReader(string sqlCmdTxt);
        DbDataReader CreateDataReader(DbCommand cmd);

        DbParameter CreateParameter();
        DbParameter CreateParameter(string name, System.Data.DbType type, object value);
        /// <summary>
        /// 生成一个DbParameter参数实例
        /// </summary>
        /// <param name="name">参数名</param>
        /// <param name="type">参数类型。e.g.DbType.Int32,DbType.String</param>
        /// <param name="parameterSize">获取或设置列中数据的最大大小（以字节为单位）。祥见DbParameter.Size</param>
        /// <returns></returns>
        DbParameter CreateOutPutParameter(string name, System.Data.DbType type, int parameterSize);

        DbCommand CreateCommand();
        DbCommand CreateCommand(string sqlCmdTxt);

        string CreateExampleConnectionString(string pathWithfileName);
        string CreateExampleConnectionString(string fileOrDsnPath_Or_DsnName, string user, string password);
        string CreateExampleConnectionString(string server, string database, string user, string password);
        System.Data.DataSet SelectData(System.Data.Common.DbCommand selectCommand);
        System.Data.DataSet SelectData(string selectString);
    }
}
