﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using uoLib.Data.Factories;

namespace uoLib.Data
{
    public abstract partial class BaseProvider : IBaseProvider, IConnectionFactory
    {
        /// <summary>
        /// 获取指定节点下所有子节点的ID。（此方法适用于经典无极分类表设计情况，祥见SDK）
        /// </summary>
        /// <param name="rootID">根节点ID</param>
        /// <param name="tableName">表名</param>
        /// <param name="IdColumnName">节点ID列的名称</param>
        /// <param name="parentIdColumnName">节点父ID列的名称</param>
        /// <remarks>
        /// <para>
        /// 分类表在很多系统中都有应用，特别是不限制分类数和分类级别的无限极分类。一般来讲，一个经典的无限极分类表是这样设计的：
        /// </para>
        /// <code><![CDATA[
        /// if object_id('[TB]') is not null drop table [TB]
        /// go
        /// create table [TB]([Id] int,[Name] varchar(20),[ParentId] int)
        /// insert [TB]
        /// select 1,'数码产品',0 union all
        /// select 2,'手机',1 union all
        /// select 3,'电脑',1 union all
        /// select 4,'Windows Mobile',2 union all
        /// select 5,'塞班',2 union all
        /// select 6,'笔记本',3 union all
        /// select 7,'台式机',3 union all
        /// select 8,'Windows Mobile 6.5 uonun',4 union all
        /// select 9,'Windows Phone 7 udnz.com',4
        /// select * from [TB]
        /// ]]></code>
        /// <para>
        /// 类似上述的表结构设计，它具有两个主要要素：
        /// <ul>
        /// <li>用于唯一表示当前记录的ID</li>
        /// <li>用于指明父级记录的ParentID</li>
        /// </ul>
        /// </para>
        /// <para>针对这样的情况，无法通过一句简单的SQL语句查询到一个分类的子子孙孙或根分类。
        /// 但通过本页介绍的方法 <see cref="uoLib.Data.BaseProvider.GetChildID(int, string, string, string)"/> 则可以很容易获取指定记录的所有子子孙孙的ID记录。
        /// 同理地，使用方法 <see cref="uoLib.Data.BaseProvider.GetParentID(int, string, string, string)"/> 可以很容易地追溯指定记录的所有父级ID。
        /// </para>
        /// <code>
        /// // 关于数据库实例，请见 uoLib.Data.Database
        /// SqlClientSqlServer db = SqlClientSqlServer.Instance;
        /// 
        /// string ids = db.GetChildID(1, "TB", "ID", "ParentID");
        /// /*
        ///  * ids = "1,2,3,4,5,6,7,8,9"
        ///  * 
        ///  ChildIDs
        ///  -------------------------
        ///  1,2,3,4,5,6,7,8,9
        ///  
        ///  (1 行受影响)
        ///  */
        /// 
        /// string parentIds = db.GetParentID(7, "TB", "ID", "ParentID");
        /// /*
        ///  * parentIds = "1,3,7"
        ///  * 
        ///  ParentIDs
        ///  -------------------------
        ///  1,3,7
        /// 
        ///  (1 行受影响)
        ///  */
        /// </code>
        /// <para>注：<see cref="uoLib.Data.BaseProvider.GetChildID(int, string, string, string)"/>、<see cref="uoLib.Data.BaseProvider.GetParentID(int, string, string, string)"/>
        /// 所执行的具体SQL查询语句，请自行使用工具跟踪。</para>
        /// </remarks>
        /// <returns>指定节点下所有子节点的ID，以英文逗号（,）分隔，包含指定节点本身的ID。<br />
        /// 返回 -1 表示未找到指定的分类。
        /// </returns>
        public string GetChildID(int rootID, string tableName, string IdColumnName, string parentIdColumnName)
        {
            string sql = "declare @rootID int,@ids varchar(500),@sql nvarchar(1000),@childs int;"
                        + "\n set @rootID = {3};"
                        + "\n set @ids = @rootID;"
                        + "\n set @childs = 1;"
                        + "\n while @childs!=0" 
                        + "\n begin"
                        + "\n 	set @sql = N' select @ids = @ids + '',''+convert(varchar(10),[{0}].[{1}]) '"
                        + "\n			 + N' from [{0}] '"
                        + "\n			 + N' where [{0}].[{2}] in ('+@ids+') and [{0}].[{1}] not in ('+@ids+');'"
                        + "\n			 + N' select @childs = @@rowcount;';"
                        + "\n	exec sp_executesql @sql,N'@ids varchar(500) OUTPUT,@childs int OUTPUT',@ids OUTPUT,@childs OUTPUT;"
                        + "\n end"
                        + "\n IF @ids = convert(varchar(500),@rootID)"
                        + "\n 	select 'ChildIDs'=-1;"
                        + "\n ELSE"
                        + "\n   select 'ChildIDs'=@ids;";
            object result = ExecuteScalar(CreateCommand(sql, tableName, IdColumnName, parentIdColumnName, rootID));
            return result.ToString();
        }

        /// <summary>
        /// 获取指定节点的所有父级节点ID，直到追溯到根节点。（此方法适用于经典无极分类表设计情况，祥见SDK）
        /// </summary>
        /// <param name="currentID">当前节点ID</param>
        /// <param name="tableName">表名</param>
        /// <param name="IdColumnName">节点ID列的名称</param>
        /// <param name="parentIdColumnName">节点父ID列的名称</param>
        /// <remarks>
        /// 祥见：<see cref="uoLib.Data.BaseProvider.GetChildID(int, string, string, string)"/>
        /// </remarks>
        /// <returns>
        /// 返回指定分类ID的所有父节点ID，以英文逗号（,）分隔，包含指定节点本身的ID。<br />
        /// 返回 -1 表示未找到指定的分类。
        /// </returns>
        public string GetParentID(int currentID, string tableName, string IdColumnName, string parentIdColumnName)
        {
            string sql = "declare @ids varchar(500),@sql nvarchar(1000),@currentID int,@tmpID int;"
                        + "\n set @currentID = {3};"
                        + "\n set @ids = @currentID;"
                        + "\n set @tmpID = @currentID;"
                        + "\n while @tmpID>0"
                        + "\n begin"
                        + "\n 	set @sql = N' select @tmpID = [{0}].[{2}],@ids = convert(varchar(10),[{0}].[{2}]) + '','' + @ids '"
                        + "\n 			 + N' from [{0}] '"
                        + "\n 			 + N' where [{0}].[{1}] = '+convert(varchar(10),@tmpID)+' AND [{0}].[{1}] <> [{0}].[{2}] AND [{0}].[{2}] <> 0;'"
                        + "\n 			 + N' if @@rowcount = 0 SET @tmpID = 0';"
                        + "\n 	exec sp_executesql @sql,N'@ids varchar(500) OUTPUT,@tmpID int OUTPUT',@ids OUTPUT,@tmpID OUTPUT;"
                        + "\n end"
                        + "\n IF @ids = convert(varchar(500),@currentID)"
                        + "\n 	select 'ParentIDs'=-1;"
                        + "\n ELSE"
                        + "\n   select 'ParentIDs'=@ids;";
            object result = ExecuteScalar(CreateCommand(sql, tableName, IdColumnName, parentIdColumnName, currentID));
            return result.ToString();
        }
    }
}
