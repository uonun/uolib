﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Data;
using System.Data.Common;
using uoLib.Data;
using System.Collections;
namespace uoLib.Data
{
    /// <summary>
    /// 定义数据库操作类应实现的方法。
    /// </summary>
    public interface IBaseProvider
    {
        /// <summary>
        /// 数据库连接所使用的驱动程序类型
        /// </summary>
        /// <remarks>
        /// 各种数据库所能选择的连接驱动有所不同：
        /// SQL 可选 SqlClient、Odbc、OleDb；
        /// Access/Excel 可选 Odbc、OleDb；
        /// Access2007/Excel2007 可选择为 OleDb；
        /// SQLite/DSN 可选择为Odbc；
        /// Oracle 可选择为 OracleClient；
        /// </remarks>
        ConnectionType ConnectionType { get; set; }

        #region Database 类实现的一些实例方法
        //如下方法需求Database类里的一个 DbConnection 实例（或为Database类提供一个实例），因此他们不在数据库连接工厂中
        /// <summary>
        /// 为数据库实例创建数据连接
        /// </summary>
        /// <remarks>调用此方法之后由此数据库实例所创建的DbCommand、DbDataAdapter、DbDataReader等对象将自动使用此连接。</remarks>
        DbConnection CreateConnection(string connStr);
        /// <summary>
        /// 创建一个System.Data.Common.DbCommand对象
        /// </summary>
        /// <remarks>所创建的DbCommand对象将自动使用数据库实例已有的连接。</remarks>
        DbCommand CreateCommand(string sqlStrTxt);
        /// <summary>
        /// 创建一个System.Data.Common.DbCommand对象
        /// </summary>
        /// <remarks>所创建的DbCommand对象将自动使用数据库实例已有的连接。</remarks>
        /// <example>
        /// <code>
        /// string tableName = "Table";
        /// Database db = new Database();
        /// DbCommand cmd = db.CreateCommand("SELECT * FROM [{0}]",tableName);
        /// </code>
        /// </example>
        DbCommand CreateCommand(string sqlStrTxt, params object[] args);
        /// <summary>
        /// 创建一个System.Data.Common.DbDataAdapter对象
        /// </summary>
        DbDataAdapter CreateDataAdapter(string sqlCmdTxt);
        /// <summary>
        /// 创建一个System.Data.Common.DbDataAdapter对象
        /// </summary>
        DbDataReader CreateDataReader(string sqlCmdTxt);
        #endregion

        /// <summary>
        /// 生成一个DbParameter输出参数实例
        /// </summary>
        /// <param name="name">参数名</param>
        /// <param name="type">参数类型。e.g.DbType.Int32,DbType.String</param>
        /// <param name="parameterSize">获取或设置列中数据的最大大小（以字节为单位）。祥见DbParameter.Size</param>
        /// <returns></returns>
        DbParameter CreateOutPutParameter(string name, System.Data.DbType type, int parameterSize);
        /// <summary>
        /// 生成一个DbParameter输入参数实例
        /// </summary>
        /// <param name="name">参数名</param>
        /// <param name="type">参数类型。e.g.DbType.Int32,DbType.String</param>
        /// <param name="value">参数值</param>
        DbParameter CreateParameter(string name, System.Data.DbType type, object value);

        /// <summary>
        /// 返回一个数据集。若数据集中无数据则返回 null。
        /// </summary>
        DataSet SelectData(string selectString);
        /// <summary>
        /// 返回一个数据集。若数据集中无数据则返回 null。
        /// </summary>
        DataSet SelectData(string selectString, params object[] args);
        /// <summary>
        /// 返回一个数据集。若数据集中无数据则返回 null。
        /// </summary>
        DataSet SelectData(DbCommand selectCommand);

        /// <summary>
        /// 执行DbCommand.ExecuteNonQuery()。
        /// </summary>
        /// <param name="cmd">要执行的 SQL 命令</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteScalar"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// </remarks>
        int ExecuteNonQuery(DbCommand cmd);
        /// <summary>
        /// 执行DbCommand.ExecuteScalar()。
        /// </summary>
        /// <param name="cmd">要执行的 SQL 命令</param>
        /// <returns></returns>
        /// <remarks>
        /// 与<see cref="DbCommand.ExecuteScalar"/>的区别在于，它在执行前后会自动为你打开和关闭对应的数据库连接。
        /// </remarks>
        object ExecuteScalar(DbCommand cmd);
    }
}
