/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Data;
using System.Data.Common;
using uoLib.Data.Factories;

namespace uoLib.Data
{
    /// <summary>
    /// 数据库对象类。
    /// </summary>
    /// <example>
    /// <para>如果数据库单体实例无法满足您的需求，您可以使用<see cref="Database"/>这个类来建立自己的数据库实例，它提供了比单体实例更多、更完善的方法和属性。下面对<see cref="Database"/>类的使用作一个简单的示例：</para>
    /// <code><![CDATA[
    /// using System;
    /// using System.Data;
    /// using System.Data.Common;
    /// using uoLib.Data;
    /// namespace DemoWeb  {
    ///     public partial class Demo2 : System.Web.UI.Page
    ///     {
    ///         protected void Page_Load(object sender, EventArgs e)
    ///         {
    ///             // 声明数据库实例
    ///             Database db = new Database(ConnectionType.SqlClient);
    ///             // 初始化数据库连接
    ///             db.CreateConnection(
    ///                 Database.CreateExampleConnectionString(
    ///                 "127.0.0.1", "NorthWind", "sa", "",
    ///                 ConnectionType.SqlClient) 
    ///             );
    ///             Response.Write(db.ConnectionString);
    ///             
    ///             // 示例：执行 DbCommand
    ///             // 可执行包括SELECT/INSERT/UPDATE/DELETE、存储过程等任何DbCommand命令
    ///             int _id = 10;
    ///             DbCommand cmd = db.CreateCommand("DELETE FROM [TABLE] WHERE ID=@id");
    ///             cmd.Parameters.Add(db.CreateParameter("@id", System.Data.DbType.Int32,_id));
    ///             Database.Execute(cmd);
    ///             
    ///             // 示例：使用存储过程、查询数据库
    ///             cmd = db.CreateCommand("myProcedure"); 
    ///             cmd.CommandType = CommandType.StoredProcedure;
    ///             cmd.Parameters.Add(db.CreateParameter("@id", System.Data.DbType.Int32, _id));
    ///             DataSet ds = db.SelectData(cmd);
    ///             if (ds == null) 
    ///             { 
    ///                 Response.Write("数据库为空！");
    ///             } else {
    ///                 // do something...
    ///             }
    ///         }
    ///     }
    /// }]]></code>
    /// </example>
    /// <exception cref="uoLib.Exceptions.DatabaseNotExistException">指定的数据库不存在</exception>
    /// <exception cref="uoLib.Exceptions.NotSupportedConnException">不支持的连接方式</exception>
    /// <exception cref="uoLib.Exceptions.NotSupportedDataTypeException">不支持的数据库类型</exception>
    /// <exception cref="uoLib.Exceptions.NullConnectionStringException">未找到与指定名称匹配的连接字符串</exception>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public class Database : BaseProvider
    {
        /// <summary>
        /// 生成一个数据库实例
        /// </summary>
        /// <param name="connType">数据库连接所使用的驱动程序类型。</param>
        /// <remarks>
        /// 各种数据库所能选择的连接驱动有所不同：
        /// <list type="table">
        /// <item>
        /// <term>SQL</term>
        /// <description>SqlClient、Odbc、OleDb</description>
        /// </item>
        /// <item>
        /// <term>Access/Excel/Oracle</term>
        /// <description>Odbc、OleDb</description>
        /// </item>
        /// <item>
        /// <term>Access2007/Excel2007</term>
        /// <description>OleDb</description>
        /// </item>
        /// <item>
        /// <term>SQLite/DSN</term>
        /// <description>Odbc</description>
        /// </item>
        /// <item>
        /// <term>Oracle</term>
        /// <description>OracleClient</description>
        /// </item>
        /// </list>
        /// </remarks>
        public Database(ConnectionType connType) : base(connType) { }

        #region 生成连接字符串
        //数据库连接字符串参考：http://www.connectionstrings.com/

        /// <summary>
        /// 构造连接字符串
        /// </summary>
        /// <param name="sqlServer">Sql Server 服务器。若指定端口号，请参考这样的格式：“udnz.com,2433”</param>
        /// <param name="database">数据库名称</param>
        /// <param name="user">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="connType">连接类型（Odbc|OleDb|SqlClient）</param>
        /// <returns></returns>
        public static string CreateExampleConnectionString(string sqlServer, string database, string user, string password, ConnectionType connType)
        {
            switch (connType)
            {
                case ConnectionType.Odbc:
                    return string.Format(@"Driver={SQL Server};Server={0};Database={1};Uid={2};Pwd={3};", sqlServer, database, user, password);
                case ConnectionType.OleDb:
                    return string.Format(@"Provider=sqloledb;Data Source={0};Initial Catalog={1};User Id={2};Password={3};", sqlServer, database, user, password);
                case ConnectionType.SqlClient:
                    return string.Format(@"Data Source={0};Initial Catalog={1};User Id={2};Password={3};", sqlServer, database, user, password);
                default:
                    throw new Exceptions.NotSupportedConnException(DatabaseType.SQL, connType);
            }
        }
        /// <summary>
        /// 构造连接字符串
        /// </summary>
        /// <param name="OfficeFileOrDsnPath_Or_DsnName">
        /// 文件路径（Access、Excel等）；
        /// 文件DSN路径（*.dsn）；
        /// 用户/系统DSN名称；
        /// Oracle数据源名称。</param>
        /// <param name="user">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="dbType"></param>
        /// <param name="connType"></param>
        /// <returns></returns>
        public static string CreateExampleConnectionString(string OfficeFileOrDsnPath_Or_DsnName, string user, string password, DatabaseType dbType, ConnectionType connType)
        {
            string types = GetSupportedConnType(dbType);
            if (!types.Contains(connType.ToString())) { throw new Exceptions.NotSupportedConnException(dbType, connType); }

            OfficeFileOrDsnPath_Or_DsnName = OfficeFileOrDsnPath_Or_DsnName.ToLower();

            switch (dbType)
            {
                case DatabaseType.Oracle:
                    switch (connType)
                    {
                        case ConnectionType.OracleClient:
                            return string.Format(@"Data Source={0};User Id={1};Password={2};Integrated Security=no;", OfficeFileOrDsnPath_Or_DsnName, user, password);
                    } break;
                case DatabaseType.DSN:
                    switch (connType)
                    {
                        case ConnectionType.Odbc:
                            if (OfficeFileOrDsnPath_Or_DsnName.Length > 4)
                            {
                                if (OfficeFileOrDsnPath_Or_DsnName.EndsWith(".dsn"))
                                    return string.Format(@"FILEDSN={0};Uid={1};Pwd={2};", OfficeFileOrDsnPath_Or_DsnName, user, password);
                                else
                                    return string.Format(@"DSN={0};Uid={1};Pwd={2};", OfficeFileOrDsnPath_Or_DsnName, user, password);
                            }
                            else { return string.Format(@"DSN={0};Uid={1};Pwd={2};", OfficeFileOrDsnPath_Or_DsnName, user, password); }
                    } break;
                case DatabaseType.Access:
                    switch (connType)
                    {
                        case ConnectionType.Odbc:
                            return string.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid={1};Pwd={2};", OfficeFileOrDsnPath_Or_DsnName, user, password);
                        case ConnectionType.OleDb:
                            return string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};User Id={1};Password={2};", OfficeFileOrDsnPath_Or_DsnName, user, password);
                    } break;
                case DatabaseType.Excel:
                    switch (connType)
                    {
                        case ConnectionType.Odbc:
                            return string.Format(@"Driver={{Microsoft Excel Driver (*.xls)}};DriverId=790;Dbq={0};DefaultDir=c:\temp;", OfficeFileOrDsnPath_Or_DsnName);
                        case ConnectionType.OleDb:
                            return string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";", OfficeFileOrDsnPath_Or_DsnName);
                    } break;
                case DatabaseType.Access2007:
                    if (connType == ConnectionType.OleDb)
                        return string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password={1};", OfficeFileOrDsnPath_Or_DsnName, password);
                    break;
                case DatabaseType.Excel2007:
                    if (connType == ConnectionType.OleDb)
                        return string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1"";", OfficeFileOrDsnPath_Or_DsnName);
                    break;
                case DatabaseType.SQLite:
                    if (connType == ConnectionType.Odbc)
                        return string.Format(@"DRIVER=SQLite3 ODBC Driver;Database={0};LongNames=0;Timeout=1000;NoTXN=0;SyncPragma=NORMAL;StepAPI=0;", OfficeFileOrDsnPath_Or_DsnName);
                    break;
            }
            throw new Exceptions.NotSupportedConnException(dbType, connType);
        }

        /// <summary>
        /// 返回各种数据库所支持的连接类型
        /// </summary>
        /// <param name="dbType">数据库类型</param>
        /// <returns></returns>
        public static string GetSupportedConnType(DatabaseType dbType)
        {
            switch (dbType)
            {
                case DatabaseType.SQL:
                    return string.Format("{0},{1},{2}", ConnectionType.Odbc, ConnectionType.OleDb, ConnectionType.SqlClient);
                case DatabaseType.Access:
                case DatabaseType.Excel:
                    return string.Format("{0},{1}", ConnectionType.Odbc, ConnectionType.OleDb);
                case DatabaseType.Access2007:
                case DatabaseType.Excel2007:
                    return ConnectionType.OleDb.ToString();
                case DatabaseType.SQLite:
                case DatabaseType.DSN:
                    return ConnectionType.Odbc.ToString();
                case DatabaseType.Oracle:
                    return ConnectionType.OracleClient.ToString();
                default:
                    throw new Exceptions.NotSupportedDataTypeException(dbType);
            }
        }
        #endregion

    }
}

