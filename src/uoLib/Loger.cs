﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using uoLib.Common;

namespace uoLib
{
    /// <summary>
    /// 提供<see cref="uoLib.AvailableTypeAttribute.Type.WinForm"/>程序在运行时的日志、异常记录。
    /// </summary>
    /// <example>
    /// 此类常用于<see cref="uoLib.AvailableTypeAttribute.Type.WinForm"/>程序在运行过程中的日志记录。如：
    /// <code><![CDATA[
    /// static class Program
    /// {
    ///     static void Main(string[] args)
    ///     {
    ///         Loger.Write("主程序开始执行：");
    ///         Loger.Write("wait for 5 seconds...");
    ///         Thread.Sleep(5000);
    ///         //do something...
    ///         Loger.Write("doing something...");
    ///         Loger.Write("wait for 5 seconds...");
    ///         Thread.Sleep(5000);
    ///         //do other things...
    ///         Loger.Write("doing other things...");
    ///         
    ///         Loger.Write("主程序结束。");
    ///     }
    /// }]]></code>
    /// 此段代码将输出一下 log 文件：
    /// <code><![CDATA[
    /// [2009-4-21 17:12:02] 主程序开始执行：
    /// [2009-4-21 17:12:02] wait for 5 seconds...
    /// [2009-4-21 17:12:07] doing something...
    /// [2009-4-21 17:12:07] wait for 5 seconds...
    /// [2009-4-21 17:12:12] doing other things...
    /// [2009-4-21 17:12:12] 主程序结束。
    /// ]]></code>
    /// </example>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public static class Logger
    {
        private static readonly object _lock = new object();

        #region 将日志信息写入到Logs文件夹中
        /// <summary>
        /// 将日志信息写入到Logs文件夹中。（以当前应用程序目录为根目录）
        /// </summary>
        /// <param name="msg"></param>
        [AvailableType(AvailableTypeAttribute.Type.WinForm)]
        public static void Write(string msg)
        {
            string logFolder = string.Format(@"{0}\Logs", Application.StartupPath);
            DirectoryInfo di = new DirectoryInfo(logFolder);
            if (!di.Exists) di.Create();

            Write(msg, di.FullName, true, Encoding.Default);
        }

        /// <summary>
        /// 将日志信息写入到指定文件夹中。
        /// </summary>
        /// <param name="msg">日志信息</param>
        /// <param name="folderPath">文件夹的物理路径。格式如：E:\uonun\Logs</param>
        /// <param name="append">确定是否将数据追加到文件。如果该文件存在，并且 append 为 false，则该文件被覆盖。如果该文件存在，并且 append 为 true，则数据被追加到该文件中。否则，将创建新文件。</param>
        /// <param name="encoding">要使用的字符编码。</param>
        public static void Write(string msg, string folderPath, bool append, Encoding encoding)
        {
            if (uoLib.Common.Functions.IsNullOrEmptyStr(folderPath)) throw new ArgumentNullException("folderPath");

            lock (_lock)
            {
                try
                {
                    DirectoryInfo di = new DirectoryInfo(folderPath);
                    if (!di.Exists) di.Create();

                    using (StreamWriter sw = new StreamWriter(string.Format(@"{0}\{1:yyyyMMdd}.log", di.FullName, DateTime.Now), append, encoding))
                    {
                        if (msg == Environment.NewLine)
                        {
                            sw.WriteLine(Environment.NewLine);
                        }
                        else
                        {
                            sw.WriteLine(string.Format("[{0}] {1}", DateTime.Now, msg));
                        }
                        sw.Close();
                    }
                }
                catch { }
            }
        }
        #endregion

        #region 将异常写入Errors文件夹中

        /// <summary>
        /// 将异常信息写入文本文件中
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="filePath">文本文件的物理路径</param>
        /// <param name="append">确定是否将数据追加到文件。如果该文件存在，并且 append 为 false，则该文件被覆盖。如果该文件存在，并且 append 为 true，则数据被追加到该文件中。否则，将创建新文件。</param>
        /// <param name="encoding">要使用的字符编码。</param>
        [AvailableType(AvailableTypeAttribute.Type.WinForm)]
        public static void Write(Exception ex, string filePath, bool append, Encoding encoding)
        {
            if (ex == null) throw new ArgumentNullException("ex");
            if (uoLib.Common.Functions.IsNullOrEmptyStr(filePath)) throw new ArgumentNullException("filePath");

            lock (_lock)
            {
                using (StreamWriter sw = new StreamWriter(filePath, append, encoding))
                {
                    sw.AutoFlush = true;

                    #region 写文件
                    sw.WriteLine("==============================================================================");
                    sw.WriteLine("Exe:\t\t{0}", Application.ExecutablePath);
                    sw.WriteLine("Time:\t\t{0}", DateTime.Now);
                    sw.WriteLine("OSVersion:\t{0}", Environment.OSVersion.VersionString);
                    sw.WriteLine("ProcessorCount:\t{0}", Environment.ProcessorCount);
                    sw.WriteLine(".Net Framework:\t{0}", Environment.Version);
                    sw.WriteLine("==============================================================================");
                    sw.WriteLine();
                    sw.WriteLine("This application has encountered a critical error:");
                    sw.WriteLine();

                    //通过递归生成一个包含所有异常的堆
                    Stack<Exception> exStack = new Stack<Exception>();
                    uoLib.Common.Functions.GetErrorStack(ex, exStack);

                    while (exStack.Count > 0)
                    {
                        Exception e = exStack.Pop();
                        sw.WriteLine("------------------------------------------------------------------------------");
                        sw.WriteLine("Stack<Exception>[{0}]: {1}", exStack.Count, e.Message);
                        sw.WriteLine("------------------------------------------------------------------------------");
                        sw.WriteLine("GetType():\t{0}", e.GetType());
                        sw.WriteLine("Source:\t\t{0}", e.Source);
                        MethodBase mb = e.TargetSite;
                        if (mb != null)
                        {
                            #region 引发异常的方法
                            StringBuilder psStr = new StringBuilder();
                            ParameterInfo[] ps = mb.GetParameters();
                            if (ps.Length > 0)
                            {
                                foreach (ParameterInfo p in ps)
                                {
                                    psStr.Append(p.ToString());
                                    if (p != null && p.DefaultValue != null && !string.IsNullOrEmpty(p.DefaultValue.ToString())) { psStr.Append("[" + p.DefaultValue.ToString() + "]"); }
                                    psStr.Append(", ");
                                }
                                psStr.Remove(psStr.Length - 2, 2);
                            }

                            Regex re = new Regex("\\(.*\\)", RegexOptions.IgnoreCase);
                            sw.WriteLine("TargetSite:\t{0}", re.Replace(mb.ToString(), string.Format("({0})", psStr.ToString())));
                            #endregion

                            sw.WriteLine("ReflectedType:\t{0}", mb.ReflectedType);
                        }
                        sw.WriteLine("HelpLink:\t\t{0}", e.HelpLink);

                        sw.WriteLine();
                        sw.WriteLine("-----------");
                        sw.WriteLine("ToString()");
                        sw.WriteLine("-----------");
                        sw.WriteLine(e.ToString());

                        IDictionary data = e.Data;
                        if (data != null && data.Keys.Count > 0)
                        {
                            sw.WriteLine();
                            sw.WriteLine("-----");
                            sw.WriteLine("Data");
                            sw.WriteLine("-----");
                            foreach (DictionaryEntry de in data)
                            {
                                sw.WriteLine("{0}\t\t: {1}", de.Key, de.Value);
                            }
                            sw.WriteLine();
                        }
                        sw.WriteLine();
                        sw.WriteLine();
                    }

                    sw.WriteLine();
                    sw.WriteLine();

                    sw.WriteLine("[Written by uolib v{0}, http://work.udnz.com/uolib]", Info.Version);
                    sw.WriteLine("==============================================================================");
                    sw.Close();

                    #endregion
                }
            }
        }

        /// <summary>
        /// 将异常信息写入到Errors文件夹的一个文本文件中，并返回该文本文件的路径。（以当前应用程序目录为根目录）
        /// </summary>
        /// <param name="ex"></param>
        /// <returns>文件路径</returns>
        /// <remarks>该方法不抛出异常，若写入失败将返回 null</remarks>
        [AvailableType(AvailableTypeAttribute.Type.WinForm)]
        public static string Write(Exception ex)
        {
            if (ex == null) throw new ArgumentNullException("ex");

            string folderPath = string.Format(@"{0}\Errors", Application.StartupPath);
            string filePath = string.Format(@"{0}\{1:yyyy-MM-dd HH.mm.ss} Crash.txt", folderPath, DateTime.Now);

            DirectoryInfo di = new DirectoryInfo(folderPath);
            if (!di.Exists) di.Create();

            Write(ex, filePath, true, Encoding.Default);

            return filePath;
        }
        #endregion

        #region 写入系统日志
        /// <summary>
        /// 写入系统事件日志
        /// </summary>
        /// <param name="source">事件来源</param>
        /// <param name="message">日志正文</param>
        /// <param name="type">日志类型</param>
        /// <param name="log">日志文件名，如“网络视频监控”。</param>
        /// <remarks>
        /// <para><strong>注意：如果原来的日志文件中已有当前指定的 <paramref name="source"/> ，现在要将此事件源添加到新的日志文件里
        /// 则需要重启计算机方能生效。
        /// </strong></para>
        /// </remarks>
        [AvailableType(AvailableTypeAttribute.Type.WinForm)]
        public static void WriteEventLog(string source, string message, EventLogEntryType type, string log)
        {
            try
            {
                EventLog elog = new EventLog();
                if (!uoLib.Common.Functions.IsNullOrEmptyStr(log))
                {
                    elog.Log = log;
                }

                if (!uoLib.Common.Functions.IsNullOrEmptyStr(source))
                {
                    elog.Source = source;
                }
                else
                {
                    elog.Source = "Writen by uoLib";
                }

                //先把旧版本的事件日志源删除
                if (!EventLog.Exists(elog.Log))
                {
                    if (EventLog.SourceExists(elog.Source)) { EventLog.DeleteEventSource(elog.Source); }
                }

                //事件日志源不存在，则添加
                if (!EventLog.SourceExists(elog.Source))
                {
                    EventLog.CreateEventSource(elog.Source, elog.Log);
                }

                try
                {
                    elog.WriteEntry(message, type);
                }
                catch
                {
                    EventLog.DeleteEventSource(elog.Source);
                    elog.WriteEntry(message, type);
                }
                elog.Close();
            }
            catch (System.Security.SecurityException ex)
            {
                System.Security.SecurityException err = new System.Security.SecurityException("写入事件日志失败，没有足够的权限！", ex);
                throw err;
            }
        }

        /// <summary>
        /// 写入系统事件日志
        /// </summary>
        /// <param name="message">日志正文</param>
        /// <param name="type">日志类型</param>
        [AvailableType(AvailableTypeAttribute.Type.WinForm)]
        public static void WriteEventLog(string message, EventLogEntryType type)
        {
            WriteEventLog(null, message, type, null);
        }
        #endregion


    }
}

