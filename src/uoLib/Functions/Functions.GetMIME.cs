/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace uoLib.Common
{
    public static partial class Functions 
    {
        /// <summary>
        /// 根据扩展名返回所对应的MIME类型字符串。
        /// </summary>
        /// <param name="fileExtension">文件扩展名。若没有以“.”开头，方法内部将自动补齐。</param>
        public static string GetMIME(string fileExtension)
        {
            if (string.IsNullOrEmpty(fileExtension)) { return string.Empty; }
            fileExtension = fileExtension.Trim().ToLower();
            if (!fileExtension.StartsWith(".")) fileExtension = "." + fileExtension;

            string str = string.Empty;
            switch (fileExtension)
            {
                case ".*": str = "application/octet-stream"; break;
                case ".001": str = "application/x-001"; break;
                case ".301": str = "application/x-301"; break;
                case ".323": str = "text/h323"; break;
                case ".906": str = "application/x-906"; break;
                case ".907": str = "drawing/907"; break;
                case ".a11": str = "application/x-a11"; break;
                case ".acp": str = "audio/x-mei-aac"; break;
                case ".ai": str = "application/postscript"; break;
                case ".aif": str = "audio/aiff"; break;
                case ".aifc": str = "audio/aiff"; break;
                case ".aiff": str = "audio/aiff"; break;
                case ".anv": str = "application/x-anv"; break;
                case ".asa": str = "text/asa"; break;
                case ".asf": str = "video/x-ms-asf"; break;
                case ".asp": str = "text/asp"; break;
                case ".asx": str = "video/x-ms-asf"; break;
                case ".au": str = "audio/basic"; break;
                case ".avi": str = "video/avi"; break;
                case ".awf": str = "application/vnd.adobe.workflow"; break;
                case ".biz": str = "text/xml"; break;
                case ".bmp": str = "application/x-bmp"; break;
                case ".bot": str = "application/x-bot"; break;
                case ".c4t": str = "application/x-c4t"; break;
                case ".c90": str = "application/x-c90"; break;
                case ".cal": str = "application/x-cals"; break;
                case ".cat": str = "application/vnd.ms-pki.seccat"; break;
                case ".cdf": str = "application/x-netcdf"; break;
                case ".cdr": str = "application/x-cdr"; break;
                case ".cel": str = "application/x-cel"; break;
                case ".cer": str = "application/x-x509-ca-cert"; break;
                case ".cg4": str = "application/x-g4"; break;
                case ".cgm": str = "application/x-cgm"; break;
                case ".cit": str = "application/x-cit"; break;
                case ".class": str = "java/*"; break;
                case ".cml": str = "text/xml"; break;
                case ".cmp": str = "application/x-cmp"; break;
                case ".cmx": str = "application/x-cmx"; break;
                case ".cot": str = "application/x-cot"; break;
                case ".crl": str = "application/pkix-crl"; break;
                case ".crt": str = "application/x-x509-ca-cert"; break;
                case ".csi": str = "application/x-csi"; break;
                case ".css": str = "text/css"; break;
                case ".cut": str = "application/x-cut"; break;
                case ".dbf": str = "application/x-dbf"; break;
                case ".dbm": str = "application/x-dbm"; break;
                case ".dbx": str = "application/x-dbx"; break;
                case ".dcd": str = "text/xml"; break;
                case ".dcx": str = "application/x-dcx"; break;
                case ".der": str = "application/x-x509-ca-cert"; break;
                case ".dgn": str = "application/x-dgn"; break;
                case ".dib": str = "application/x-dib"; break;
                case ".dll": str = "application/x-msdownload"; break;
                case ".doc": str = "application/msword"; break;
                case ".dot": str = "application/msword"; break;
                case ".drw": str = "application/x-drw"; break;
                case ".dtd": str = "text/xml"; break;
                //case ".dwf": str = "Model/vnd.dwf"; break;
                case ".dwf": str = "application/x-dwf"; break;
                case ".dwg": str = "application/x-dwg"; break;
                case ".dxb": str = "application/x-dxb"; break;
                case ".dxf": str = "application/x-dxf"; break;
                case ".edn": str = "application/vnd.adobe.edn"; break;
                case ".emf": str = "application/x-emf"; break;
                case ".eml": str = "message/rfc822"; break;
                case ".ent": str = "text/xml"; break;
                case ".epi": str = "application/x-epi"; break;
                //case ".eps": str = "application/x-ps"; break;
                case ".eps": str = "application/postscript"; break;
                case ".etd": str = "application/x-ebx"; break;
                case ".exe": str = "application/x-msdownload"; break;
                case ".fax": str = "image/fax"; break;
                case ".fdf": str = "application/vnd.fdf"; break;
                case ".fif": str = "application/fractals"; break;
                case ".fo": str = "text/xml"; break;
                case ".frm": str = "application/x-frm"; break;
                case ".g4": str = "application/x-g4"; break;
                case ".gbr": str = "application/x-gbr"; break;
                case ".gcd": str = "application/x-gcd"; break;
                case ".gif": str = "image/gif"; break;
                case ".gl2": str = "application/x-gl2"; break;
                case ".gp4": str = "application/x-gp4"; break;
                case ".hgl": str = "application/x-hgl"; break;
                case ".hmr": str = "application/x-hmr"; break;
                case ".hpg": str = "application/x-hpgl"; break;
                case ".hpl": str = "application/x-hpl"; break;
                case ".hqx": str = "application/mac-binhex40"; break;
                case ".hrf": str = "application/x-hrf"; break;
                case ".hta": str = "application/hta"; break;
                case ".htc": str = "text/x-component"; break;
                case ".htm": str = "text/html"; break;
                case ".html": str = "text/html"; break;
                case ".htt": str = "text/webviewhtml"; break;
                case ".htx": str = "text/html"; break;
                case ".icb": str = "application/x-icb"; break;
                case ".ico": str = "image/x-icon"; break;
                //case ".ico": str = "application/x-ico"; break;
                case ".iff": str = "application/x-iff"; break;
                case ".ig4": str = "application/x-g4"; break;
                case ".igs": str = "application/x-igs"; break;
                case ".iii": str = "application/x-iphone"; break;
                case ".img": str = "application/x-img"; break;
                case ".ins": str = "application/x-internet-signup"; break;
                case ".isp": str = "application/x-internet-signup"; break;
                case ".IVF": str = "video/x-ivf"; break;
                case ".java": str = "java/*"; break;
                case ".jfif": str = "image/jpeg"; break;
                case ".jpe": str = "image/jpeg"; break;
                //case ".jpe": str = "application/x-jpe"; break;
                case ".jpeg": str = "image/jpeg"; break;
                case ".jpg": str = "image/pjpeg"; break;
                //case ".jpg": str = "application/x-jpg"; break;
                case ".js": str = "application/x-javascript"; break;
                case ".jsp": str = "text/html"; break;
                case ".la1": str = "audio/x-liquid-file"; break;
                case ".lar": str = "application/x-laplayer-reg"; break;
                case ".latex": str = "application/x-latex"; break;
                case ".lavs": str = "audio/x-liquid-secure"; break;
                case ".lbm": str = "application/x-lbm"; break;
                case ".lmsff": str = "audio/x-la-lms"; break;
                case ".ls": str = "application/x-javascript"; break;
                case ".ltr": str = "application/x-ltr"; break;
                case ".m1v": str = "video/x-mpeg"; break;
                case ".m2v": str = "video/x-mpeg"; break;
                case ".m3u": str = "audio/mpegurl"; break;
                case ".m4e": str = "video/mpeg4"; break;
                case ".mac": str = "application/x-mac"; break;
                case ".man": str = "application/x-troff-man"; break;
                case ".math": str = "text/xml"; break;
                case ".mdb": str = "application/msaccess"; break;
                //case ".mdb": str = "application/x-mdb"; break;
                case ".mfp": str = "application/x-shockwave-flash"; break;
                case ".mht": str = "message/rfc822"; break;
                case ".mhtml": str = "message/rfc822"; break;
                case ".mi": str = "application/x-mi"; break;
                case ".mid": str = "audio/mid"; break;
                case ".midi": str = "audio/mid"; break;
                case ".mil": str = "application/x-mil"; break;
                case ".mml": str = "text/xml"; break;
                case ".mnd": str = "audio/x-musicnet-download"; break;
                case ".mns": str = "audio/x-musicnet-stream"; break;
                case ".mocha": str = "application/x-javascript"; break;
                case ".movie": str = "video/x-sgi-movie"; break;
                case ".mp1": str = "audio/mp1"; break;
                case ".mp2": str = "audio/mp2"; break;
                case ".mp2v": str = "video/mpeg"; break;
                case ".mp3": str = "audio/mp3"; break;
                case ".mp4": str = "video/mpeg4"; break;
                case ".mpa": str = "video/x-mpg"; break;
                case ".mpd": str = "application/vnd.ms-project"; break;
                case ".mpe": str = "video/x-mpeg"; break;
                case ".mpeg": str = "video/mpg"; break;
                case ".mpg": str = "video/mpg"; break;
                case ".mpga": str = "audio/rn-mpeg"; break;
                case ".mpp": str = "application/vnd.ms-project"; break;
                case ".mps": str = "video/x-mpeg"; break;
                case ".mpt": str = "application/vnd.ms-project"; break;
                case ".mpv": str = "video/mpg"; break;
                case ".mpv2": str = "video/mpeg"; break;
                case ".mpw": str = "application/vnd.ms-project"; break;
                case ".mpx": str = "application/vnd.ms-project"; break;
                case ".mtx": str = "text/xml"; break;
                case ".mxp": str = "application/x-mmxp"; break;
                case ".net": str = "image/pnetvue"; break;
                case ".nrf": str = "application/x-nrf"; break;
                case ".nws": str = "message/rfc822"; break;
                case ".odc": str = "text/x-ms-odc"; break;
                case ".out": str = "application/x-out"; break;
                case ".p10": str = "application/pkcs10"; break;
                case ".p12": str = "application/x-pkcs12"; break;
                case ".p7b": str = "application/x-pkcs7-certificates"; break;
                case ".p7c": str = "application/pkcs7-mime"; break;
                case ".p7m": str = "application/pkcs7-mime"; break;
                case ".p7r": str = "application/x-pkcs7-certreqresp"; break;
                case ".p7s": str = "application/pkcs7-signature"; break;
                case ".pc5": str = "application/x-pc5"; break;
                case ".pci": str = "application/x-pci"; break;
                case ".pcl": str = "application/x-pcl"; break;
                case ".pcx": str = "application/x-pcx"; break;
                case ".pdf": str = "application/pdf"; break;
                case ".pdx": str = "application/vnd.adobe.pdx"; break;
                case ".pfx": str = "application/x-pkcs12"; break;
                case ".pgl": str = "application/x-pgl"; break;
                case ".pic": str = "application/x-pic"; break;
                case ".pko": str = "application/vnd.ms-pki.pko"; break;
                case ".pl": str = "application/x-perl"; break;
                case ".plg": str = "text/html"; break;
                case ".pls": str = "audio/scpls"; break;
                case ".plt": str = "application/x-plt"; break;
                case ".png": str = "image/png"; break;
                //case ".png": str = "application/x-png"; break;
                case ".pot": str = "application/vnd.ms-powerpoint"; break;
                case ".ppa": str = "application/vnd.ms-powerpoint"; break;
                case ".ppm": str = "application/x-ppm"; break;
                case ".pps": str = "application/vnd.ms-powerpoint"; break;
                case ".ppt": str = "application/vnd.ms-powerpoint"; break;
                //case ".ppt": str = "application/x-ppt"; break;
                case ".pr": str = "application/x-pr"; break;
                case ".prf": str = "application/pics-rules"; break;
                case ".prn": str = "application/x-prn"; break;
                case ".prt": str = "application/x-prt"; break;
                case ".ps": str = "application/x-ps"; break;
                //case ".ps": str = "application/postscript"; break;
                case ".ptn": str = "application/x-ptn"; break;
                case ".pwz": str = "application/vnd.ms-powerpoint"; break;
                case ".r3t": str = "text/vnd.rn-realtext3d"; break;
                case ".ra": str = "audio/vnd.rn-realaudio"; break;
                case ".ram": str = "audio/x-pn-realaudio"; break;
                case ".ras": str = "application/x-ras"; break;
                case ".rat": str = "application/rat-file"; break;
                case ".rdf": str = "text/xml"; break;
                case ".rec": str = "application/vnd.rn-recording"; break;
                case ".red": str = "application/x-red"; break;
                case ".rgb": str = "application/x-rgb"; break;
                case ".rjs": str = "application/vnd.rn-realsystem-rjs"; break;
                case ".rjt": str = "application/vnd.rn-realsystem-rjt"; break;
                case ".rlc": str = "application/x-rlc"; break;
                case ".rle": str = "application/x-rle"; break;
                case ".rm": str = "application/vnd.rn-realmedia"; break;
                case ".rmf": str = "application/vnd.adobe.rmf"; break;
                case ".rmi": str = "audio/mid"; break;
                case ".rmj": str = "application/vnd.rn-realsystem-rmj"; break;
                case ".rmm": str = "audio/x-pn-realaudio"; break;
                case ".rmp": str = "application/vnd.rn-rn_music_package"; break;
                case ".rms": str = "application/vnd.rn-realmedia-secure"; break;
                case ".rmvb": str = "application/vnd.rn-realmedia-vbr"; break;
                case ".rmx": str = "application/vnd.rn-realsystem-rmx"; break;
                case ".rnx": str = "application/vnd.rn-realplayer"; break;
                case ".rp": str = "image/vnd.rn-realpix"; break;
                case ".rpm": str = "audio/x-pn-realaudio-plugin"; break;
                case ".rsml": str = "application/vnd.rn-rsml"; break;
                case ".rt": str = "text/vnd.rn-realtext"; break;
                case ".rtf": str = "application/msword"; break;
                //case ".rtf": str = "application/x-rtf"; break;
                case ".rv": str = "video/vnd.rn-realvideo"; break;
                case ".sam": str = "application/x-sam"; break;
                case ".sat": str = "application/x-sat"; break;
                case ".sdp": str = "application/sdp"; break;
                case ".sdw": str = "application/x-sdw"; break;
                case ".sit": str = "application/x-stuffit"; break;
                case ".slb": str = "application/x-slb"; break;
                case ".sld": str = "application/x-sld"; break;
                case ".slk": str = "drawing/x-slk"; break;
                case ".smi": str = "application/smil"; break;
                case ".smil": str = "application/smil"; break;
                case ".smk": str = "application/x-smk"; break;
                case ".snd": str = "audio/basic"; break;
                case ".sol": str = "text/plain"; break;
                case ".sor": str = "text/plain"; break;
                case ".spc": str = "application/x-pkcs7-certificates"; break;
                case ".spl": str = "application/futuresplash"; break;
                case ".spp": str = "text/xml"; break;
                case ".ssm": str = "application/streamingmedia"; break;
                case ".sst": str = "application/vnd.ms-pki.certstore"; break;
                case ".stl": str = "application/vnd.ms-pki.stl"; break;
                case ".stm": str = "text/html"; break;
                case ".sty": str = "application/x-sty"; break;
                case ".svg": str = "text/xml"; break;
                case ".swf": str = "application/x-shockwave-flash"; break;
                case ".tdf": str = "application/x-tdf"; break;
                case ".tg4": str = "application/x-tg4"; break;
                case ".tga": str = "application/x-tga"; break;
                case ".tif": str = "image/tiff"; break;
                //case ".tif": str = "application/x-tif"; break;
                case ".tiff": str = "image/tiff"; break;
                case ".tld": str = "text/xml"; break;
                case ".top": str = "drawing/x-top"; break;
                case ".torrent": str = "application/x-bittorrent"; break;
                case ".tsd": str = "text/xml"; break;
                case ".txt": str = "text/plain"; break;
                case ".uin": str = "application/x-icq"; break;
                case ".uls": str = "text/iuls"; break;
                case ".vcf": str = "text/x-vcard"; break;
                case ".vda": str = "application/x-vda"; break;
                case ".vdx": str = "application/vnd.visio"; break;
                case ".vml": str = "text/xml"; break;
                case ".vpg": str = "application/x-vpeg005"; break;
                case ".vsd": str = "application/vnd.visio"; break;
                //case ".vsd": str = "application/x-vsd"; break;
                case ".vss": str = "application/vnd.visio"; break;
                //case ".vst": str = "application/vnd.visio"; break;
                case ".vst": str = "application/x-vst"; break;
                case ".vsw": str = "application/vnd.visio"; break;
                case ".vsx": str = "application/vnd.visio"; break;
                case ".vtx": str = "application/vnd.visio"; break;
                case ".vxml": str = "text/xml"; break;
                case ".wav": str = "audio/wav"; break;
                case ".wax": str = "audio/x-ms-wax"; break;
                case ".wb1": str = "application/x-wb1"; break;
                case ".wb2": str = "application/x-wb2"; break;
                case ".wb3": str = "application/x-wb3"; break;
                case ".wbmp": str = "image/vnd.wap.wbmp"; break;
                case ".wiz": str = "application/msword"; break;
                case ".wk3": str = "application/x-wk3"; break;
                case ".wk4": str = "application/x-wk4"; break;
                case ".wkq": str = "application/x-wkq"; break;
                case ".wks": str = "application/x-wks"; break;
                case ".wm": str = "video/x-ms-wm"; break;
                case ".wma": str = "audio/x-ms-wma"; break;
                case ".wmd": str = "application/x-ms-wmd"; break;
                case ".wmf": str = "application/x-wmf"; break;
                case ".wml": str = "text/vnd.wap.wml"; break;
                case ".wmv": str = "video/x-ms-wmv"; break;
                case ".wmx": str = "video/x-ms-wmx"; break;
                case ".wmz": str = "application/x-ms-wmz"; break;
                case ".wp6": str = "application/x-wp6"; break;
                case ".wpd": str = "application/x-wpd"; break;
                case ".wpg": str = "application/x-wpg"; break;
                case ".wpl": str = "application/vnd.ms-wpl"; break;
                case ".wq1": str = "application/x-wq1"; break;
                case ".wr1": str = "application/x-wr1"; break;
                case ".wri": str = "application/x-wri"; break;
                case ".wrk": str = "application/x-wrk"; break;
                case ".ws": str = "application/x-ws"; break;
                case ".ws2": str = "application/x-ws"; break;
                case ".wsc": str = "text/scriptlet"; break;
                case ".wsdl": str = "text/xml"; break;
                case ".wvx": str = "video/x-ms-wvx"; break;
                case ".xdp": str = "application/vnd.adobe.xdp"; break;
                case ".xdr": str = "text/xml"; break;
                case ".xfd": str = "application/vnd.adobe.xfd"; break;
                case ".xfdf": str = "application/vnd.adobe.xfdf"; break;
                case ".xhtml": str = "text/html"; break;
                //case ".xls": str = "application/vnd.ms-excel"; break;
                case ".xls": str = "application/x-xls"; break;
                case ".xlw": str = "application/x-xlw"; break;
                case ".xml": str = "text/xml"; break;
                case ".xpl": str = "audio/scpls"; break;
                case ".xq": str = "text/xml"; break;
                case ".xql": str = "text/xml"; break;
                case ".xquery": str = "text/xml"; break;
                case ".xsd": str = "text/xml"; break;
                case ".xsl": str = "text/xml"; break;
                case ".xslt": str = "text/xml"; break;
                case ".xwd": str = "application/x-xwd"; break;
                case ".x_b": str = "application/x-x_b"; break;
                case ".x_t": str = "application/x-x_t"; break;
            }
            return str;
        }
    }
}
