/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Management;

namespace uoLib.Common
{
    public static partial class Functions
    {
        /// <summary>
        /// 获取物理内存总数(K)
        /// </summary>
        /// <returns></returns>
        public static long GetTotalPhysicalMemory()
        {
            int n = 0; string pName = "TotalPhysicalMemory";
            ManagementObjectCollection qc = null;

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");
            qc = searcher.Get();
            n = qc.Count;
            pName = "TotalVisibleMemorySize";

            if (qc == null) return 0;

            if (qc == null || n > 0)
            {
                foreach (ManagementBaseObject mo in qc)
                {
                    PropertyDataCollection ps = mo.Properties;
                    foreach (PropertyData p in ps)
                    {
                        if (p.Name.Trim().ToLower() == pName.ToLower())
                        {
                            return long.Parse(p.Value.ToString());
                        }
                    }
                }
                return 0;
            }
            else
            {
                return 0;
            }
        }

    }
}
