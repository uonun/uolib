/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Web;

namespace uoLib.Common
{
    /// <summary>
    /// 提供大量常用静态方法
    /// </summary>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public static partial class Functions
    {
        #region 验证
        /// <summary>
        /// 验证是否为纯数字的字符串。不论字符串长度，只要每一位都是数字则返回 true 。
        /// </summary>
        /// <param name="str">待验证的字符串</param>
        /// <returns></returns>
        public static bool IsNumber(String str)
        {
            if (IsNullOrEmptyStr(str)) { return false; }
            for (int i = 0;i < str.Length;i++)
            {
                if (str[i] < '0' || str[i] > '9')
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 验证是否为空字符串。若无需裁切两端空格，建议直接使用 String.IsNullOrEmpty(string)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        /// <remarks>
        /// 不同于String.IsNullOrEmpty(string)，此方法会增加一步Trim操作。如 IsNullOrEmptyStr(" ") 将返回 true。
        /// </remarks>
        public static bool IsNullOrEmptyStr(string str)
        {
            if (string.IsNullOrEmpty(str)) { return true; }
            if (str.Trim().Length == 0) { return true; }
            return false;
        }

        /// <summary>
        /// 验证是否包含特殊字符，常用于用户名字符串的过滤。
        /// </summary>
        /// <remarks><![CDATA[常用于用户名字符串的过滤。过滤的字符串有：“$!?#%@~`&*(){};.:+=\'/|"”以及空格、Tab、制表符、大于号、小于号]]></remarks>
        public static bool IsValidName(string name)
        {
            if (IsNullOrEmptyStr(name)) { return false; }
            else
            {
                IEnumerator ie = name.GetEnumerator();
                while (ie.MoveNext())
                {
                    if (@"$!<>?#%@~`&*(){};.:+=\'/|"" 		".IndexOf((char)ie.Current) != -1)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// 验证手机号码格式是否正确
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static bool IsMobileNum(string mobile)
        {
            if (IsNullOrEmptyStr(mobile)) { return false; }
            else
            {
                Regex re = new Regex(RegexPatterns.MobileNum, RegexOptions.IgnoreCase);
                return IsMatchRegex(mobile, re);
            }
        }

        /// <summary>
        /// 验证邮箱地址的合法性
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsEmail(string email)
        {
            if (IsNullOrEmptyStr(email)) { return false; }
            else
            {
                Regex re = new Regex(RegexPatterns.Email, RegexOptions.IgnoreCase);
                return IsMatchRegex(email, re);
            }
        }

        /// <summary>
        /// 验证是否匹配指定正则表达式
        /// </summary>
        public static bool IsMatchRegex(string str, Regex re)
        {
            if (IsNullOrEmptyStr(str)) { return false; }
            else
            {
                return re.IsMatch(str);
            }
        }

        /// <summary>
        /// 验证是否为http|https协议的URL地址
        /// </summary>
        public static bool IsUrl(string url)
        {
            if (IsNullOrEmptyStr(url)) { return false; }
            else
            {
                Regex re = new Regex(RegexPatterns.URL, RegexOptions.IgnoreCase);
                return re.IsMatch(url);
            }
        }

        /// <summary>
        /// 验证字符串是否符合IPv4格式
        /// </summary>
        public static bool IsIP(string ip)
        {
            if (IsNullOrEmptyStr(ip)) { return false; }
            else
            {
                Regex re = new Regex(RegexPatterns.IPv4, RegexOptions.IgnoreCase);
                return re.IsMatch(ip);
            }
        }

        /// <summary>
        /// 根据文件夹命名规则验证字符串是否符合文件夹格式
        /// </summary>
        public static bool IsFolderName(string folderName)
        {
            if (IsNullOrEmptyStr(folderName)) { return false; }
            else
            {
                // 不能以 “.” 开头
                folderName = folderName.Trim().ToLower();

                // “nul”、“aux”、“con”、“com1”、“lpt1”不能为文件夹/文件的名称
                // 作为文件夹，只需满足名称不为这几个就行。
                switch (folderName)
                {
                    case "nul":
                    case "aux":
                    case "con":
                    case "com1":
                    case "lpt1":
                        return false;
                    default:
                        break;
                }

                Regex re = new Regex(RegexPatterns.FolderName, RegexOptions.IgnoreCase);
                return re.IsMatch(folderName);
            }
        }

        /// <summary>
        /// 根据文件名命名规则验证字符串是否符合文件名格式
        /// </summary>
        public static bool IsFileName(string fileName)
        {
            if (IsNullOrEmptyStr(fileName)) { return false; }
            else
            {
                fileName = fileName.Trim().ToLower();
                // 不能以 “.” 开头
                // 作为文件名，第一个“.” 之前不能是“nul”、“aux”、“con”、“com1”、“lpt1”
                if (fileName.StartsWith(".")
                    || fileName.StartsWith("nul.")
                    || fileName.StartsWith("aux.")
                    || fileName.StartsWith("con.")
                    || fileName.StartsWith("com1.")
                    || fileName.StartsWith("lpt1.")
                    ) return false;

                Regex re = new Regex(RegexPatterns.FileName, RegexOptions.IgnoreCase);
                return re.IsMatch(fileName);
            }
        }

        /// <summary>
        /// 验证是否为合法的RGB颜色字符串
        /// </summary>
        /// <param name="color">RGB颜色，如：#00ccff | #039 | ffffcc</param>
        /// <returns></returns>
        public static bool IsRGBColor(string color)
        {
            if (IsNullOrEmptyStr(color)) { return false; }
            else
            {
                Regex re = new Regex(RegexPatterns.HtmlColor, RegexOptions.IgnoreCase);
                return re.IsMatch(color);
            }
        }

        /// <summary>
        /// 检查字符串中是否包含非法关键字
        /// </summary>
        /// <param name="str">待检查的字符串，为空或null时将返回false;</param>
        /// <param name="badWords">非法关键字，为空或null时将返回false;</param>
        /// <param name="comparisonType"></param>
        /// <returns>
        /// </returns>
        public static bool IsContainBadWord(string str, string[] badWords, StringComparison comparisonType)
        {
            if (string.IsNullOrEmpty(str)) return false;
            if (badWords == null || badWords.Length < 1) return false;

            str = str.ToLower();
            foreach (string s in badWords)
            {
                if (str.IndexOf(s.ToLower(), 0, comparisonType) > -1) return true;
            }
            return false;
        }

        #endregion

        #region 获取及格式化

        /// <summary>
        /// 将以字节表示的文件大小格式化为对用户友好的字符串表示。
        /// </summary>
        /// <param name="sizeAsBytes">要格式化的数字（字节）</param>
        /// <remarks>
        /// <para>
        /// 此方法可以讲以字节数表示的文件大小格式化为对用户友好的字符串表示，比如对于大小为 3,032,576 字节的文件，使用此方法可以将之格式化表示为 2.89MB。
        /// 若需要控制小数点后保留的精度，请使用重载<see cref="uoLib.Common.Functions.FormatFileSize(long, int)"/>。
        /// </para>
        /// 
        /// <para>
        /// 相反地，<see cref="uoLib.Common.Functions.GetFileSizeFromString(string)"/> 可以将形如 2.89MB 的字符串还原为以字节位单位的文件大小（长整形）。
        /// 但需注意的是：<see cref="uoLib.Common.Functions.GetFileSizeFromString(string)"/> 可能因为小数位数保留不够而丧失精度，这一点应当尤为注意！
        /// 比如对于大小为 2.89 MB (3,032,576 字节) 的文件：
        /// </para>
        /// <code>
        /// long size = 3032576;
        /// string formatedSizeA = Functions.FormatFileSize(size);			// 2.89MB
        /// string formatedSizeB = Functions.FormatFileSize(size, 6);		// 2.892090MB
        /// long newSizeA = Functions.GetFileSizeFromString(formatedSizeA);	// 3030384
        /// long newSizeB = Functions.GetFileSizeFromString(formatedSizeB);	// 3032576
        /// </code>
        /// </remarks>
        /// <returns></returns>
        public static string FormatFileSize(long sizeAsBytes)
        {
            return FormatFileSize(sizeAsBytes, 2);
        }
        /// <summary>
        /// 将以字节表示的文件大小格式化为对用户友好的字符串表示。
        /// </summary>
        /// <param name="sizeAsBytes">要格式化的数字（字节）</param>
        /// <param name="decimals">小数点后保留的位数</param>
        /// <remarks>
        /// 参见：<see cref="uoLib.Common.Functions.FormatFileSize(long)"/>
        /// </remarks>
        /// <returns></returns>
        public static string FormatFileSize(long sizeAsBytes, int decimals)
        {
            StringBuilder sb = new StringBuilder();
            if (sizeAsBytes >= 1099511627776)
            {
                sb.Append(Math.Round((decimal)sizeAsBytes / 1099511627776, decimals).ToString());
                sb.Append("TB");
            }
            else if (sizeAsBytes >= 1073741824)
            {
                sb.Append(Math.Round((decimal)sizeAsBytes / 1073741824, decimals).ToString());
                sb.Append("GB");
            }
            else if (sizeAsBytes >= 1048576)
            {
                sb.Append(Math.Round((decimal)sizeAsBytes / 1048576, decimals).ToString());
                sb.Append("MB");
            }
            else if (sizeAsBytes >= 1024)
            {
                sb.Append(Math.Round((decimal)sizeAsBytes / 1024, decimals).ToString());
                sb.Append("KB");
            }
            else
            {
                sb.Append(sizeAsBytes.ToString());
                sb.Append("Bytes");
            }
            return sb.ToString();
        }
        /// <summary>
        /// 将形如 10.1MB 格式对用户友好的文件大小字符串还原成真实的文件大小，单位为字节。
        /// </summary>
        /// <param name="formatedSize">形如 10.1MB 格式的文件大小字符串</param>
        /// <remarks>
        /// 参见：<see cref="uoLib.Common.Functions.FormatFileSize(long)"/>
        /// </remarks>
        /// <returns></returns>
        public static long GetFileSizeFromString(string formatedSize)
        {
            if (IsNullOrEmptyStr(formatedSize)) throw new ArgumentNullException("formatedSize");

            long size;
            if (long.TryParse(formatedSize, out size)) return size;

            //去掉数字分隔符
            formatedSize = formatedSize.Replace(",", "");

            Regex re = new Regex(@"^([\d\.]+)((?:TB|GB|MB|KB|Bytes))$");
            if (re.IsMatch(formatedSize))
            {
                MatchCollection mc = re.Matches(formatedSize);
                Match m = mc[0];
                double s = double.Parse(m.Groups[1].Value);

                switch (m.Groups[2].Value)
                {
                    case "TB":
                        s *= 1099511627776;
                        break;
                    case "GB":
                        s *= 1073741824;
                        break;
                    case "MB":
                        s *= 1048576;
                        break;
                    case "KB":
                        s *= 1024;
                        break;
                }

                size = (long)s;
                return size;
            }

            throw new ArgumentException("formatedSize");
        }

        /// <summary>
        /// 过滤特殊字符（“'"*?().”以及Tab、换行、回车、大于号、小于号）
        /// </summary>
        /// <remarks></remarks>
        public static string ReplaceBadChar(string str)
        {
            if (IsNullOrEmptyStr(str)) { return string.Empty; }
            else
            {
                StringBuilder sb = new StringBuilder(str);
                sb.Replace("'", "");
                sb.Replace("*", "");
                sb.Replace("?", "");
                sb.Replace("(", "");
                sb.Replace(")", "");
                sb.Replace("<", "");
                sb.Replace(">", "");
                sb.Replace(".", "");
                sb.Replace(Char.ConvertFromUtf32(9), "");   //Tab
                sb.Replace(Char.ConvertFromUtf32(10), "");  //换行
                sb.Replace(Char.ConvertFromUtf32(13), "");  //回车
                sb.Replace(Char.ConvertFromUtf32(34), "");  //双引号
                return sb.ToString();
            }
        }

        /// <summary>
        /// 过滤空白字符
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <param name="replaceTo">用于替换空白字符的字符串</param>
        /// <returns></returns>
        public static string ReplaceWitheChar(string str, string replaceTo)
        {
            if (string.IsNullOrEmpty(str)) { return string.Empty; }

            Regex re = new Regex(@"\s+", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            str = re.Replace(str, replaceTo);
            return str;
        }

        /// <summary>
        /// 获取由数字构成的随机字符串。首位不为“0”。
        /// </summary>
        /// <param name="len">字符串长度</param>
        /// <returns></returns>
        public static string GetRandomNum(int len)
        {
            if (len <= 0) { return null; }
            StringBuilder sb = new StringBuilder();
            //使用Guid.NewGuid().GetHashCode()作为种子，可以确保Random在极短时间产生的随机数尽可能做到不重复    
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            do { sb.Append(rand.Next(0, 9)); }
            while (sb.Length < len);

            //第一位为 0 就去掉再补一位
            if (sb[0] == '0')
            {
                sb.Remove(0, 1);
                sb.Insert(0, rand.Next(1, 9));
            }

            return sb.ToString();
        }

        /// <summary>
        /// 将HTML字符串格式化为JavaScirpt字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string HtmlToJs(string str)
        {
            if (IsNullOrEmptyStr(str)) { return string.Empty; }
            else
            {
                StringBuilder sb = new StringBuilder(str);
                sb.Replace(Char.ConvertFromUtf32(34), @"\" + Char.ConvertFromUtf32(34));
                sb.Replace(@"\n", @"\\n");
                sb.Replace(Environment.NewLine, @"\n");
                sb.Replace(@"\r", @"\\r");
                sb.Replace(Char.ConvertFromUtf32(9), "\r");
                sb.Replace(@"\/", @"\\/");
                sb.Replace(@"\'", @"\\\'");
                return sb.ToString();
            }
        }
        /// <summary>
        /// 获取可用于文件夹名称的安全字符串，确保该字符串中不含文件夹名称不允许的字符。
        /// 注意：“con”也不能用于文件夹名，但此处未验证
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public static string GetValidFolderName(string folderName)
        {
            if (IsNullOrEmptyStr(folderName)) { return null; }
            else
            {
                Regex re = new Regex(RegexPatterns.FolderName, RegexOptions.IgnoreCase);
                folderName = re.Replace(folderName, string.Empty);
                return folderName;
            }
        }
        /// <summary>
        /// 获取可用于文件名的安全字符串，确保该字符串中不含文件名不允许的字符。
        /// 注意：“con”也不能用于文件名，但此处未验证
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetValidFileName(string fileName)
        {
            if (IsNullOrEmptyStr(fileName)) { return null; }
            else
            {
                Regex re = new Regex(RegexPatterns.FileName, RegexOptions.IgnoreCase);
                fileName = re.Replace(fileName, string.Empty);
                return fileName;
            }
        }

        /// <summary>
        /// 将HTML代码替换为页面文本形式。
        /// </summary>
        /// <param name="str"></param>
        /// <remarks>
        /// <![CDATA[替换了：&、>、<、'、"、Tab、空格、换行符、回车符。]]>
        /// </remarks>
        public static string HtmlEnCode(string str)
        {
            if (IsNullOrEmptyStr(str)) { return string.Empty; }
            else
            {
                StringBuilder sb = new StringBuilder(str);
                sb.Replace(@"&", @"&amp;");
                sb.Replace(@">", @"&gt;");
                sb.Replace(@"<", @"&lt;");
                sb.Replace(Char.ConvertFromUtf32(34), @"&quot;");
                sb.Replace(Char.ConvertFromUtf32(39), @"&#39;");
                sb.Replace("\r\n", @"<br />");
                return sb.ToString();
            }
        }
        /// <summary>
        /// 将页面文本形式字符串还原成HTML代码。
        /// </summary>
        /// <param name="str"></param>
        /// <remarks>
        /// <![CDATA[替换了：&、>、<、'、"、Tab、空格、换行符、回车符。]]>
        /// </remarks>
        public static string HtmlDeCode(string str)
        {
            if (IsNullOrEmptyStr(str)) { return string.Empty; }
            else
            {
                StringBuilder sb = new StringBuilder(str);
                sb.Replace(@"&amp;", @"&");
                sb.Replace(@"&gt;", @">");
                sb.Replace(@"&lt;", @"<");
                sb.Replace(@"&#39;", Char.ConvertFromUtf32(39));
                sb.Replace(@"<br />", "\r\n");
                return sb.ToString();
            }
        }

        /// <summary>
        /// 过滤HTML标签
        /// </summary>
        public static string HtmlFilter(string str)
        {
            if (IsNullOrEmptyStr(str)) { return string.Empty; }
            else
            {
                Regex re = new Regex(RegexPatterns.HtmlTag, RegexOptions.IgnoreCase);
                return re.Replace(str, "");
            }
        }
        /// <summary>
        /// 裁切字符串（中文按照两个字符计算）
        /// </summary>
        /// <param name="str">旧字符串</param>
        /// <param name="len">新字符串长度</param>
        /// <param name="HtmlEnable">为 false 时过滤 Html 标签后再进行裁切，反之则保留 Html 标签。</param>
        /// <remarks>
        /// <para>注意：<ol>
        /// <li>若字符串被截断则会在末尾追加“...”，反之则直接返回原始字符串。</li>
        /// <li>参数 <paramref name="HtmlEnable"/> 为 false 时会先调用<see cref="uoLib.Common.Functions.HtmlFilter"/>过滤掉 Html 标签再进行裁切。</li>
        /// <li>中文按照两个字符计算。若指定长度位置恰好只获取半个中文字符，则会将其补全，如下面的例子：<br/>
        /// <code><![CDATA[
        /// string str = "感谢使用uoLib。";
        /// string A = CutStr(str,4);   // A = "感谢..."
        /// string B = CutStr(str,5);   // B = "感谢使..."
        /// ]]></code></li>
        /// </ol>
        /// </para>
        /// </remarks>
        public static string CutStr(string str, int len, bool HtmlEnable)
        {
            if (str == null || str.Length == 0 || len <= 0) { return string.Empty; }

            if (HtmlEnable == false) str = HtmlFilter(str);
            int l = str.Length;

            #region 计算长度
            int clen = 0;//当前长度 
            while (clen < len && clen < l)
            {
                //每遇到一个中文，则将目标长度减一。
                if ((int)str[clen] > 128) { len--; }
                clen++;
            }
            #endregion

            if (clen < l)
            {
                return str.Substring(0, clen) + "...";
            }
            else
            {
                return str;
            }
        }
        /// <summary>
        /// 裁切字符串（中文按照两个字符计算，裁切前会先过滤 Html 标签）
        /// </summary>
        /// <param name="str">旧字符串</param>
        /// <param name="len">新字符串长度</param>
        /// <remarks>
        /// <para>注意：<ol>
        /// <li>若字符串被截断则会在末尾追加“...”，反之则直接返回原始字符串。</li>
        /// <li>中文按照两个字符计算。若指定长度位置恰好只获取半个中文字符，则会将其补全，如下面的例子：<br/>
        /// <code><![CDATA[
        /// string str = "感谢使用uoLib模块。";
        /// string A = CutStr(str,4);   // A = "感谢..."
        /// string B = CutStr(str,5);   // B = "感谢使..."
        /// ]]></code></li>
        /// </ol>
        /// </para>
        /// </remarks>
        public static string CutStr(string str, int len)
        {
            if (IsNullOrEmptyStr(str)) { return string.Empty; }
            else
            {
                return CutStr(str, len, false);
            }
        }

        /// <summary>
        /// 获取字符串长度。与string.Length不同的是，该方法将中文作 2 个字符计算。
        /// </summary>
        /// <param name="str">目标字符串</param>
        /// <returns></returns>
        public static int GetLength(string str)
        {
            if (str == null || str.Length == 0) { return 0; }

            int l = str.Length;
            int realLen = l;

            #region 计算长度
            int clen = 0;//当前长度 
            while (clen < l)
            {
                //每遇到一个中文，则将实际长度加一。
                if ((int)str[clen] > 128) { realLen++; }
                clen++;
            }
            #endregion

            return realLen;
        }

        /// <summary>
        /// 将IPv4格式的字符串转换为int型表示
        /// </summary>
        /// <param name="strIPAddress">IPv4格式的字符</param>
        /// <returns></returns>
        public static int IPToNumber(string strIPAddress)
        {
            //将目标IP地址字符串strIPAddress转换为数字 
            string[] arrayIP = strIPAddress.Split('.');
            int sip1 = Int32.Parse(arrayIP[0]);
            int sip2 = Int32.Parse(arrayIP[1]);
            int sip3 = Int32.Parse(arrayIP[2]);
            int sip4 = Int32.Parse(arrayIP[3]);
            int tmpIpNumber;
            tmpIpNumber = sip1 * 256 * 256 * 256 + sip2 * 256 * 256 + sip3 * 256 + sip4;
            return tmpIpNumber;
        }

        /// <summary>
        /// 将int型表示的IP还原成正常IPv4格式。
        /// </summary>
        /// <param name="intIPAddress">int型表示的IP</param>
        /// <returns></returns>
        public static string NumberToIP(int intIPAddress)
        {
            int tempIPAddress;
            //将目标整形数字intIPAddress转换为IP地址字符串 
            //-1062731518 192.168.1.2 
            //-1062731517 192.168.1.3 
            if (intIPAddress >= 0)
            {
                tempIPAddress = intIPAddress;
            }
            else
            {
                tempIPAddress = intIPAddress + 1;
            }
            int s1 = tempIPAddress / 256 / 256 / 256;
            int s21 = s1 * 256 * 256 * 256;
            int s2 = (tempIPAddress - s21) / 256 / 256;
            int s31 = s2 * 256 * 256 + s21;
            int s3 = (tempIPAddress - s31) / 256;
            int s4 = tempIPAddress - s3 * 256 - s31;
            if (intIPAddress < 0)
            {
                s1 = 255 + s1;
                s2 = 255 + s2;
                s3 = 255 + s3;
                s4 = 255 + s4;
            }
            string strIPAddress = s1.ToString() + "." + s2.ToString() + "." + s3.ToString() + "." + s4.ToString();
            return strIPAddress;
        }

        #endregion

        #region WebRequest
        /// <summary>
        /// 使用HttpWebRequest发送HTTP请求，同时支持GET/POST方式提交。
        /// </summary>
        /// <param name="url">要访问的URL地址</param>
        /// <param name="queryString"><![CDATA[QueryString形式的数据，如：action=1&id=2]]></param>
        /// <param name="method">任何 HTTP 1.1 协议谓词：GET、HEAD、POST、PUT、DELETE、TRACE 或 OPTIONS。</param>
        /// <param name="encoding">页面使用的编码</param>
        /// <remarks>
        /// <para>
        /// 请注意：
        /// <ol>
        /// <li><paramref name="queryString"/> 中如果有中文，则需要使用 Uri.EscapeDataString(string) 或 Uri.EscapeUriString(string) 进行对应转换。</li>
        /// <li>理论上支持任何 HTTP 1.1 协议谓词，不过就实际使用情况来说，仅对GET/POST方式进行了测试，其他谓词并未测试。</li>
        /// </ol>
        /// </para>
        /// </remarks>
        /// <returns></returns>
        public static string GetWebRequest(string url, string queryString, string method, Encoding encoding)
        {
            if (string.IsNullOrEmpty(url)) { throw new ArgumentException("网络地址无效：" + url, "url"); }
            if (!IsMethodOk(method)) { throw new ArgumentException("Http协议谓词无效：" + method, "method"); }

            Uri uri;
            try
            {
                uri = new Uri(url, UriKind.RelativeOrAbsolute);
                if (!uri.IsAbsoluteUri)
                {
                    Uri baseUri = HttpContext.Current.Request.Url;
                    uri = new Uri(baseUri, uri);
                }
            }
            catch
            {
                throw new ArgumentException("网络地址无效：" + url, "url");
            }

            HttpWebRequest request;
            if (uri.IsAbsoluteUri)
            {
                UriBuilder ub = new UriBuilder(uri);
                queryString = (queryString ?? string.Empty).Trim();
                if (!string.IsNullOrEmpty(queryString))
                {
                    if (string.IsNullOrEmpty(ub.Query))
                    {
                        ub.Query = queryString;
                    }
                    else
                    {
                        ub.Query = ub.Query.TrimStart('?') + "&" + queryString.TrimStart('?');
                    }
                }
                request = (HttpWebRequest)HttpWebRequest.Create(ub.Uri);
            }
            else
            {
                throw new ArgumentException("网络地址无效：" + url, "url");
            }

            string html = string.Empty;
            try
            {
                request.UserAgent = "Mozilla/4.0 (compatible; uoLib " + uoLib.Info.Version + "; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022)";
                request.Method = method;

                if (method == "POST")
                {
                    byte[] data = encoding.GetBytes(queryString);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;

                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(data, 0, data.Length);
                        reqStream.Close();
                    }
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response != null)
                    {
                        if (response.StatusCode == HttpStatusCode.OK && request.HaveResponse)
                        {
                            using (StreamReader sr = new StreamReader(response.GetResponseStream(), encoding))
                            {
                                if (sr != null)
                                {
                                    html = sr.ReadToEnd();
                                }
                            }
                        }
                        response.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                html = ex.ToString();
            }
            return html;
        }

        /// <summary>
        /// 检查指定字符串是否是 HTTP 1.1 协议谓词
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        private static bool IsMethodOk(string method)
        {
            if (IsNullOrEmptyStr(method)) return false;
            switch (method.ToUpper())
            {
                case "GET":
                case "HEAD":
                case "POST":
                case "PUT":
                case "DELETE":
                case "TRACE":
                case "OPTIONS":
                    return true;
                default:
                    return false;
            }
        }
        #endregion

        /// <summary>
        /// 递归指定异常的内部异常（包括该异常本身）
        /// </summary>
        /// <param name="ex">指定的异常</param>
        /// <param name="exStack">包所有内部异常的集合</param>
        /// <remarks>
        /// 当前异常先于其内部异常被插入到 Stack 集合。
        /// </remarks>
        public static void GetErrorStack(Exception ex, Stack<Exception> exStack)
        {
            if (ex == null) return;
            exStack.Push(ex);

            if (ex.InnerException != null)
            {
                //exStack.Push(ex.InnerException);
                GetErrorStack(ex.InnerException, exStack);
            }
        }

        /// <summary>
        /// 获取安全的文件夹名字符串
        /// </summary>
        /// <param name="foldName"></param>
        /// <returns></returns>
        public static string GetSafeFolderName(string foldName)
        {
            if (string.IsNullOrEmpty(foldName)) throw new ArgumentException("文件名不能为空！", "filename");
            foldName = foldName.Trim();
            if (IsFolderName(foldName)) return foldName;
            //另：可基于Path.GetInvalidPathChars来判断，有待验证两个方法的效率
            Regex re = new Regex("[\\\\/\\?\\*\"><:|]*", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
            string result = re.Replace(foldName, "");
            if (!string.IsNullOrEmpty(result)) return result;
            throw new Exception("无法获取可用的文件夹名！");
        }

        /// <summary>
        /// 获取安全的文件名字符串
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string GetSafeFileName(string filename)
        {
            if (string.IsNullOrEmpty(filename)) throw new ArgumentException("文件名不能为空！", "filename");
            filename = filename.Trim();
            if (IsFileName(filename)) return filename;
            //另：可基于Path.GetInvalidFileNameChars来判断，有待验证两个方法的效率
            Regex re = new Regex("[\\\\/\\?\\*\"><:|]*", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
            string result = re.Replace(filename, "").Trim(new char[] { '.' });
            if (!string.IsNullOrEmpty(result)) return result;
            throw new Exception("无法获取可用的文件名！");
        }

        #region 数组
        /// <summary>
        /// 将一个数组中的部分抽取为一个新的数组
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static T[] Slice<T>(T[] source, int index, int count)
        {
            T[] result = new T[count];
            Array.Copy(source, index, result, 0, count);
            return result;
        }

        /// <summary>
        /// 反转一个数组元素的排列顺序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T[] Reverse<T>(T[] source)
        {
            Array.Reverse(source);
            return source;
        }
        #endregion
    }
}
