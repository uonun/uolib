﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace uoLib.Security
{
    /// <summary>
    /// DES 加密解密类
    /// </summary>
    public class DES
    {
        /// <summary>
        /// DES解密字符串；
        /// </summary>
        /// <param name="key">DES密钥，至少16位字符；</param>
        /// <param name="cypherText">加密后的字符串；</param>
        /// <returns>DES解密后的字符串；</returns>
        public static string DecryptStr(string key, string cypherText)
        {
            if (string.IsNullOrEmpty(key) || key.Length < 16) throw new ArgumentOutOfRangeException("key", "DES密钥长度过短（至少16位字符）");

            if (string.IsNullOrEmpty(cypherText)) return cypherText;

            StringBuilder temp = new StringBuilder(255);
            Byte[] rgbIV;
            Byte[] rgbKey;
            Byte[] EncryptByte;
            rgbIV = System.Text.ASCIIEncoding.ASCII.GetBytes(key.Substring(0, 8));
            rgbKey = System.Text.ASCIIEncoding.ASCII.GetBytes(key.Substring(8, 8));
            try
            {
                EncryptByte = Convert.FromBase64String(cypherText);
            }
            catch
            {
                return cypherText;
            }
            try
            {
                DESCryptoServiceProvider cryptoserver = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream(EncryptByte);
                CryptoStream encStream = new CryptoStream(ms, cryptoserver.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Read);
                StreamReader sr = new StreamReader(encStream);
                String val = sr.ReadLine();
                sr.Close();
                encStream.Close();
                ms.Close();
                return val;
            }
            catch
            {
                return cypherText;
            }
        }


        /// <summary>
        /// DES加密字符串；
        /// </summary>
        /// <param name="key">DES密钥，至少16位字符；</param>
        /// <param name="plainText">需要加密的字符串；</param>
        /// <returns>DES加密后的字符串；</returns>
        public static string EncryptStr(string key, string plainText)
        {
            if (string.IsNullOrEmpty(plainText)) return plainText;
            if (string.IsNullOrEmpty(key) || key.Length < 16) throw new ArgumentOutOfRangeException("key", "DES密钥长度过短（至少16位字符）");

            StringBuilder temp = new StringBuilder(255);
            Byte[] rgbIV;
            Byte[] rgbKey;
            rgbIV = System.Text.ASCIIEncoding.ASCII.GetBytes(key.Substring(0, 8));
            rgbKey = System.Text.ASCIIEncoding.ASCII.GetBytes(key.Substring(8, 8));
            try
            {
                DESCryptoServiceProvider cryptoserver = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream encStream = new CryptoStream(ms, cryptoserver.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                StreamWriter sw = new StreamWriter(encStream);
                sw.WriteLine(plainText);
                sw.Close();
                encStream.Close();
                Byte[] buffer = ms.ToArray();
                ms.Close();
                return Convert.ToBase64String(buffer, 0, buffer.Length);
            }
            catch
            {
                return plainText;
            }
        }
    }
}
