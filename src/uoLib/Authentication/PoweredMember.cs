﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections;
using System.Text;

namespace uoLib.Authentication
{
    /// <summary>
    /// 角色权限对象，拥有指定权限。（可支持64个独立的权限项）
    /// </summary>
    /// <remarks></remarks>
    [Obsolete("此类将在以后的版本取消，请使用相同功能的 uoLib.Common.FlagBehavior<T> 。")]
    public class PoweredMember<T> : Common.FlagBehavior<T>
        where T : IComparable, IConvertible, IFormattable
    {
        #region 属性
        /// <summary>
        /// 获取当前对象的权限
        /// </summary>
        public T Power
        {
            get { return base.Flag; }
        }

        /// <summary>
        /// 获取当前对象权限的二进制字符串形式
        /// </summary>
        public string PowerString
        {
            get
            {
                return base.FlagString;
            }
        }
        /// <summary>
        /// 获取当前对象权限的长整形形式
        /// </summary>
        public ulong PowerUInt64 { get { return base.FlagU8; } }
        #endregion

        #region 构造函数
        /// <summary>
        /// 构造一个角色权限对象，无任何权限。
        /// </summary>
        public PoweredMember() : base() { }
        /// <summary>
        /// 构造一个角色权限对象，权限由权限枚举指定。
        /// </summary>
        /// <param name="ownedPower">该角色拥有的权限。如：“<typeparamref name="T"/>.查看用户 | <typeparamref name="T"/>.增改用户”</param>
        public PoweredMember(T ownedPower) : base(ownedPower) { }
        /// <summary>
        /// 构造一个角色权限对象，权限由权限标识指定（标识将被转换为二进制形式）。
        /// </summary>
        /// <param name="ownedPower">该角色拥有的权限（标识，将被转换为二进制形式）</param>
        public PoweredMember(ulong ownedPower) : base(ownedPower) { }
        /// <summary>
        /// 构造一个角色权限对象，权限由二进制字符串指定。
        /// </summary>
        /// <param name="ownedPower">该角色拥有的权限（二进制如：“10000100000”）</param>
        public PoweredMember(string ownedPower) : base(ownedPower) { }
        #endregion

        #region 重载
        /// <summary>
        /// 查看当前对象的权限详情
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("角色权限");
            sb.AppendLine("-------------------------------------");
            sb.AppendLine("权限标识（ulong）：" + this.PowerUInt64);
            sb.AppendLine("二进制字符串：" + this.PowerString);

            sb.AppendLine("权限详情：（" + this.Power + "）");
            Array powers = Enum.GetValues(typeof(T));
            IEnumerator ie = powers.GetEnumerator();
            T tmpP;
            while (ie.MoveNext())
            {
                tmpP = (T)Enum.Parse(typeof(T), ie.Current.ToString());
                if (Check(tmpP))
                {
                    sb.AppendLine("\t允许：" + tmpP.ToString());
                }
                else
                {
                    sb.AppendLine("\t禁止：" + tmpP.ToString());
                }
            }
            return sb.ToString();
        }
        #endregion
    }
}
