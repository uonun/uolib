/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace uoLib.Common
{
    //注意：由于此为静态类，其下全是静态方法，因此其中所有引用的Page实例必须在方法内部声明
    //否则点击网页后退按钮后，Page实例将会丢失，将会抛出“服务器无法刷新已完成的响应。”的错误！
    public static partial class Functions
    {
        /// <summary>
        /// 验证是否来自于远程提交
        /// </summary>
        /// <remarks>通过 HTTP_REFERER 来验证，严格地讲此验证可以被伪装欺骗。</remarks>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static bool IsRemotePosting()
        {
            HttpContext Page = HttpContext.Current;
            string s1 = Page.Request.ServerVariables.Get("HTTP_REFERER").ToLower().Trim();
            string s2 = Page.Request.ServerVariables.Get("SERVER_NAME").ToLower().Trim();
            if (s1.Substring(7, s2.Length) == s2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// 返回浏览者的IP地址。若无法取得，则返回空字符串。
        /// </summary>
        /// <returns></returns>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static string GetIP()
        {
            HttpContext Page = HttpContext.Current;
            string ip = Page.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
            if (Functions.IsNullOrEmptyStr(ip) == true)
            {
                ip = Page.Request.ServerVariables.Get("REMOTE_ADDR");
            }
            if (Functions.IsNullOrEmptyStr(ip) == true)
            {
                ip = Page.Request.UserHostAddress;
            }
            return ip;
        }

        #region 根据Value选定TreeView的默认节点
        /// <summary>
        /// 根据Value选定TreeView的默认节点
        /// </summary>
        /// <param name="tv">目标TreeView</param>
        /// <param name="sNodeValue">要选定的值</param>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static void SelectNodeByValue(TreeView tv, string sNodeValue)
        {
            if (tv == null) return;
            foreach (TreeNode tRoot in tv.Nodes)
            {
                TreeNode tTmp = null;
                tTmp = FindNode(tRoot, sNodeValue);
                if (tTmp != null)
                    tTmp.Select();
            }
        }
        /// <summary>
        /// 递归查找父节点
        /// </summary>
        /// <param name="tnParent">指定一个根节点，然后遍历它</param>
        /// <param name="strValue">所要查找的节点的value</param>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        private static TreeNode FindNode(TreeNode tnParent, string strValue)
        {
            if (tnParent == null) return null;
            if (tnParent.Value == strValue) return tnParent;
            TreeNode tnRet = null;
            foreach (TreeNode tn in tnParent.ChildNodes)
            {
                tnRet = FindNode(tn, strValue);
                if (tnRet != null) break;
            }
            return tnRet;
        }
        #endregion

        #region ReportMsg
        /// <summary>
        /// 返回能警告框的JavaScript代码
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。</remarks>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static string ReportMsgStr(string msg)
        {
            HttpContext Page = HttpContext.Current;
            msg = Functions.HtmlToJs(msg);
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n<!--\n");
            sb.Append(string.Format("alert(\"{0}\");", msg));
            sb.Append("\n//-->\n</script>");
            return sb.ToString();
        }
        /// <summary>
        /// 返回能警告框的JavaScript代码
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。</remarks>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static string ReportMsgStr(string msg, string backUrl)
        {
            //兼容。如果 backUrl 为字符串类型的数字，比如 “-1”，仍然按照数字执行，即后退一页。
            int historyPageIndex;
            if (Int32.TryParse(backUrl, out historyPageIndex)) { return ReportMsgStr(msg, historyPageIndex); }

            HttpContext Page = HttpContext.Current;
            msg = Functions.HtmlToJs(msg);
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n<!--\n");
            sb.Append(string.Format("alert(\"{0}\");", msg));
            sb.Append(string.Format("location.href=\"{0}\";", backUrl));
            sb.Append("\n//-->\n</script>");
            return sb.ToString();
        }
        /// <summary>
        /// 返回能警告框的JavaScript代码
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。</remarks>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static string ReportMsgStr(string msg, int historyPageIndex)
        {
            HttpContext Page = HttpContext.Current;
            msg = Functions.HtmlToJs(msg);
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n<!--\n");
            sb.Append(string.Format("alert(\"{0}\");", msg));
            sb.Append(string.Format("history.go({0});", historyPageIndex));
            sb.Append("\n//-->\n</script>");
            return sb.ToString();
        }

        //对 End、Redirect 和 Transfer 方法的调用在当前响应提前结束时会引发一个 ThreadAbortException 异常。
        //具体为，将这三个方法放到try{...}catch{...}模块中将会引发该异常。
        //ms-help://MS.MSDNQTR.v90.chs/fxref_system.web/html/34c7664a-6d70-54b9-21c4-cbb29530bd4e.htm
        //http://msdn.microsoft.com/zh-cn/library/system.web.httpresponse.end(VS.80).aspx

        /// <summary>
        /// 向页面输出JavaScript格式的警告框
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。</remarks>
        /// <param name="msg">警告框消息</param>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static void ReportMsg(string msg)
        {
            HttpContext Page = HttpContext.Current;
            Page.Response.Write(ReportMsgStr(msg));
        }

        /// <summary>
        /// 向页面输出JavaScript格式的警告框
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。
        /// <br />
        /// <strong>注意：</strong><br />
        /// 对 End、Redirect 和 Transfer 方法的调用在当前响应提前结束时会引发一个 ThreadAbortException 异常。
        /// 祥见：<a href="http://msdn.microsoft.com/zh-cn/library/system.web.httpresponse.end(VS.80).aspx" target="_blank">http://msdn.microsoft.com/zh-cn/library/system.web.httpresponse.end(VS.80).aspx</a>
        /// </remarks>
        /// <param name="msg">警告框消息</param>
        /// <param name="isEndTheResponse">为 true 则调用Page.Response.End();终止页面输出。</param>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static void ReportMsg(string msg, bool isEndTheResponse)
        {
            HttpContext Page = HttpContext.Current;
            Page.Response.Write(ReportMsgStr(msg));
            if (isEndTheResponse && Page.Response.IsClientConnected)
            {
                try
                {
                    Page.Response.End();
                }
                catch (ThreadAbortException)
                {
                    Thread.ResetAbort();
                }
            }
        }
        /// <summary>
        /// 向页面输出JavaScript格式的警告框
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。
        /// <br />
        /// 此方法将在输出相关Javascript代码后自动调用Page.Response.End();终止页面输出。
        /// </remarks>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static void ReportMsg(string msg, string backUrl)
        {
            HttpContext Page = HttpContext.Current;
            Page.Response.Write(ReportMsgStr(msg, backUrl));

            if (Page.Response.IsClientConnected)
            {
                try
                {
                    Page.Response.End();
                }
                catch (ThreadAbortException)
                {
                    Thread.ResetAbort();
                }
            }
        }
        /// <summary>
        /// 向页面输出JavaScript格式的警告框
        /// </summary>
        /// <remarks>此方法会自动将警告消息进行<see cref="HtmlToJs"/>处理。
        /// <br />
        /// 此方法将在输出相关Javascript代码后自动调用Page.Response.End();终止页面输出。
        /// </remarks>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static void ReportMsg(string msg, int historyPageIndex)
        {
            HttpContext Page = HttpContext.Current;
            Page.Response.Write(ReportMsgStr(msg, historyPageIndex));
            try
            {
                Page.Response.End();
            }
            catch (ThreadAbortException)
            {
                Thread.ResetAbort();
            }
        }
        #endregion

        #region 附加或修改 QueryString 的参数
        /// <summary>
        /// 附加或修改 QueryString 的参数
        /// </summary>
        /// <remarks>
        /// 若原来的 QueryString 中不存在所指定的的参数，则添加此参数。
        /// 若已经存在，则用新值替换。
        /// </remarks>
        /// <param name="urlParameterName">参数名</param>
        /// <param name="urlParameterValue">参数值</param>
        /// <returns></returns>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static string BuildUrlString(string urlParameterName, string urlParameterValue)
        {
            string queryString = HttpContext.Current.Request.ServerVariables["Query_String"];
            return BuildUrlString(queryString, urlParameterName, urlParameterValue);
        }

        /// <summary>
        /// 附加或修改 QueryString 的参数
        /// </summary>
        /// <param name="urlParameter">含有 Url 参数键/值对的哈希表</param>
        /// <returns></returns>
        [AvailableType(AvailableTypeAttribute.Type.WebForm)]
        public static string BuildUrlString(Hashtable urlParameter)
        {
            string queryString = HttpContext.Current.Request.ServerVariables["Query_String"];
            return BuildUrlString(queryString, urlParameter);
        }

        /// <summary>
        /// 附加或修改 QueryString 的参数
        /// </summary>
        /// <param name="queryString">要修改的 QueryString</param>
        /// <param name="urlParameter">含有 Url 参数键/值对的哈希表</param>
        /// <returns></returns>
        public static string BuildUrlString(string queryString, Hashtable urlParameter)
        {
            IDictionaryEnumerator ie = urlParameter.GetEnumerator();
            while (ie.MoveNext())
            {
                queryString = BuildUrlString(queryString, ie.Key.ToString(), ie.Value.ToString());
            }
            return queryString;
        }

        /// <summary>
        /// 附加或修改 QueryString 的参数
        /// </summary>
        /// <param name="queryString">要修改的 QueryString</param>
        /// <param name="urlParameterName">参数名</param>
        /// <param name="urlParameterValue">参数值</param>
        /// <returns></returns>
        public static string BuildUrlString(string queryString, string urlParameterName, string urlParameterValue)
        {
            StringBuilder ubuilder = new StringBuilder();
            bool keyFound = false;
            int num = (queryString != null) ? queryString.Length : 0;
            for (int i = 0;i < num;i++)
            {
                int startIndex = i;
                int num4 = -1;
                while (i < num)
                {
                    char ch = queryString[i];
                    if (ch == '=')
                    {
                        if (num4 < 0)
                        {
                            num4 = i;
                        }
                    }
                    else if (ch == '&')
                    {
                        break;
                    }
                    i++;
                }
                string skey = null;
                string svalue;
                if (num4 >= 0)
                {
                    skey = queryString.Substring(startIndex, num4 - startIndex);
                    svalue = queryString.Substring(num4 + 1, (i - num4) - 1);
                }
                else
                {
                    svalue = queryString.Substring(startIndex, i - startIndex);
                }
                ubuilder.Append(skey).Append("=");
                if (skey == urlParameterName)
                {
                    keyFound = true;
                    ubuilder.Append(urlParameterValue);
                }
                else
                    ubuilder.Append(svalue);
                ubuilder.Append("&");
            }
            if (!keyFound)
                ubuilder.Append(urlParameterName).Append("=").Append(urlParameterValue);

            if (!ubuilder.ToString().StartsWith("?"))
                ubuilder.Insert(0, "?");

            return ubuilder.ToString().Trim('&');
        }
        #endregion
    }
}
