﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace uoLib.Web.Controls
{
    /// <summary>
    /// 用于加载文本的服务器控件
    /// </summary>
    [DefaultProperty("Src")]
    [ToolboxData("<{0}:Loader runat=server></{0}:Loader>")]
    [Designer(typeof(LoaderDesigner))]
    [ParseChildren(false)]
    public class Loader : CompositeControl, INamingContainer
    {
        /// <summary>
        /// 加载方式。注：当<see cref="Src"/>指定为网络路径时，此设置将固定为静态加载。
        /// </summary>
        [Bindable(true)]
        [Category("Loader")]
        [DefaultValue(LoadMethodType.StaticText)]
        [Localizable(true)]
        [DisplayName("加载方式")]
        [Description("设置为静态加载会直接将源按照文本读取；否则会先执行源，之后再读取源执行后产生的文本。")]
        public LoadMethodType LoadMethod
        {
            get
            {
                LoadMethodType m = (LoadMethodType)ViewState["Method"];
                return m;
            }
            set
            {
                ViewState["Method"] = value;
            }
        }

        /// <summary>
        /// 源路径。可以是本地路径，也可以是http(s)://开头的网络路径。
        /// </summary>
        [Bindable(true)]
        [Category("Loader")]
        [DefaultValue("")]
        [Localizable(true)]
        [DisplayName("源路径")]
        [Description("要加载或执行的源")]
        [UrlProperty()]
        public string Src
        {
            get
            {
                String s = (String)ViewState["Src"];
                return ((s == null) ? String.Empty : s);
            }
            set
            {
                ViewState["Src"] = value;
            }
        }

        /// <summary>
        /// 要加载的源的编码方式
        /// </summary>
        [Bindable(true)]
        [Category("Loader")]
        [DefaultValue(EncodingType.Default)]
        [Localizable(true)]
        [DisplayName("编码")]
        [Description("加载源时使用的编码类型")]
        public EncodingType Encoding
        {
            get
            {
                EncodingType m = (EncodingType)ViewState["Encoding"];
                return m;
            }
            set
            {
                ViewState["Encoding"] = value;
            }
        }

        /// <summary>
        /// 包装标签
        /// </summary>
        [Bindable(true)]
        [Category("Loader")]
        [DefaultValue(WarpTagType.None)]
        [Localizable(true)]
        [DisplayName("包装标签")]
        [Description("设置包装标签可以为输出文本包装额外内容")]
        public WarpTagType WarpTag
        {
            get
            {
                WarpTagType tmp = (WarpTagType)ViewState["WarpTag"];
                return tmp;
            }
            set
            {
                ViewState["WarpTag"] = value;
            }
        }

        /// <summary>
        /// 对输出文本进行 Html 编码
        /// </summary>
        [Bindable(true)]
        [Category("Loader")]
        [DefaultValue(false)]
        [Localizable(true)]
        [DisplayName("Html编码")]
        [Description("对输出文本进行 Html 编码")]
        public bool HtmlEncode
        {
            get
            {
                bool tmp = (bool)ViewState["HtmlEncode"];
                return tmp;
            }
            set
            {
                ViewState["HtmlEncode"] = value;
            }
        }

        ///// <summary>
        ///// 源路径类型
        ///// </summary>
        //[Bindable(true)]
        //[Category("Loader")]
        //[DefaultValue(SrcPathType.LocalFile)]
        //[Localizable(true)]
        //[DisplayName("源路径类型")]
        //[Description("")]
        //public SrcPathType SrcType
        //{
        //    get
        //    {
        //        SrcPathType tmp = (SrcPathType)ViewState["SrcType"];
        //        return tmp;
        //    }
        //    set
        //    {
        //        ViewState["SrcType"] = value;
        //    }
        //}
        private LoadAsType loadAs;

        public Loader()
        {
            this.WarpTag = WarpTagType.None;
            this.Encoding = EncodingType.Default;
            this.LoadMethod = Loader.LoadMethodType.StaticText;
            this.HtmlEncode = false;
            this.loadAs = LoadAsType.LocalFile;
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            if (this.Visible == false) return;
            StringBuilder sb = new StringBuilder();

            Encoding ec;
            switch (this.Encoding)
            {
                case EncodingType.ASCII:
                    ec = System.Text.Encoding.ASCII;
                    break;
                case EncodingType.UTF7:
                    ec = System.Text.Encoding.UTF7;
                    break;
                case EncodingType.UTF8:
                    ec = System.Text.Encoding.UTF8;
                    break;
                case EncodingType.UTF32:
                    ec = System.Text.Encoding.UTF32;
                    break;
                case EncodingType.Unicode:
                    ec = System.Text.Encoding.Unicode;
                    break;
                case EncodingType.Default:
                default:
                    ec = System.Text.Encoding.Default;
                    break;
            }

            string fullPath = string.Empty;
            try
            {
                fullPath = this.Page.Server.MapPath(this.Src);
                if (this.LoadMethod == LoadMethodType.StaticText)
                    loadAs = LoadAsType.LocalFile;
                else
                    loadAs = LoadAsType.Url;
            }
            catch
            {
                loadAs = LoadAsType.Url;
            }

            if (loadAs == LoadAsType.LocalFile)
            {
                using (StreamReader fs = new StreamReader(fullPath, ec))
                {
                    sb.Append(fs.ReadToEnd());
                }
            }
            else
            {
                Uri uri = new Uri(this.Src, UriKind.RelativeOrAbsolute);
                if (!uri.IsAbsoluteUri)
                {
                    Uri baseUri = this.Page.Request.Url;
                    uri = new Uri(baseUri, uri);
                }

                // 动态加载时，不允许递归调用！
                if (Uri.Compare(uri, this.Page.Request.Url,
                    UriComponents.SchemeAndServer | UriComponents.Path,
                    UriFormat.UriEscaped,
                    StringComparison.OrdinalIgnoreCase) == 0)
                {
                    throw new NotSupportedException(this.GetType() + "的属性 Src 设置错误：动态加载时，源路径不允许指定为控件所在页面（" + this.Src + "）");
                }

                string result = Common.Functions.GetWebRequest(this.Page.ResolveUrl(this.Src), "", "GET", ec);
                sb.Append(result);
            }

            switch (this.WarpTag)
            {
                case WarpTagType.Javascript:
                    sb.Insert(0, "<script language=\"javascript\" type=\"text/javascript\">\n<!--\n");
                    sb.AppendLine("\n//-->\n</script>");
                    break;
                case WarpTagType.ECMAScript:
                    sb.Insert(0, "<script language=\"ecmascript\" type=\"text/ecmascript\">\n<!--\n");
                    sb.AppendLine("\n//-->\n</script>");
                    break;
                case WarpTagType.JScript:
                    sb.Insert(0, "<script language=\"jscript\" type=\"text/jscript\">\n<!--\n");
                    sb.AppendLine("\n//-->\n</script>");
                    break;
                case WarpTagType.VBScript:
                    sb.Insert(0, "<script language=\"vbscript\" type=\"text/vbscript\">\n<!--\n");
                    sb.AppendLine("\n//-->\n</script>");
                    break;
                case WarpTagType.Css:
                    sb.Insert(0, "<style type=\"text/css\">\n");
                    sb.AppendLine("\n</style>");
                    break;
                case WarpTagType.None:
                default:
                    break;
            }

            if (this.HtmlEncode)
            {
                writer.Write(this.Page.Server.HtmlEncode(sb.ToString()));
            }
            else
            {
                writer.Write(sb.ToString());
            }
        }

        #region 无效属性
        public override Unit Width
        {
            get
            {
                throw new NotSupportedException();
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        public override Unit Height
        {
            get
            {
                throw new NotSupportedException();
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        #endregion

        #region 属性类型
        /// <summary>
        /// 加载方式
        /// </summary>
        public enum LoadMethodType
        {
            /// <summary>
            /// 按静态文本载入
            /// </summary>
            StaticText = 0,
            /// <summary>
            /// 按动态可执行文件，载入执行后的静态文本
            /// </summary>
            DynamicExecutive,
        }

        /// <summary>
        /// 包装标签
        /// </summary>
        public enum WarpTagType
        {
            None = 0,
            Javascript,
            JScript,
            VBScript,
            ECMAScript,
            Css,
        }

        /// <summary>
        /// 数据编码
        /// </summary>
        public enum EncodingType
        {
            Default = 0,
            ASCII,
            UTF7,
            UTF8,
            UTF32,
            Unicode
        }

        /// <summary>
        /// 加载类型
        /// </summary>
        public enum LoadAsType
        {
            LocalFile = 0, Url
        }
        #endregion
    }

    #region 设计器
    public class LoaderDesigner : ControlDesigner
    {
        private Loader loader;

        public override string GetDesignTimeHtml()
        {
            this.loader = (Loader)base.Component;
            return "<span style=\"border:1px solid #00aa00;padding:0 5px; background-color:#aaffaa;\">Loader: " + this.loader.Src + "</span>";
        }

        protected override string GetErrorDesignTimeHtml(Exception e)
        {
            string instruction = "Error creating control：" + e.Message;
            return base.CreatePlaceHolderDesignTimeHtml(instruction);
        }

        public override bool AllowResize
        {
            get { return false; }
        }

        #region 设计视图面板
        private DesignerActionListCollection _actionLists = null;
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (_actionLists == null)
                {
                    _actionLists = new DesignerActionListCollection();
                    _actionLists.AddRange(base.ActionLists);

                    // Add a custom DesignerActionList
                    _actionLists.Add(new ActionList(this));
                }
                return _actionLists;
            }
        }

        public class ActionList : DesignerActionList
        {
            private Loader loader;
            private LoaderDesigner designer;
            private DesignerActionItemCollection _items;

            public string Src
            {
                get
                {
                    return loader.Src;
                }
                set
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        System.Windows.Forms.MessageBox.Show("源路径不能为空！");
                        return;
                    }
                    SetProperty("Src", value);
                }
            }

            public Loader.LoadMethodType LoadMethod
            {
                get
                {
                    return loader.LoadMethod;
                }
                set
                {
                    SetProperty("LoadMethod", value);
                }
            }

            public Loader.WarpTagType WarpTag
            {
                get
                {
                    return loader.WarpTag;
                }
                set
                {
                    SetProperty("WarpTag", value);
                }
            }

            public Loader.EncodingType Encoding
            {
                get
                {
                    return loader.Encoding;
                }
                set
                {
                    SetProperty("Encoding", value);
                }
            }

            public bool HtmlEncode
            {
                get
                {
                    return loader.HtmlEncode;
                }
                set
                {
                    SetProperty("HtmlEncode", value);
                }
            }

            // Constructor
            public ActionList(LoaderDesigner parent)
                : base(parent.Component)
            {
                designer = parent;
                loader = (Loader)designer.Component;
            }

            // Create the ActionItem collection and add one command
            public override DesignerActionItemCollection GetSortedActionItems()
            {
                if (_items == null)
                {
                    _items = new DesignerActionItemCollection();
                    _items.Add(new DesignerActionHeaderItem("设置"));
                    _items.Add(new DesignerActionPropertyItem("Src", "源路径", "Loader"));
                    _items.Add(new DesignerActionPropertyItem("Encoding", "字符编码", "Loader"));
                    _items.Add(new DesignerActionPropertyItem("HtmlEncode", "Html编码", "Loader"));
                    _items.Add(new DesignerActionPropertyItem("LoadMethod", "加载类型", "Loader"));
                    _items.Add(new DesignerActionPropertyItem("WarpTag", "包装标签", "Loader"));
                    _items.Add(new DesignerActionMethodItem(this, "ShowAboutForm", "关于", "Loader", true));
                }
                return _items;
            }

            private void SetProperty(string propertyName, object value)
            {
                TypeDescriptor.GetProperties(this.loader)[propertyName].SetValue(this.loader, value);
            }

            private void ShowAboutForm()
            {
                FAbout f = new FAbout();
                f.ShowDialog();
            }
        }
        #endregion

    }
    #endregion

}
