/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Reflection;
using uoLib.Web.Debugger;
namespace uoLib
{
    /// <summary>
    /// 提供程序集基本信息
    /// </summary>
    public static class Info
    {
        private static string _helpLink = "http://work.udnz.com/uolib/";

        private static string _adminEmail = "";
        /// <summary>
        /// 获取或设置管理员的电子邮件地址。
        /// </summary>
        /// <remarks>此地址将显示到生成的错误日志中。</remarks>
        public static string AdminEmail { get { return _adminEmail; } set { _adminEmail = value; } }

        private static Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();

        /// <summary>
        /// 获取已加载uoLib.dll的内部版本号。
        /// </summary>
        public static Version Version { get { return asm.GetName().Version; } }
        /// <summary>
        /// 获取已加载uoLib.dll的路径。
        /// </summary>
        public static string Path { get { return asm.Location; } }
        /// <summary>
        /// 获取已加载uoLib.dll的程序集名称。
        /// </summary>
        public static string Name { get { return asm.GetName().Name; } }
        /// <summary>
        /// 获取已加载uoLib.dll的URL位置。
        /// </summary>
        public static string CodeBase { get { return asm.GetName().CodeBase; } }

        /// <summary>
        /// 获取uoLib的官方帮助链接地址。
        /// </summary>
        public static string HelpLink { get { return _helpLink; } }
    }
}
