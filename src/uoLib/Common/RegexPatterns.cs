﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace uoLib.Common
{
    /// <summary>
    /// 一些常用的正则表达式
    /// </summary>
    [HelpAttribute("http://work.udnz.com/uolib/")]
    [Author("uonun", Email = "uonun@163.com", HomePage = "http://work.udnz.com/uolib/")]
    public static class RegexPatterns
    {
        /*
         * 文件夹和文件的名称的非法字符是一样的的，只不过对于文件名来说，如果名称中只有一个“.”，则这个“.”不能位于第一个字符的位置。
         */
        /// <summary>
        /// 文件夹命名规则（一些特殊字符不能用于文件夹名；注意：“nul”、“aux”、“con”、“com1”、“lpt1”也不能用于文件夹名，但此处未验证）
        /// </summary>
        public const string FolderName = "^[^\\\\/\\?\\*\"><:|]{1,255}$";
        /// <summary>
        /// 文件命名规则（一些特殊字符不能用于文件名；注意：“nul”、“aux”、“con”、“com1”、“lpt1”也不能用于文件名，但此处未验证）
        /// </summary>
        public const string FileName = "^[^\\.\\\\/\\?\\*\"><:|]{1}[^\\\\/\\?\\*\"><:|]{0,254}$";

        /// <summary>
        /// 手机号码
        /// </summary>
        public const string MobileNum = @"(\+?86)?1[3|5|8][\d]{9}\b";
        /// <summary>
        /// 电子邮件
        /// </summary>
        public const string Email = @"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$";
        /// <summary>
        /// URL地址。仅包括 https|http 协议
        /// </summary>
        /// <remarks>
        /// 参见：<see cref="uoLib.Common.RegexPatterns.URI"/>
        /// </remarks>
        public const string URL = @"^(https|http)?://((([0-9a-z_!~*'().&=+$%-]+:)?[0-9a-z_!~*'().&=+$%-]+)@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|[^`~!@#$%\^&*\\(\)=\+_\[\]{}\|;:\.'"",<>/\?]+|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-z_!~*'().;?:@&=+$,%#-/]+)+)$";
        /// <summary>
        /// URI地址。包括 https|http|ftp|rtsp|mms 协议
        /// </summary>
        /// <remarks>
        /// <para>该正则支持域名/多级域名/IP/计算机名/端口/FTP登录用户名和密码等等，下面是可以匹配的几个举例：
        /// <ul>
        /// <li>http://www.udnz.com/</li>
        /// <li>http://192.168.1.1:81/</li>
        /// <li>https://PC-NAME:81/</li>
        /// <li>ftp://user:pass@udnz.com:21/folder/</li>
        /// <li>http://any.to.first.udnz.com/folder/a.aspx?q=1#n</li>
        /// </ul>
        /// </para>
        /// </remarks>
        public const string URI = @"^(https|http|ftp|rtsp|mms)?://((([0-9a-z_!~*'().&=+$%-]+:)?[0-9a-z_!~*'().&=+$%-]+)@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|[^`~!@#$%\^&*\\(\)=\+_\[\]{}\|;:\.'"",<>/\?]+|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-z_!~*'().;?:@&=+$,%#-/]+)+)$";
        /// <summary>
        /// IPv4地址
        /// </summary>
        public const string IPv4 = @"^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$";
        /// <summary>
        /// Html代码中使用的颜色字符串。如#FF0000,0000FF
        /// </summary>
        public const string HtmlColor = @"^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$";
        /// <summary>
        /// Html标签
        /// </summary>
        public const string HtmlTag = @"<[^>]+>";
    }
}
