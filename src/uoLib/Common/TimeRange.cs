﻿/************************************************************************ 
 * uoLib library for .Net projects.
 * Copyright (c) 2008-2010 by uonun
 * Homepage: http://work.udnz.com/uolib
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用作者的另一作品：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace uoLib.Common
{
    /// <summary>
    /// 具有起止信息的时间范围
    /// </summary>
    /// <remarks>
    /// 区别于<see cref="TimeSpan"/>，除表示时间间隔外，它还能表示时间起止点。
    /// </remarks>
    public class TimeRange
    {
        private DateTime _timeBegin;
        private DateTime _timeEnd;
        /// <summary>
        /// 允许的最小时间值（与SQL Server 中 datetime 数据类型一致，即1753-1-1）
        /// </summary>
        public static readonly DateTime MinDate = new DateTime(1753, 1, 1);
        /// <summary>
        /// 允许的最大时间值（与SQL Server 中 datetime 数据类型一致，即9999-12-31）
        /// </summary>
        public static readonly DateTime MaxDate = new DateTime(9999, 12, 31);

        /// <summary>
        /// 具有起止信息的时间范围
        /// </summary>
        /// <param name="time1">时间范围节点1</param>
        /// <param name="time2">时间范围节点2</param>
        /// <remarks>
        /// <paramref name="time1"/> 与 <paramref name="time2"/> 的时间前后关系无关紧要，此类会自动格式化为起止时间。
        /// </remarks>
        public TimeRange(DateTime time1, DateTime time2)
        {
            this._timeBegin = time1;
            this._timeEnd = time2;

            if (this._timeBegin == DateTime.MinValue)
                this._timeBegin = MinDate;
            else if (this._timeBegin == DateTime.MaxValue)
                this._timeBegin = MaxDate;

            if (this._timeEnd == DateTime.MinValue)
                this._timeEnd = MinDate;
            else if (this._timeEnd == DateTime.MaxValue)
                this._timeEnd = MaxDate;

            if (this._timeBegin > this._timeEnd)
            {
                DateTime tmp = this._timeEnd;
                this._timeEnd = this._timeBegin;
                this._timeBegin = tmp;
            }
        }

        /// <summary>
        /// 时间范围起点
        /// </summary>
        public DateTime Begin { get { return this._timeBegin; } set { this._timeBegin = value; } }
        /// <summary>
        /// 时间范围终点
        /// </summary>
        public DateTime End { get { return this._timeEnd; } set { this._timeEnd = value; } }
        /// <summary>
        /// 当前时间范围表示的间隔
        /// </summary>
        public TimeSpan TimeSpan { get { return this.End - this.Begin; } }

        /// <summary>
        /// 确定指定时刻是否在 TimeRange 类约定的最大最小时间范围内
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static bool Contain(DateTime datetime)
        {
            if (datetime < TimeRange.MinDate) return false;
            if (datetime > TimeRange.MaxDate) return false;
            return true;
        }

        /// <summary>
        /// 确定指定时刻是否在当前时间范围内
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public bool Contains(DateTime datetime)
        {
            if (datetime < this.Begin) return false;
            if (datetime > this.End) return false;
            return true;
        }

        /// <summary>
        /// 输出 从...到... 格式的时间范围
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("从{0}到{1}", this.Begin, this.End);
        }
    }

}
