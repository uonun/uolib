﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

namespace Documents
{
    public partial class Get : System.Web.UI.Page
    {
        private string downloadPath = System.Configuration.ConfigurationManager.AppSettings["downloadPath"];

        protected void Page_Load(object sender, EventArgs e)
        {
            string act = Request.QueryString["act"];
            string version = Request.QueryString["version"];
            if (string.IsNullOrEmpty(version) && act != "source")
            {
                msg.Text = "非法进入！";
                Response.Redirect("Download.aspx");
            }
            else
            {
                if (act == "source")
                {
                    #region 下载源码
                    string keyId = Request.QueryString["keyId"];
                    bool canDown = false;
                    if (!string.IsNullOrEmpty(keyId))
                    {
                        DataTable dt = Functions.GetItems("sourceDownload");
                        foreach (DataRow r in dt.Rows)
                        {
                            if (r["keyId"].ToString() == keyId)
                            {
                                DateTime d = DateTime.MinValue;
                                DateTime.TryParse(r["time"].ToString(), out d);

                                if ((DateTime.Now - d).TotalDays <= 7) {
                                    GetFile(Functions.GetSourceFileName());
                                    canDown = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!canDown) {
                        uoLib.Common.Functions.ReportMsg("下载链接已失效，请重新获取。", "GetDownloadLink.aspx");
                    }
                    #endregion
                }
                else
                {
                    #region 下载 dll
                    DataTable dt = Functions.GetItems("update");
                    foreach (DataRow r in dt.Rows)
                    {
                        if (r["version"].ToString() == version)
                        {
                            bool isOk = GetFile(r["file"].ToString());
                            if (isOk)
                            {
                                r["hits"] = int.Parse(r["hits"].ToString()) + 1;
                            }
                            else
                            {
                                r.Delete();
                            }
                            r.AcceptChanges();
                            break;
                        }
                    }
                    Functions.WriteUpdateItems(dt);
                    #endregion
                }
                Response.End();
            }
        }

        private bool GetFile(string fileName)
        {
            FileInfo fi = new FileInfo(Server.MapPath(string.Format("{0}{1}", downloadPath, fileName)));
            if (fi.Exists)
            {
                Response.Clear();
                Response.ClearHeaders();
                Response.Buffer = false;
                Response.ContentType = "application/octet-stream";
                Response.AppendHeader("Content-Disposition", "attachment;filename="
                    + HttpUtility.UrlEncode(fi.Name, System.Text.Encoding.UTF8));
                Response.AppendHeader("Content-Length", fi.Length.ToString());
                Response.WriteFile(fi.FullName);
                Response.Flush();
                return true;
            }
            else
            {
                uoLib.Common.Functions.ReportMsg("此文件已被删除，不再提供下载。", "Download.aspx");
                return false;
            }
        }
    }
}
