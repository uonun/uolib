﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Documents
{
    public partial class GLOBAL : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = Page.Title + " - " + System.Configuration.ConfigurationManager.AppSettings["appName"];
        }
    }
}
