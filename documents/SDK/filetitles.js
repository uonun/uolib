﻿var FileTitles = [
"PoweredMember(T)&nbsp;Members",
"AuthorAttribute&nbsp;Members",
"AvailableTypeAttribute&nbsp;Members",
"FlagBehavior(T)&nbsp;Members",
"FlagBehavior(T).AfterCheckEventArgs&nbsp;Members",
"FlagBehavior(T).BeforeCheckEventArgs&nbsp;Members",
"Functions&nbsp;Members",
"RegexPatterns&nbsp;Members",
"TimeRange&nbsp;Members",
"BaseProvider&nbsp;Members",
"Database&nbsp;Members",
"DbColumn&nbsp;Members",
"ExecuteEventArgs&nbsp;Members",
"DataColumn(T)&nbsp;Members",
"ExportEngine(T)&nbsp;Members",
"Options&nbsp;Members",
"IConnectionFactory&nbsp;Members",
"OdbcConnFactory&nbsp;Members",
"OleDbConnFactory&nbsp;Members",
"OracleClientConnFactory&nbsp;Members",
"SqlClientConnFactory&nbsp;Members",
"IBaseProvider&nbsp;Members",
"IDbProviderFactory&nbsp;Members",
"OdbcSQLite&nbsp;Members",
"OleDbAccess&nbsp;Members",
"SqlClientSqlServer&nbsp;Members",
"DatabaseNotExistException&nbsp;Members",
"NotSupportedConnException&nbsp;Members",
"NotSupportedDataTypeException&nbsp;Members",
"NullConnectionStringException&nbsp;Members",
"FAbout&nbsp;Members",
"HelpAttribute&nbsp;Members",
"Info&nbsp;Members",
"Logger&nbsp;Members",
"Base64&nbsp;Members",
"DES&nbsp;Members",
"Loader&nbsp;Members",
"LoaderDesigner&nbsp;Members",
"LoaderDesigner.ActionList&nbsp;Members",
"Configuration&nbsp;Members",
"ErrorReporter&nbsp;Members",
"ServerErrorList&nbsp;Members",
"ValidateImg&nbsp;Members",
"PoweredMember(T)&nbsp;Constructor&nbsp;",
"PoweredMember(T)&nbsp;Constructor&nbsp;(UInt64)",
"PoweredMember(T)&nbsp;Constructor&nbsp;(String)",
"PoweredMember(T)&nbsp;Constructor&nbsp;(T)",
"AuthorAttribute&nbsp;Constructor&nbsp;",
"AvailableTypeAttribute&nbsp;Constructor&nbsp;",
"FlagBehavior(T).AfterCheckEventArgs&nbsp;Constructor&nbsp;",
"FlagBehavior(T).BeforeCheckEventArgs&nbsp;Constructor&nbsp;",
"FlagBehavior(T)&nbsp;Constructor&nbsp;",
"FlagBehavior(T)&nbsp;Constructor&nbsp;(UInt64)",
"FlagBehavior(T)&nbsp;Constructor&nbsp;(String)",
"FlagBehavior(T)&nbsp;Constructor&nbsp;(T)",
"TimeRange&nbsp;Constructor&nbsp;",
"TimeRange&nbsp;Constructor&nbsp;",
"BaseProvider&nbsp;Constructor&nbsp;",
"Database&nbsp;Constructor&nbsp;",
"ExecuteEventArgs&nbsp;Constructor&nbsp;",
"DataColumn(T)&nbsp;Constructor&nbsp;",
"ExportEngine(T)&nbsp;Constructor&nbsp;",
"ExportEngine(T)&nbsp;Constructor&nbsp;(List(DataColumn(T)))",
"ExportEngine(T)&nbsp;Constructor&nbsp;(List(DataColumn(T)),&nbsp;Options)",
"Options&nbsp;Constructor&nbsp;",
"OdbcConnFactory&nbsp;Constructor&nbsp;",
"OleDbConnFactory&nbsp;Constructor&nbsp;",
"OracleClientConnFactory&nbsp;Constructor&nbsp;",
"SqlClientConnFactory&nbsp;Constructor&nbsp;",
"OdbcSQLite&nbsp;Constructor&nbsp;",
"OdbcSQLite&nbsp;Constructor&nbsp;",
"OleDbAccess&nbsp;Constructor&nbsp;",
"OleDbAccess&nbsp;Constructor&nbsp;",
"SqlClientSqlServer&nbsp;Constructor&nbsp;",
"SqlClientSqlServer&nbsp;Constructor&nbsp;",
"DatabaseNotExistException&nbsp;Constructor&nbsp;",
"NotSupportedConnException&nbsp;Constructor&nbsp;(ConnectionType)",
"NotSupportedConnException&nbsp;Constructor&nbsp;(DatabaseType,&nbsp;ConnectionType)",
"NotSupportedDataTypeException&nbsp;Constructor&nbsp;",
"NullConnectionStringException&nbsp;Constructor&nbsp;",
"FAbout&nbsp;Constructor&nbsp;",
"HelpAttribute&nbsp;Constructor&nbsp;",
"Info&nbsp;Constructor&nbsp;",
"Logger&nbsp;Constructor&nbsp;",
"Base64&nbsp;Constructor&nbsp;",
"Base64&nbsp;Constructor&nbsp;",
"DES&nbsp;Constructor&nbsp;",
"LoaderDesigner.ActionList&nbsp;Constructor&nbsp;",
"LoaderDesigner&nbsp;Constructor&nbsp;",
"Loader&nbsp;Constructor&nbsp;",
"Configuration&nbsp;Constructor&nbsp;",
"ServerErrorList&nbsp;Constructor&nbsp;",
"ValidateImg&nbsp;Constructor&nbsp;",
"PoweredMember(T)&nbsp;Events",
"FlagBehavior(T)&nbsp;Events",
"FAbout&nbsp;Events",
"Loader&nbsp;Events",
"AfterCheck&nbsp;Event",
"BeforeCheck&nbsp;Event",
"PoweredMember(T)&nbsp;Fields",
"AuthorAttribute&nbsp;Fields",
"AvailableTypeAttribute&nbsp;Fields",
"FlagBehavior(T)&nbsp;Fields",
"FlagBehavior(T).AfterCheckEventArgs&nbsp;Fields",
"FlagBehavior(T).BeforeCheckEventArgs&nbsp;Fields",
"RegexPatterns&nbsp;Fields",
"TimeRange&nbsp;Fields",
"BaseProvider&nbsp;Fields",
"Database&nbsp;Fields",
"DbColumn&nbsp;Fields",
"ExecuteEventArgs&nbsp;Fields",
"DataColumn(T)&nbsp;Fields",
"ExportEngine(T)&nbsp;Fields",
"Options&nbsp;Fields",
"OdbcSQLite&nbsp;Fields",
"OleDbAccess&nbsp;Fields",
"SqlClientSqlServer&nbsp;Fields",
"DatabaseNotExistException&nbsp;Fields",
"NotSupportedConnException&nbsp;Fields",
"NotSupportedDataTypeException&nbsp;Fields",
"NullConnectionStringException&nbsp;Fields",
"FAbout&nbsp;Fields",
"HelpAttribute&nbsp;Fields",
"Info&nbsp;Fields",
"Logger&nbsp;Fields",
"Base64&nbsp;Fields",
"Loader&nbsp;Fields",
"LoaderDesigner&nbsp;Fields",
"LoaderDesigner.ActionList&nbsp;Fields",
"Configuration&nbsp;Fields",
"ServerErrorList&nbsp;Fields",
"_author&nbsp;Field",
"_email&nbsp;Field",
"_homePage&nbsp;Field",
"_type&nbsp;Field",
"AfterCheck&nbsp;Field",
"CheckedFlag&nbsp;Field",
"CheckResult&nbsp;Field",
"BeforeCheck&nbsp;Field",
"Flag&nbsp;Field",
"FlagStringErr&nbsp;Field",
"FullFlagU8&nbsp;Field",
"MinFlagErr&nbsp;Field",
"TBaseErr&nbsp;Field",
"TTypeErr&nbsp;Field",
"TValueErr&nbsp;Field",
"_flag&nbsp;Field",
"Email&nbsp;Field",
"FileName&nbsp;Field",
"FolderName&nbsp;Field",
"HtmlColor&nbsp;Field",
"HtmlTag&nbsp;Field",
"IPv4&nbsp;Field",
"MobileNum&nbsp;Field",
"URI&nbsp;Field",
"URL&nbsp;Field",
"MaxDate&nbsp;Field",
"MinDate&nbsp;Field",
"_timeBegin&nbsp;Field",
"_timeEnd&nbsp;Field",
"connFactroy&nbsp;Field",
"connStringName&nbsp;Field",
"connType&nbsp;Field",
"_connectionString&nbsp;Field",
"ColumnName&nbsp;Field",
"DbType&nbsp;Field",
"Value&nbsp;Field",
"Command&nbsp;Field",
"ElapsedMilliseconds&nbsp;Field",
"Errors&nbsp;Field",
"RecordsAffected&nbsp;Field",
"DataField&nbsp;Field",
"DataFormater&nbsp;Field",
"DataFormatString&nbsp;Field",
"HeaderText&nbsp;Field",
"HtmlEncode&nbsp;Field",
"HtmlEncodeFormatString&nbsp;Field",
"_charset&nbsp;Field",
"_columns&nbsp;Field",
"_contentEncoding&nbsp;Field",
"_fileType&nbsp;Field",
"_opt&nbsp;Field",
"_watch&nbsp;Field",
"AlternatingRowFontName&nbsp;Field",
"AlternatingRowFontSize&nbsp;Field",
"AlternatingRowStyle&nbsp;Field",
"FileName&nbsp;Field",
"HeaderFontName&nbsp;Field",
"HeaderFontSize&nbsp;Field",
"HeaderIsBold&nbsp;Field",
"HeaderIsItalic&nbsp;Field",
"HeaderStyle&nbsp;Field",
"RemarkHtml&nbsp;Field",
"RowFontName&nbsp;Field",
"RowFontSize&nbsp;Field",
"RowStyle&nbsp;Field",
"TitleBackColor&nbsp;Field",
"TitleFontName&nbsp;Field",
"TitleFontSize&nbsp;Field",
"TitleForeColor&nbsp;Field",
"TitleHeight&nbsp;Field",
"TitleHorizontalAlign&nbsp;Field",
"TitleIsBold&nbsp;Field",
"TitleIsItalic&nbsp;Field",
"TitleText&nbsp;Field",
"db&nbsp;Field",
"padlock&nbsp;Field",
"db&nbsp;Field",
"padlock&nbsp;Field",
"db&nbsp;Field",
"padlock&nbsp;Field",
"components&nbsp;Field",
"label1&nbsp;Field",
"lbCopyright&nbsp;Field",
"lbVersion&nbsp;Field",
"linkLabel1&nbsp;Field",
"_url&nbsp;Field",
"asm&nbsp;Field",
"_adminEmail&nbsp;Field",
"_helpLink&nbsp;Field",
"_lock&nbsp;Field",
"BASE64_DECODE_TABLE&nbsp;Field",
"BASE64_ENCODE_TABLE&nbsp;Field",
"designer&nbsp;Field",
"loader&nbsp;Field",
"_items&nbsp;Field",
"loader&nbsp;Field",
"_actionLists&nbsp;Field",
"loadAs&nbsp;Field",
"_isRecord&nbsp;Field",
"_path&nbsp;Field",
"_type&nbsp;Field",
"AllLogRegex&nbsp;Field",
"PoweredMember(T)&nbsp;Methods&nbsp;",
"AuthorAttribute&nbsp;Methods&nbsp;",
"AvailableTypeAttribute&nbsp;Methods&nbsp;",
"FlagBehavior(T)&nbsp;Methods&nbsp;",
"FlagBehavior(T).AfterCheckEventArgs&nbsp;Methods&nbsp;",
"FlagBehavior(T).BeforeCheckEventArgs&nbsp;Methods&nbsp;",
"Functions&nbsp;Methods&nbsp;",
"TimeRange&nbsp;Methods&nbsp;",
"BaseProvider&nbsp;Methods&nbsp;",
"Database&nbsp;Methods&nbsp;",
"DbColumn&nbsp;Methods&nbsp;",
"ExecuteEventArgs&nbsp;Methods&nbsp;",
"DataColumn(T)&nbsp;Methods&nbsp;",
"ExportEngine(T)&nbsp;Methods&nbsp;",
"Options&nbsp;Methods&nbsp;",
"IConnectionFactory&nbsp;Methods&nbsp;",
"OdbcConnFactory&nbsp;Methods&nbsp;",
"OleDbConnFactory&nbsp;Methods&nbsp;",
"OracleClientConnFactory&nbsp;Methods&nbsp;",
"SqlClientConnFactory&nbsp;Methods&nbsp;",
"IBaseProvider&nbsp;Methods&nbsp;",
"IDbProviderFactory&nbsp;Methods&nbsp;",
"OdbcSQLite&nbsp;Methods&nbsp;",
"OleDbAccess&nbsp;Methods&nbsp;",
"SqlClientSqlServer&nbsp;Methods&nbsp;",
"DatabaseNotExistException&nbsp;Methods&nbsp;",
"NotSupportedConnException&nbsp;Methods&nbsp;",
"NotSupportedDataTypeException&nbsp;Methods&nbsp;",
"NullConnectionStringException&nbsp;Methods&nbsp;",
"FAbout&nbsp;Methods&nbsp;",
"HelpAttribute&nbsp;Methods&nbsp;",
"Logger&nbsp;Methods&nbsp;",
"Base64&nbsp;Methods&nbsp;",
"DES&nbsp;Methods&nbsp;",
"Loader&nbsp;Methods&nbsp;",
"LoaderDesigner&nbsp;Methods&nbsp;",
"LoaderDesigner.ActionList&nbsp;Methods&nbsp;",
"ErrorReporter&nbsp;Methods&nbsp;",
"ServerErrorList&nbsp;Methods&nbsp;",
"ValidateImg&nbsp;Methods&nbsp;",
"ToString&nbsp;Method&nbsp;",
"Append&nbsp;Method&nbsp;",
"Check&nbsp;Method&nbsp;(T)",
"Check&nbsp;Method&nbsp;(T,&nbsp;T)",
"GetSqlConditions&nbsp;Method&nbsp;",
"IsPowerOfTwo&nbsp;Method&nbsp;",
"Remove&nbsp;Method&nbsp;",
"Toggle&nbsp;Method&nbsp;",
"ToString&nbsp;Method&nbsp;",
"TtoU8&nbsp;Method&nbsp;",
"UlongToStr&nbsp;Method&nbsp;",
"BuildUrlString&nbsp;Method&nbsp;(Hashtable)",
"BuildUrlString&nbsp;Method&nbsp;(String,&nbsp;Hashtable)",
"BuildUrlString&nbsp;Method&nbsp;(String,&nbsp;String)",
"BuildUrlString&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;String)",
"CutStr&nbsp;Method&nbsp;(String,&nbsp;Int32)",
"CutStr&nbsp;Method&nbsp;(String,&nbsp;Int32,&nbsp;Boolean)",
"FindNode&nbsp;Method&nbsp;",
"FormatFileSize&nbsp;Method&nbsp;(Int64)",
"FormatFileSize&nbsp;Method&nbsp;(Int64,&nbsp;Int32)",
"GetErrorStack&nbsp;Method&nbsp;",
"GetFileSizeFromString&nbsp;Method&nbsp;",
"GetIP&nbsp;Method&nbsp;",
"GetLength&nbsp;Method&nbsp;",
"GetMIME&nbsp;Method&nbsp;",
"GetRandomNum&nbsp;Method&nbsp;",
"GetTotalPhysicalMemory&nbsp;Method&nbsp;",
"GetValidFileName&nbsp;Method&nbsp;",
"GetValidFolderName&nbsp;Method&nbsp;",
"GetWebRequest&nbsp;Method&nbsp;",
"HtmlDeCode&nbsp;Method&nbsp;",
"HtmlEnCode&nbsp;Method&nbsp;",
"HtmlFilter&nbsp;Method&nbsp;",
"HtmlToJs&nbsp;Method&nbsp;",
"IPToNumber&nbsp;Method&nbsp;",
"IsContainBadWord&nbsp;Method&nbsp;",
"IsEmail&nbsp;Method&nbsp;",
"IsFileName&nbsp;Method&nbsp;",
"IsFolderName&nbsp;Method&nbsp;",
"IsIP&nbsp;Method&nbsp;",
"IsMatchRegex&nbsp;Method&nbsp;",
"IsMethodOk&nbsp;Method&nbsp;",
"IsMobileNum&nbsp;Method&nbsp;",
"IsNullOrEmptyStr&nbsp;Method&nbsp;",
"IsNumber&nbsp;Method&nbsp;",
"IsRemotePosting&nbsp;Method&nbsp;",
"IsRGBColor&nbsp;Method&nbsp;",
"IsUrl&nbsp;Method&nbsp;",
"IsValidName&nbsp;Method&nbsp;",
"NumberToIP&nbsp;Method&nbsp;",
"ReplaceBadChar&nbsp;Method&nbsp;",
"ReplaceWitheChar&nbsp;Method&nbsp;",
"ReportMsgStr&nbsp;Method&nbsp;(String)",
"ReportMsgStr&nbsp;Method&nbsp;(String,&nbsp;Int32)",
"ReportMsgStr&nbsp;Method&nbsp;(String,&nbsp;String)",
"ReportMsg&nbsp;Method&nbsp;(String)",
"ReportMsg&nbsp;Method&nbsp;(String,&nbsp;Boolean)",
"ReportMsg&nbsp;Method&nbsp;(String,&nbsp;Int32)",
"ReportMsg&nbsp;Method&nbsp;(String,&nbsp;String)",
"SelectNodeByValue&nbsp;Method&nbsp;",
"Contains&nbsp;Method&nbsp;",
"Contain&nbsp;Method&nbsp;",
"ToString&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommandBuilder&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;(String)",
"CreateCommand&nbsp;Method&nbsp;(String,&nbsp;DbParameter[])",
"CreateCommand&nbsp;Method&nbsp;(String,&nbsp;Object[])",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnectionStringBuilder&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;(String)",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataAdapter&nbsp;Method&nbsp;(String)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(String)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand,&nbsp;CommandBehavior)",
"CreateOutPutParameter&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;(String,&nbsp;DbType,&nbsp;Object)",
"ExecuteNonQuery&nbsp;Method&nbsp;(DbCommand)",
"ExecuteNonQuery&nbsp;Method&nbsp;(String)",
"ExecuteNonQuery&nbsp;Method&nbsp;(String,&nbsp;Object[])",
"ExecuteNonQuery&nbsp;Method&nbsp;(DbCommand,&nbsp;BaseProvider.CommandExecuedCallback)",
"ExecuteScalar&nbsp;Method&nbsp;(DbCommand)",
"ExecuteScalar&nbsp;Method&nbsp;(DbCommand,&nbsp;BaseProvider.CommandExecuedCallback)",
"ExecuteWithTransaction&nbsp;Method&nbsp;(List(String))",
"ExecuteWithTransaction&nbsp;Method&nbsp;(List(DbCommand))",
"GetChildID&nbsp;Method&nbsp;",
"GetParentID&nbsp;Method&nbsp;",
"InitConnFactory&nbsp;Method&nbsp;",
"Insert&nbsp;Method&nbsp;",
"ReturnValue(T)&nbsp;Method&nbsp;(DbCommand)",
"ReturnValue(T)&nbsp;Method&nbsp;(String)",
"ReturnValue(T)&nbsp;Method&nbsp;(DbCommand,&nbsp;Int32)",
"ReturnValue(T)&nbsp;Method&nbsp;(DbCommand,&nbsp;String)",
"ReturnValue(T)&nbsp;Method&nbsp;(String,&nbsp;Int32)",
"ReturnValue(T)&nbsp;Method&nbsp;(String,&nbsp;String)",
"SelectData&nbsp;Method&nbsp;(DbCommand)",
"SelectData&nbsp;Method&nbsp;(String)",
"SelectData&nbsp;Method&nbsp;(String,&nbsp;DbParameter[])",
"SelectData&nbsp;Method&nbsp;(String,&nbsp;Object[])",
"SelectData&nbsp;Method&nbsp;(DbCommand,&nbsp;BaseProvider.CommandExecuedCallback)",
"TestConnection&nbsp;Method&nbsp;",
"Update&nbsp;Method&nbsp;",
"CreateExampleConnectionString&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;String,&nbsp;DatabaseType,&nbsp;ConnectionType)",
"CreateExampleConnectionString&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;String,&nbsp;String,&nbsp;ConnectionType)",
"GetSupportedConnType&nbsp;Method&nbsp;",
"ExportToExcel&nbsp;Method&nbsp;(Object)",
"ExportToExcel&nbsp;Method&nbsp;(GridView)",
"gvw_RowDataBound&nbsp;Method&nbsp;",
"SetStype&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommandBuilder&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnectionStringBuilder&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand,&nbsp;CommandBehavior)",
"CreateParameter&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommandBuilder&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnectionStringBuilder&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand,&nbsp;CommandBehavior)",
"CreateParameter&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommandBuilder&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnectionStringBuilder&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand,&nbsp;CommandBehavior)",
"CreateParameter&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommandBuilder&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnectionStringBuilder&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand,&nbsp;CommandBehavior)",
"CreateParameter&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommandBuilder&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnectionStringBuilder&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand,&nbsp;CommandBehavior)",
"CreateParameter&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;(String)",
"CreateCommand&nbsp;Method&nbsp;(String,&nbsp;Object[])",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateOutPutParameter&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"ExecuteNonQuery&nbsp;Method&nbsp;",
"ExecuteScalar&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;(DbCommand)",
"SelectData&nbsp;Method&nbsp;(String)",
"SelectData&nbsp;Method&nbsp;(String,&nbsp;Object[])",
"CreateCommand&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;(String)",
"CreateConnection&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;(String)",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;(DbCommand)",
"CreateDataAdapter&nbsp;Method&nbsp;(String)",
"CreateDataReader&nbsp;Method&nbsp;(DbCommand)",
"CreateDataReader&nbsp;Method&nbsp;(String)",
"CreateExampleConnectionString&nbsp;Method&nbsp;(String)",
"CreateExampleConnectionString&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;String)",
"CreateExampleConnectionString&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;String,&nbsp;String)",
"CreateOutPutParameter&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;(String,&nbsp;DbType,&nbsp;Object)",
"SelectData&nbsp;Method&nbsp;(DbCommand)",
"SelectData&nbsp;Method&nbsp;(String)",
"CreateExampleConnectionString&nbsp;Method&nbsp;",
"GetSupportedConnType&nbsp;Method&nbsp;",
"CreateExampleConnectionString&nbsp;Method&nbsp;",
"GetSupportedConnType&nbsp;Method&nbsp;",
"BackUp&nbsp;Method&nbsp;",
"CreateDatabase&nbsp;Method&nbsp;",
"CreateExampleConnectionString&nbsp;Method&nbsp;",
"DeleteDatabase&nbsp;Method&nbsp;",
"GetSupportedConnType&nbsp;Method&nbsp;",
"GetTableDetail&nbsp;Method&nbsp;",
"IsColumnExists&nbsp;Method&nbsp;",
"IsDatabaseExist&nbsp;Method&nbsp;",
"ListColumns&nbsp;Method&nbsp;",
"ListDatabases&nbsp;Method&nbsp;",
"ListTables&nbsp;Method&nbsp;",
"ListTriggers&nbsp;Method&nbsp;",
"ListViews&nbsp;Method&nbsp;",
"Restore&nbsp;Method&nbsp;(SqlConnection,&nbsp;String)",
"Restore&nbsp;Method&nbsp;(SqlConnection,&nbsp;String,&nbsp;String)",
"Dispose&nbsp;Method&nbsp;(Boolean)",
"InitializeComponent&nbsp;Method&nbsp;",
"linkLabel1_LinkClicked&nbsp;Method&nbsp;",
"WriteEventLog&nbsp;Method&nbsp;(String,&nbsp;EventLogEntryType)",
"WriteEventLog&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;EventLogEntryType,&nbsp;String)",
"Write&nbsp;Method&nbsp;(String)",
"Write&nbsp;Method&nbsp;(Exception)",
"Write&nbsp;Method&nbsp;(Exception,&nbsp;String,&nbsp;Boolean,&nbsp;Encoding)",
"Write&nbsp;Method&nbsp;(String,&nbsp;String,&nbsp;Boolean,&nbsp;Encoding)",
"FromBase64String&nbsp;Method&nbsp;",
"ToBase64String&nbsp;Method&nbsp;",
"ToBase64&nbsp;Method&nbsp;",
"DecryptStr&nbsp;Method&nbsp;",
"EncryptStr&nbsp;Method&nbsp;",
"GetSortedActionItems&nbsp;Method&nbsp;",
"SetProperty&nbsp;Method&nbsp;",
"ShowAboutForm&nbsp;Method&nbsp;",
"GetDesignTimeHtml&nbsp;Method&nbsp;",
"GetErrorDesignTimeHtml&nbsp;Method&nbsp;",
"CreateChildControls&nbsp;Method&nbsp;",
"RenderControl&nbsp;Method&nbsp;(HtmlTextWriter)",
"BuildErrorLogContent&nbsp;Method&nbsp;",
"EnhancedStackTrace&nbsp;Method&nbsp;(Exception)",
"EnhancedStackTrace&nbsp;Method&nbsp;(StackTrace)",
"GetErrorFolder&nbsp;Method&nbsp;",
"RecordErrors&nbsp;Method&nbsp;(Exception)",
"RecordErrors&nbsp;Method&nbsp;(String,&nbsp;LogType)",
"ShowServerErrorBody&nbsp;Method&nbsp;",
"ShowServerErrorFooter&nbsp;Method&nbsp;",
"ShowServerErrorHeader&nbsp;Method&nbsp;",
"ShowServerError&nbsp;Method&nbsp;(Exception)",
"ShowServerError&nbsp;Method&nbsp;(Exception,&nbsp;Boolean)",
"StackFrameToString&nbsp;Method&nbsp;",
"CheckLogin&nbsp;Method&nbsp;",
"ProcessRequest&nbsp;Method&nbsp;",
"ShowBody&nbsp;Method&nbsp;",
"ShowFooter&nbsp;Method&nbsp;",
"ShowHeader&nbsp;Method&nbsp;",
"CreateCode&nbsp;Method&nbsp;",
"CreateImages&nbsp;Method&nbsp;",
"ProcessRequest&nbsp;Method&nbsp;",
"(&nbsp;默认命名空间&nbsp;)&nbsp;Namespace",
"uoLib&nbsp;Namespace",
"uoLib.Authentication&nbsp;Namespace",
"uoLib.Common&nbsp;Namespace",
"uoLib.Data&nbsp;Namespace",
"uoLib.Data.Export&nbsp;Namespace",
"uoLib.Data.Factories&nbsp;Namespace",
"uoLib.Data.Singleton&nbsp;Namespace",
"uoLib.Exceptions&nbsp;Namespace",
"uoLib.Security&nbsp;Namespace",
"uoLib.Web.Controls&nbsp;Namespace",
"uoLib.Web.Debugger&nbsp;Namespace",
"uoLib.Web.HttpHandlers&nbsp;Namespace",
"PoweredMember(T)&nbsp;Constructor&nbsp;",
"Check&nbsp;Method&nbsp;",
"FlagBehavior(T)&nbsp;Constructor&nbsp;",
"BuildUrlString&nbsp;Method&nbsp;",
"CutStr&nbsp;Method&nbsp;",
"FormatFileSize&nbsp;Method&nbsp;",
"ReportMsg&nbsp;Method&nbsp;",
"ReportMsgStr&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"ExecuteNonQuery&nbsp;Method&nbsp;",
"ExecuteScalar&nbsp;Method&nbsp;",
"ExecuteWithTransaction&nbsp;Method&nbsp;",
"ReturnValue&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateExampleConnectionString&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"ExecuteNonQuery&nbsp;Method&nbsp;",
"ExecuteScalar&nbsp;Method&nbsp;",
"ExecuteWithTransaction&nbsp;Method&nbsp;",
"ReturnValue&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"ExportEngine(T)&nbsp;Constructor&nbsp;",
"ExportToExcel&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateExampleConnectionString&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"ExecuteNonQuery&nbsp;Method&nbsp;",
"ExecuteScalar&nbsp;Method&nbsp;",
"ExecuteWithTransaction&nbsp;Method&nbsp;",
"ReturnValue&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"ExecuteNonQuery&nbsp;Method&nbsp;",
"ExecuteScalar&nbsp;Method&nbsp;",
"ExecuteWithTransaction&nbsp;Method&nbsp;",
"ReturnValue&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"CreateCommand&nbsp;Method&nbsp;",
"CreateConnection&nbsp;Method&nbsp;",
"CreateDataAdapter&nbsp;Method&nbsp;",
"CreateDataReader&nbsp;Method&nbsp;",
"CreateParameter&nbsp;Method&nbsp;",
"ExecuteNonQuery&nbsp;Method&nbsp;",
"ExecuteScalar&nbsp;Method&nbsp;",
"ExecuteWithTransaction&nbsp;Method&nbsp;",
"Restore&nbsp;Method&nbsp;",
"ReturnValue&nbsp;Method&nbsp;",
"SelectData&nbsp;Method&nbsp;",
"NotSupportedConnException&nbsp;Constructor&nbsp;",
"AccessibilityNotifyClients&nbsp;Method&nbsp;",
"ActivateControlInternal&nbsp;Method&nbsp;",
"AdjustSystemMenu&nbsp;Method&nbsp;",
"ApplySizeConstraints&nbsp;Method&nbsp;",
"BeginInvoke&nbsp;Method&nbsp;",
"ComputeWindowSize&nbsp;Method&nbsp;",
"CreateControl&nbsp;Method&nbsp;",
"Dispose&nbsp;Method&nbsp;",
"EndUpdateInternal&nbsp;Method&nbsp;",
"GetChildAtPoint&nbsp;Method&nbsp;",
"Invalidate&nbsp;Method&nbsp;",
"Invoke&nbsp;Method&nbsp;",
"MemberwiseClone&nbsp;Method&nbsp;",
"PaintBackground&nbsp;Method&nbsp;",
"PaintTransparentBackground&nbsp;Method&nbsp;",
"PerformAutoScale&nbsp;Method&nbsp;",
"PerformLayout&nbsp;Method&nbsp;",
"ResumeLayout&nbsp;Method&nbsp;",
"RtlTranslateAlignment&nbsp;Method&nbsp;",
"Scale&nbsp;Method&nbsp;",
"ScaleControl&nbsp;Method&nbsp;",
"Select&nbsp;Method&nbsp;",
"SendMessage&nbsp;Method&nbsp;",
"SetBounds&nbsp;Method&nbsp;",
"Show&nbsp;Method&nbsp;",
"ShowDialog&nbsp;Method&nbsp;",
"SizeFromClientSize&nbsp;Method&nbsp;",
"UpdateBounds&nbsp;Method&nbsp;",
"UpdateMenuHandles&nbsp;Method&nbsp;",
"Validate&nbsp;Method&nbsp;",
"ValidateChildren&nbsp;Method&nbsp;",
"WmContextMenu&nbsp;Method&nbsp;",
"Write&nbsp;Method&nbsp;",
"WriteEventLog&nbsp;Method&nbsp;",
"CreateErrorDesignTimeHtml&nbsp;Method&nbsp;",
"CreatePlaceHolderDesignTimeHtml&nbsp;Method&nbsp;",
"Dispose&nbsp;Method&nbsp;",
"GetDesignTimeHtml&nbsp;Method&nbsp;",
"Invalidate&nbsp;Method&nbsp;",
"UseRegions&nbsp;Method&nbsp;",
"DataBind&nbsp;Method&nbsp;",
"FindControl&nbsp;Method&nbsp;",
"RenderControl&nbsp;Method&nbsp;",
"ValidateEvent&nbsp;Method&nbsp;",
"EnhancedStackTrace&nbsp;Method&nbsp;",
"RecordErrors&nbsp;Method&nbsp;",
"ShowServerError&nbsp;Method&nbsp;",
"PoweredMember(T)&nbsp;Properties&nbsp;",
"AuthorAttribute&nbsp;Properties&nbsp;",
"AvailableTypeAttribute&nbsp;Properties&nbsp;",
"FlagBehavior(T)&nbsp;Properties&nbsp;",
"TimeRange&nbsp;Properties&nbsp;",
"BaseProvider&nbsp;Properties&nbsp;",
"Database&nbsp;Properties&nbsp;",
"ExportEngine(T)&nbsp;Properties&nbsp;",
"Options&nbsp;Properties&nbsp;",
"IBaseProvider&nbsp;Properties&nbsp;",
"IDbProviderFactory&nbsp;Properties&nbsp;",
"OdbcSQLite&nbsp;Properties&nbsp;",
"OleDbAccess&nbsp;Properties&nbsp;",
"SqlClientSqlServer&nbsp;Properties&nbsp;",
"DatabaseNotExistException&nbsp;Properties&nbsp;",
"NotSupportedConnException&nbsp;Properties&nbsp;",
"NotSupportedDataTypeException&nbsp;Properties&nbsp;",
"NullConnectionStringException&nbsp;Properties&nbsp;",
"FAbout&nbsp;Properties&nbsp;",
"HelpAttribute&nbsp;Properties&nbsp;",
"Info&nbsp;Properties&nbsp;",
"Loader&nbsp;Properties&nbsp;",
"LoaderDesigner&nbsp;Properties&nbsp;",
"LoaderDesigner.ActionList&nbsp;Properties&nbsp;",
"Configuration&nbsp;Properties&nbsp;",
"ServerErrorList&nbsp;Properties&nbsp;",
"ValidateImg&nbsp;Properties&nbsp;",
"Power&nbsp;Property&nbsp;",
"PowerString&nbsp;Property&nbsp;",
"PowerUInt64&nbsp;Property&nbsp;",
"Author&nbsp;Property&nbsp;",
"Email&nbsp;Property&nbsp;",
"HomePage&nbsp;Property&nbsp;",
"Flag&nbsp;Property&nbsp;",
"FlagString&nbsp;Property&nbsp;",
"FlagU8&nbsp;Property&nbsp;",
"FullFlag&nbsp;Property&nbsp;",
"MaxFlag&nbsp;Property&nbsp;",
"MaxFlagU8&nbsp;Property&nbsp;",
"Begin&nbsp;Property&nbsp;",
"End&nbsp;Property&nbsp;",
"TimeSpan&nbsp;Property&nbsp;",
"ConnectionString&nbsp;Property&nbsp;",
"ConnectionType&nbsp;Property&nbsp;",
"Charset&nbsp;Property&nbsp;",
"Columns&nbsp;Property&nbsp;",
"ContentEncoding&nbsp;Property&nbsp;",
"FileType&nbsp;Property&nbsp;",
"Options&nbsp;Property&nbsp;",
"InfoHtml&nbsp;Property&nbsp;",
"ConnectionType&nbsp;Property&nbsp;",
"ConnectionType&nbsp;Property&nbsp;",
"DatabaseType&nbsp;Property&nbsp;",
"Instance&nbsp;Property&nbsp;",
"Instance&nbsp;Property&nbsp;",
"Instance&nbsp;Property&nbsp;",
"Url&nbsp;Property&nbsp;",
"AdminEmail&nbsp;Property&nbsp;",
"CodeBase&nbsp;Property&nbsp;",
"HelpLink&nbsp;Property&nbsp;",
"Name&nbsp;Property&nbsp;",
"Path&nbsp;Property&nbsp;",
"Version&nbsp;Property&nbsp;",
"ActionLists&nbsp;Property&nbsp;",
"Encoding&nbsp;Property&nbsp;",
"HtmlEncode&nbsp;Property&nbsp;",
"LoadMethod&nbsp;Property&nbsp;",
"Src&nbsp;Property&nbsp;",
"WarpTag&nbsp;Property&nbsp;",
"AllowResize&nbsp;Property&nbsp;",
"Encoding&nbsp;Property&nbsp;",
"Height&nbsp;Property&nbsp;",
"HtmlEncode&nbsp;Property&nbsp;",
"LoadMethod&nbsp;Property&nbsp;",
"Src&nbsp;Property&nbsp;",
"WarpTag&nbsp;Property&nbsp;",
"Width&nbsp;Property&nbsp;",
"IsRecordErrors&nbsp;Property&nbsp;",
"LockPassword&nbsp;Property&nbsp;",
"LogPath&nbsp;Property&nbsp;",
"LogType&nbsp;Property&nbsp;",
"IsReusable&nbsp;Property&nbsp;",
"IsReusable&nbsp;Property&nbsp;",
"PoweredMember(T)&nbsp;Class",
"AuthorAttribute&nbsp;Class",
"AvailableTypeAttribute&nbsp;Class",
"AvailableTypeAttribute.Type&nbsp;Enumeration",
"FlagBehavior(T)&nbsp;Class",
"FlagBehavior(T).AfterCheckEventArgs&nbsp;Class",
"FlagBehavior(T).AfterCheckEventHandler&nbsp;Delegate",
"FlagBehavior(T).BeforeCheckEventArgs&nbsp;Class",
"FlagBehavior(T).BeforeCheckEventHandler&nbsp;Delegate",
"Functions&nbsp;Class",
"RegexPatterns&nbsp;Class",
"TimeRange&nbsp;Class",
"BaseProvider&nbsp;Class",
"BaseProvider.CommandExecuedCallback&nbsp;Delegate",
"ConnectionType&nbsp;Enumeration",
"Database&nbsp;Class",
"DatabaseType&nbsp;Enumeration",
"DbColumn&nbsp;Structure",
"ExecuteEventArgs&nbsp;Class",
"DataColumn(T)&nbsp;Class",
"DataFormatter(T)&nbsp;Delegate",
"ExportEngine(T)&nbsp;Class",
"FileType&nbsp;Enumeration",
"Options&nbsp;Class",
"IConnectionFactory&nbsp;Interface",
"OdbcConnFactory&nbsp;Class",
"OleDbConnFactory&nbsp;Class",
"OracleClientConnFactory&nbsp;Class",
"SqlClientConnFactory&nbsp;Class",
"IBaseProvider&nbsp;Interface",
"IDbProviderFactory&nbsp;Interface",
"OdbcSQLite&nbsp;Class",
"OleDbAccess&nbsp;Class",
"SqlClientSqlServer&nbsp;Class",
"DatabaseNotExistException&nbsp;Class",
"NotSupportedConnException&nbsp;Class",
"NotSupportedDataTypeException&nbsp;Class",
"NullConnectionStringException&nbsp;Class",
"FAbout&nbsp;Class",
"Functions&nbsp;Class",
"HelpAttribute&nbsp;Class",
"Info&nbsp;Class",
"Logger&nbsp;Class",
"RegexPatterns&nbsp;Class",
"Base64&nbsp;Class",
"DES&nbsp;Class",
"Loader&nbsp;Class",
"LoaderDesigner&nbsp;Class",
"LoaderDesigner.ActionList&nbsp;Class",
"Loader.EncodingType&nbsp;Enumeration",
"Loader.LoadAsType&nbsp;Enumeration",
"Loader.LoadMethodType&nbsp;Enumeration",
"Loader.WarpTagType&nbsp;Enumeration",
"Configuration&nbsp;Class",
"ErrorReporter&nbsp;Class",
"LogType&nbsp;Enumeration",
"ServerErrorList&nbsp;Class",
"ValidateImg&nbsp;Class",
"&lt;Module&gt;&nbsp;Class"];
