﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Documents
{
    public partial class GetDownloadLink : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sourceFileName.Text = Functions.GetSourceFileName();
                sourceFileHits.Text = Functions.GetSourceUserCount().ToString();
            }
            else
            {
                if (!uoLib.Common.Functions.IsEmail(mailbox.Text))
                {
                    uoLib.Common.Functions.ReportMsg("电子邮件格式不正确！", -1);
                }
                else
                {
                    string fileName = Functions.GetSourceFileName();
                    string keyId = Guid.NewGuid().ToString();
                    string link = "http://uolib.udnz.com/Get.aspx?act=source&keyId=" + keyId;
                    string subject = "下载链接：" + fileName;
                    string content =
                        "<p>感谢下载我们的作品，这里是您的下载链接：（注：1周内有效）<br />"
                        + "<a href=\"" + link + "\" target=\"_blank\">" + link + "</a>"                   
                        + "</p><p>要查看更多信息请访问："
                        + "<a href=\"http://www.udnz.com\" target=\"_blank\">http://www.udnz.com</a></p>";

                    bool isSent = Functions.SendEmail(mailbox.Text, subject, content);
                    if (isSent)
                    {
                        Functions.AddSourceUsers(mailbox.Text, keyId);
                        uoLib.Common.Functions.ReportMsg("下载链接已经发送到您的电子邮箱，请查收。", "/");
                    }
                    else
                    {
                        uoLib.Common.Functions.ReportMsg("邮件发送失败，请手动发送邮件到uonun@163.com索取源代码。", -1);
                    }
                }
            }
        }
    }
}
