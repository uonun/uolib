﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

namespace Documents
{
    public partial class Download : System.Web.UI.Page
    {
        private string downloadPath = System.Configuration.ConfigurationManager.AppSettings["downloadPath"];

        protected void Page_Load(object sender, EventArgs e)
        {
            list.ItemDataBound += new RepeaterItemEventHandler(list_ItemDataBound);
            list.DataSource = Functions.GetItems("update");
            list.DataBind();

            sourceFileName.Text = Functions.GetSourceFileName();
            sourceFileHits.Text = Functions.GetSourceUserCount().ToString();
        }

        void list_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem
                || e.Item.ItemType == ListItemType.EditItem
                || e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.SelectedItem)
            {
                DataRowView dr = (DataRowView)e.Item.DataItem;

                HyperLink title = e.Item.FindControl("titleLB") as HyperLink;
                title.Text = dr["title"].ToString();
                title.NavigateUrl = string.Format("Get.aspx?version={0}", dr["version"]);

                Label fileSize = e.Item.FindControl("fileSize") as Label;
                FileInfo fi = new FileInfo(Server.MapPath(string.Format("{0}{1}", downloadPath, dr["file"])));
                if (fi.Exists)
                {
                    fileSize.Text = uoLib.Common.Functions.FormatFileSize(fi.Length);
                }
                else
                {
                    fileSize.Text = "0";
                }

                Label hits = e.Item.FindControl("hits") as Label;
                hits.Text = (int.Parse(dr["hits"].ToString()) + DateTime.Now.Year).ToString();

                Label infoLB = e.Item.FindControl("infoLB") as Label;
                infoLB.Text = dr["info"].ToString();
            }
        }
    }
}
