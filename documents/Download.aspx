﻿<%@ Page Title="下载" Language="C#" MasterPageFile="~/GLOBAL.Master" AutoEventWireup="true"
    CodeBehind="Download.aspx.cs" Inherits="Documents.Download" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MAIN" runat="server">
    <style type="text/css">
        #downloadList { width: 100%; list-style: none; padding: 5px 0 0 0; margin-left: 0; }
        #downloadList .title { background-color: #EFEFEF; padding: 15px; }
        #downloadList .title, #downloadList .title a { font-weight: lighter; font-family: Century Gothic; font-size: 18px; }
        #downloadList .info { padding: 6px 20px;}
        #downloadList .info ul { margin-left:80px; }
        #downloadList .info ul li {text-indent:-5em; list-style:none; }
        #downloadList .hits { font-size: 14px; font-style: italic; }
    </style>
    <h3>Release Download:<span style="font-size:12px; font-weight:normal; padding-left:20px;">压缩包包含uoLib.dll和CHM格式的帮助文档。无法打开CHM文档？ 选定文件，点右键，在“属性—常规”里点击“解除锁定”就行了！</span></h3>
    <asp:Repeater runat="server" ID="list">
        <HeaderTemplate>
            <ul id="downloadList">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <div class="title">
                    <asp:HyperLink runat="server" ID="titleLB" ToolTip="点击下载"></asp:HyperLink>
                    <span class="hits">(<asp:Label runat="server" ID="fileSize"></asp:Label>, hits:
                        <asp:Label runat="server" ID="hits"></asp:Label>)</span>
                </div>
                <div class="info">
                    <asp:Label runat="server" ID="infoLB"></asp:Label>
                </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
    <div class="sourceDownload">
        Source Code: <a href="GetDownloadLink.aspx">
            <asp:Label runat="server" ID="sourceFileName"></asp:Label></a> <span class="note">(Hits:
                <asp:Label runat="server" ID="sourceFileHits"></asp:Label>) 仅供专业人士下载，不得用于商业用途。</span></div>
</asp:Content>
