﻿<%@ Page Title="源码下载" Language="C#" MasterPageFile="~/GLOBAL.Master" AutoEventWireup="true"
    CodeBehind="GetDownloadLink.aspx.cs" Inherits="Documents.GetDownloadLink" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MAIN" runat="server">
<style>
.dform { margin:25px 50px;}
.dform td { font-size:14px;}
</style>
    <div class="sourceDownload">
        Source Code: <a href="GetDownloadLink.aspx"><asp:Label runat="server" ID="sourceFileName"></asp:Label></a> <span
            class="note">(Hits: <asp:Label runat="server" ID="sourceFileHits"></asp:Label>) 仅供专业人士下载，不得用于商业用途。</span></div>
<table cellspacing="0" cellpadding="0" border="0" class="dform">
<tr><th colspan="2" align="left" height="25">请填写您的电子邮件地址，以便我们将下载链接发送给您。</th></tr>
	<tr>
		<td>电子邮件地址：</td>
		<td><asp:TextBox runat="server" ID="mailbox" CssClass="sTxt" />
		<asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="mailbox" Display="Dynamic"
		 Text="* 必填"></asp:RequiredFieldValidator>
		 <asp:RegularExpressionValidator runat="server" ID="rqvRegx" ControlToValidate="mailbox"
        Display="Dynamic" Text="* 电子邮件格式不正确" ValidationExpression="^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$"></asp:RegularExpressionValidator>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="font-size:12px; color:#CCC;">
		此措施仅用于防止盗链行为，我们不会将您的电子邮件使用在别的任何地方。</td>
	</tr>
	<tr><td>&nbsp;</td><td>
	<asp:Button runat="server" ID="buSubmit" Text="提交" CssClass="sBu" /></td></tr>
</table>
</asp:Content>
