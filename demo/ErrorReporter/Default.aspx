﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ErrorReporter._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>uoLib 异常日志捕获、列表功能Demo</title>
    <base target="_blank" />
    <style type="text/css">
        body { width: 776px; margin: 20px auto; }
        body, p { font-size: 14px; }
        a:link { color: #0000FF; }
        .note { font-size: 12px; color: #aaa; }
        .s5 { border-bottom-color: gray; border-left: gray 1px solid; border-top-color: gray; width: 16px; border-right-color: gray; }
        div#mainSection table { border: 0; font-size: 100%; width: 98.9%; margin-top: 5px; margin-bottom: 5px; }
        div#mainSection table tr { vertical-align: top; }
        div#mainSection table th { background-color: #EFEFF7; border-bottom: 1px solid #C8CDDE; border-left: 1px none #D5D5D3; color: #000066; padding-left: 5px; padding-right: 5px; text-align: left; }
        span.copyCode { color: #0000ff; font-size: 90%; font-weight: normal; cursor: pointer; float: right; display: inline; text-align: right; text-decoration: underline; }
        div#mainSection table td { background-color: #F7F7FF; border-bottom: 1px solid #D5D5D3; border-left: 1px none #D5D5D3; padding-left: 5px; padding-right: 5px; }
        .style1 { width: 100%; }
        .style2 { font-weight: bold; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>异常捕获模块演示</h1>
        <p>
            此模块提供运行时异常的日志记录，可以将这些运行时异常深入地捕获并输出出来。输出格式可以是.txt文本文件、.htm网页文件形式的日志，也可以直接在B/S项目运行时的页面上直接显示出来。</p>
        <p>
            <b>程序集： &nbsp;<a href="http://udnz.com/Works/uoLib/">uoLib</a> </b>
            <br />
            <b>涉及的项目文件：</b>Web.config, Global.asax<br />
            <b>涉及的类：</b><a href="http://udnz.com/Works/uoLib/SDK/T_uoLib_Web_Debugger_ErrorReporter.htm">ErrorReporter</a>,
            <a href="http://udnz.com/Works/uoLib/SDK/T_uoLib_Web_Debugger_Configuration.htm">Configuration</a>,
            <a href="http://udnz.com/Works/uoLib/SDK/T_uoLib_Web_HttpHandlers_ServerErrorList.htm">
                ServerErrorList</a></p>
        <p>
            点击：<a href="NotFound.jpg">生成一个简单异常</a><span class="note">（点击此链接，将产生一个404错误“无法找到资源”，<b>并显示.NET默认的异常页面</b>）</span><br />
            点击：<a href="Default.aspx?GetError=1">生成一个复杂异常</a><span class="note">（点击此链接，将产生两个异常，其中一个是手动捕获的，另一个是程序中生成的。<b>并直接显示异常详细信息</b>）</span></p>
        <p>
            <span style="color: Red" class="style2">高级应用</span>：<a href="Default.aspx?GetError=2">此模块甚至能用于监视某个变量的值</a><span
                class="note">（点击此链接，将会产生包含有被监视变量信息的异常。）</span></p>
        <p>
            已配置好的<a href="Errors/list.aspx">异常日志列表页</a> （默认密码：<span style="color: Red">udnz.com</span>）</p>
    </div>
    </form>
    <h2>配置步骤</h2>
    <p>
        <strong>1.基本配置：将异常保存为日志</strong>
    </p>
    <p>
        要将异常保存为日志，在项目中引用uoLib之后只需要配置 Global.asax 的 Application_Error 事件即可。
    </p>
    <div class="style1">
        <span codelanguage="other"><table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <tr>
                <td colspan="2"><font face="Courier New">&nbsp;<span class="s5">&nbsp;</span><font
                    color="blue">public</font> <font color="blue">class</font> Global : System.Web.HttpApplication
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="green">// 其他代码...
                    </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue">protected</font>
                    <font color="blue">void</font> Application_Error(object sender, EventArgs e)
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="blue">if</font> (HttpContext.Current.AllErrors.Length &gt; 0)
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="blue">foreach</font> (Exception ex <font color="blue">in</font> HttpContext.Current.AllErrors)
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uoLib.Web.Debugger.ErrorReporter.RecordErrors(ex);
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HttpContext.Current.ApplicationInstance.CompleteRequest();
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="green">// 其他代码...
                    </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>
                    <br />
                </font></td>
            </tr>
        </table></span>
    </div>
    <p>
        这样设置之后，功能已经正常可用使用了，项目中一旦产生了运行时异常将自动被以日志形式记录下来。 默认的日志类型是.htm文件，记录在~/Errors/目录下（需要有写入权限）。
    </p>
    <p>
        <strong>2.进阶配置：文件类型、保存路径等的配置；查看保存下来的错误日志</strong>
    </p>
    <p>
        如果你要进一步设置文件类型、保存路径等，可以进一步地在 Global.asax 的 Application_Start 事件中配置。 这些配置信息将影响错误日志记录及日志列表展示的行为。
    </p>
    <div class="style1">
        <span codelanguage="other"><table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <tr>
                <td colspan="2"><font face="Courier New">&nbsp;<span class="s5">&nbsp;</span><font
                    color="blue">public</font> <font color="blue">class</font> Global : System.Web.HttpApplication
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="green">// 其他代码...
                    </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue">protected</font>
                    <font color="blue">void</font> Application_Start(object sender, EventArgs e)
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="green">// 错误日志功能的全局开关，默认为开启。 </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="green">// 如下设置与默认值相同，可以略去，此处仅为示例。 </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uoLib.Web.Debugger.Configuration.IsRecordErrors
                    = <font color="blue">true</font>;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="green">// 设置错误日志保存的路径。如下设置与默认值相同，可以略去，此处仅为示例。 </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uoLib.Web.Debugger.Configuration.LogPath
                    = &quot;~/Errors/&quot;;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="green">// 设置错误日志保存的类型。如下设置与默认值相同，可以略去，此处仅为示例。 </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uoLib.Web.Debugger.Configuration.LogType
                    = uoLib.Web.Debugger.LogType.Htm;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="green">// 其他代码...
                    </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>
                    <br />
                </font></td>
            </tr>
        </table></span>
    </div>
    <p>
        <strong>注意：</strong> <a href="http://udnz.com/Works/uoLib/SDK/P_uoLib_Web_Debugger_Configuration_IsRecordErrors.htm">
            IsRecordErrors</a> 属性必须和 Global.asax 中的 Application_Error 事件处理 配合设置。当 <a href="http://udnz.com/Works/uoLib/SDK/P_uoLib_Web_Debugger_Configuration_IsRecordErrors.htm">
                IsRecordErrors</a> 为 true 时，需按上面 Application_Error 事件中的示例代码设置后方能正式生效。 反之，<a href="http://udnz.com/Works/uoLib/SDK/P_uoLib_Web_Debugger_Configuration_IsRecordErrors.htm">IsRecordErrors</a>
        为 false 时，无论 Application_Error 事件中是否已设置调用处理，异常都不会被记录。
    </p>
    <p>
        更进一步地，如果您希望能以Web页面的形式访问到系统记录的异常日志，只需要配置 web.config 中的 httpHandlers 一节即可：
    </p>
    <div class="style1">
        <span codelanguage="other"><table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <tr>
                <td colspan="2"><font face="Courier New">&nbsp;<span class="s5">&nbsp;</span>&lt;?xml
                    version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&lt;configuration&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&lt;appSettings&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;clear/&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!--设置错误日志列表页的访问密码。设为空则关闭密码保护功能。不设置此配置节，则默认密码为“udnz.com”--&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;add
                    key=&quot;uoLib.ServerErrorList.LockPassword&quot; value=&quot;YourPasswordHere&quot;/&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/appSettings&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&lt;system.web&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;httpHandlers&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;add
                    verb=&quot;GET,POST&quot; path=&quot;Errors/List.aspx&quot; type=&quot;uoLib.Web.HttpHandlers.ServerErrorList,uoLib&quot;/&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/httpHandlers&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/system.web&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&lt;/configuration&gt;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>
                    <br />
                </font></td>
            </tr>
        </table></span>
    </div>
    <p>
        其中“Errors/List.aspx”可以是您希望的任意路径，不必存在这个.aspx文件。同时，如上例所示， 你也可以在 Web.config 中的 /configuration/appSettings/add[@key=&#39;uoLib.ServerErrorList.LockPassword&#39;]
        节设置异常日志列表页面的访问密码（不设置则默认密码为“udnz.com”）。</p>
    <p>
        <span style="color: #f00">注意：若您使用的是MVC模式，除配置 web.config 中的 httpHandlers 之外， 还需要在Global.asax.cs里，指明忽略错误日志列表页的URL，如下所示：</span></p>
    <div class="style1">
        <span codelanguage="other"><table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th>
                    &nbsp;
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <tr>
                <td colspan="2"><font face="Courier New">&nbsp;<span class="s5">&nbsp;</span><font
                    color="blue">public</font> <font color="blue">class</font> MvcApplication : System.Web.HttpApplication
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="green">// 其他代码...
                    </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue">public</font>
                    <font color="blue">static</font> <font color="blue">void</font> RegisterRoutes(RouteCollection
                    routes)
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;{
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;routes.IgnoreRoute(&quot;{resource}.axd/{*pathInfo}&quot;);
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font color="green">/*&nbsp;<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;指明忽略错误日志列表页的URL。<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;为与MVC的URL形式统一，这个路径可以是任意路径，比如“Errors”、“Errors/List”等等，<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;唯一需要满足的条件仅仅是必须与&nbsp;web.config&nbsp;里配置的路径一致即可。<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;注意：由于MVC项目默认生成的View文件夹下有一个&nbsp;web.config，其中配置了<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&lt;add&nbsp;path=&quot;*&quot;&nbsp;verb=&quot;*&quot;&nbsp;type=&quot;System.Web.HttpNotFoundHandler&quot;/&gt;<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;因此请勿将路径配置在View下，否则请将此配置删除<br />
                        &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/</font><br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;routes.IgnoreRoute(&quot;Errors/List.aspx&quot;);
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                        color="green">// 其他代码... </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;}
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;<font color="green">// 其他代码...
                    </font>
                    <br />
                    &nbsp;<span class="s5">&nbsp;</span>}
                    <br />
                </font></td>
            </tr>
        </table></span>
    </div>
    <p>
        &nbsp;</p>
</body>
</html>
