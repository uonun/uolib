﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace ErrorReporter
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            /*
             * 
             * 这只是个示例，如果默认配置满足你的要求，完全不用这些代码
             *  
             * 
            // 错误日志功能的全局开关，默认为开启。 
            // 如下设置与默认值相同，可以略去，此处仅为示例。 
            uoLib.Web.Debugger.Configuration.IsRecordErrors = true;

            // 设置错误日志保存的路径。如下设置与默认值相同，可以略去，此处仅为示例。 
            uoLib.Web.Debugger.Configuration.LogPath = "~/Errors/";

            // 设置错误日志保存的类型。如下设置与默认值相同，可以略去，此处仅为示例。 
            uoLib.Web.Debugger.Configuration.LogType = uoLib.Web.Debugger.LogType.Htm;
             */

            // 在具体的异常信息页面，末尾可以显示一个技术联系邮箱
            uoLib.Info.AdminEmail = "YourEamil@CouldBe.Here";
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (HttpContext.Current.AllErrors.Length > 0)
            {
                foreach (Exception ex in HttpContext.Current.AllErrors)
                {
                    uoLib.Web.Debugger.ErrorReporter.RecordErrors(ex);
                }
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}