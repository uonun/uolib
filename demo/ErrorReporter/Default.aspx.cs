﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ErrorReporter
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["GetError"] == "1")
            {
                // 手动生成一个异常
                GetOne();

                // 下面4个异常，模拟了系统生成的某些复杂异常。
                Exception ex1 = new Exception("A:“异常是我最先发现的！”", new NotImplementedException());
                Exception ex2 = new Exception("B:“报告， A 发现了异常！”", ex1);
                Exception ex3 = new Exception("C:“报告上级，B 说 A 发现了异常，但我无法处理！”", ex2);
                Exception ex4 = new Exception("D:“我是总裁，C，你报告上来的异常已清楚，忽略就是！”", ex3);

                // 用这个可以直接将异常显示出来
                uoLib.Web.Debugger.ErrorReporter.ShowServerError(ex4);
                Response.End();
            }
            else if (Request["GetError"] == "2")
            {
                // 监视一个变量的值
                GetAnother();

                Response.Write("已生成监视异常，请查看<a href=\"Errors/list.aspx\">异常日志列表页</a>");
                Response.End();
            }
        }

        /// <summary>
        /// 监视变量值
        /// </summary>
        private void GetAnother()
        {
            string tmp = "这里是 tmp 的值";

            // 没有产生异常，但我们可以用 ErrorReporter 来监视变量值哦
            uoLib.Web.Debugger.ErrorReporter.RecordErrors(new Exception("变量 tmp 的值是：" + tmp));
        }

        /// <summary>
        /// 手动生成一个异常
        /// </summary>
        private void GetOne()
        {
            try
            {
                string a = null;
                Response.Write(a.Length);
            }
            catch (Exception ex)
            {
                // 某些很难输出信息的地方，可以用这种方式人工地生成一个异常来监视哦~
                uoLib.Web.Debugger.ErrorReporter.RecordErrors(new Exception("这是一个手动捕获的异常", ex));
            }
        }
    }
}
