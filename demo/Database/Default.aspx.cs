﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uoLib.Data;
using System.Configuration;

namespace DatabaseDemo
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            Database db = new Database(ConnectionType.OleDb);
            db.ConnectionString = Database.CreateExampleConnectionString(Server.MapPath("~/App_Data/access.mdb"), "", "", DatabaseType.Access2007, db.ConnectionType);

            DbColumn[] cols = new DbColumn[]{
                new DbColumn(){ ColumnName="content_Title", DbType= System.Data.DbType.String, Value=string.Format("Insert方法测试({0})",DateTime.Now)},
                new DbColumn(){ ColumnName="content_Author", DbType= System.Data.DbType.Int32, Value=10},
                // and other columns.
            };
            int newID;
            db.Insert("BlogContent", cols, out newID);
            Response.Write(newID);
            */

            Database db = new Database(ConnectionType.SqlClient);
            db.ConnectionString = "YOURCONNSTRING";

            string varString = "var enabled.";
            DbColumn[] cols = new DbColumn[]{
                // 构造你要填写的字段。值可以是各种类型的变量/常量。
                new DbColumn(){ ColumnName="col1", DbType= System.Data.DbType.String, Value=varString},
                new DbColumn(){ ColumnName="col2", DbType= System.Data.DbType.Int32, Value=10},
                new DbColumn(){ ColumnName="col3", DbType= System.Data.DbType.Boolean, Value=true},
                // and other columns.
            };
            int newID;
            bool isOk = db.Insert("TABLENAME", cols, out newID);
            if (isOk)
            {
                Response.Write(string.Format("Insert 执行成功，新主键值 = {0}", newID));
            }
            else
            {
                Response.Write("Insert 失败，你可以从异常信息中找到实际执行的Insert语句，从而判断失败原因。");
            }

            //类似的方法，可以对行执行Update操作
            int n = db.Update("TABLENAME", cols, " ID=1 ");
        }
    }
}
