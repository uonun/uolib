using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using uoLib.Data;
using uoLib.Data.Singleton;

namespace uoLib.example
{
    static class showDatabase
    {
        public static void Start()
        {
            try
            {
                //uoLib.Data.Database db = new Data.Database(uoLib.Data.ConnectionType.OleDb);
                //db.CreateConnection(Database.CreateExampleConnectionString(@"../../files/test.mdb", "", "", DatabaseType.Access, ConnectionType.OleDb));
                OleDbAccess db = OleDbAccess.Instance;
                //DbConnection conn = db.CreateConnection();

                DateTime a = db.ReturnValue<DateTime>("select top 1 content_PostTime from BlogContent order by content_ID desc", "content_PostTime");
                Console.WriteLine(a);

                #region 对 Access 数据库进行数据读取操作
                Console.WriteLine("\n对 Access 数据库进行数据读取操作:\n----------------------------------------------");
                try
                {
                    DateTime start1, start2;
                    string sql = new StringBuilder("select top 5 content_ID,content_Title,content_PostTime from BlogContent order by content_ID desc").ToString();

                    //使用 DbDataReader 打开数据
                    Console.WriteLine("----Data_From_DbDataReader----------------");
                    start1 = DateTime.Now;
                    //if (conn.State != ConnectionState.Open) { conn.Close(); conn.Open(); }
                    DbDataReader r = db.CreateDataReader(sql);
                    while (r.Read())
                    {
                        Console.WriteLine(string.Format("\t{0}: {1}（{2}）", r[0], r[1], r[2]));
                    }
                    //if (conn.State != ConnectionState.Closed) { conn.Close(); }
                    start2 = DateTime.Now;
                    Console.WriteLine("\tEnd");
                    //Console.WriteLine("\tConn.State = {0}", conn.State.ToString());
                    TimeSpan ts2 = start2 - start1;
                    Console.WriteLine("\tTime = {0} ms\n", ts2.Milliseconds);

                    //使用 DbDataAdapter 打开数据
                    Console.WriteLine("----Data_From_DbDataAdapter----------------");
                    start1 = DateTime.Now;
                    DbDataAdapter adp = db.CreateDataAdapter();
                    adp.SelectCommand = db.CreateCommand(sql);
                    DataSet ds = new DataSet("Table_From_DbDataAdapter");
                    adp.Fill(ds);
                    if (ds.Tables.Count > 0)
                    {
                        DataTableCollection dc = ds.Tables;
                        IEnumerator ie = dc.GetEnumerator();
                        while (ie.MoveNext())
                        {
                            DataTable dt = (DataTable)ie.Current;
                            foreach (DataRow dr in dt.Rows)
                            {
                                Console.WriteLine(string.Format("\t{0}: {1}（{2}）", dr.ItemArray[0], dr.ItemArray[1], dr.ItemArray[2]));
                            }

                        }
                    }
                    start2 = DateTime.Now;
                    Console.WriteLine("\tEnd");
                    //Console.WriteLine("\tConn.State = {0}", conn.State.ToString());
                    TimeSpan ts1 = start2 - start1;
                    Console.WriteLine("\tTime = {0} ms\n", ts1.Milliseconds);



                    //使用 SelectData 打开数据
                    Console.WriteLine("----Data_From_SelectData----------------");
                    start1 = DateTime.Now;
                    string sql2 = "select top 10 content_ID,content_Title,content_PostTime from BlogContent WHERE content_ID<@cid AND content_PostTime<@t order by content_ID desc";
                    DbParameter[] paras = {
                        db.CreateParameter("@cid", DbType.Int32,100)
                        ,db.CreateParameter("@t", DbType.DateTime,DateTime.Parse("2006-5-1"))
                    };
                    DbCommand cmd = db.CreateCommand(sql2, paras);
                    DataSet ds2 = db.SelectData(cmd, new BaseProvider.CommandExecuedCallback(db_OnCommandExecuted));
                    if (ds2.Tables.Count > 0)
                    {
                        DataTableCollection dc = ds2.Tables;
                        IEnumerator ie = dc.GetEnumerator();
                        while (ie.MoveNext())
                        {
                            DataTable dt = (DataTable)ie.Current;
                            foreach (DataRow dr in dt.Rows)
                            {
                                Console.WriteLine(string.Format("\t{0}: {1}（{2}）", dr.ItemArray[0], dr.ItemArray[1], dr.ItemArray[2]));
                            }
                        }
                    }
                    start2 = DateTime.Now;
                    Console.WriteLine("\tEnd");
                    //Console.WriteLine("\tConn.State = {0}", conn.State.ToString());
                    TimeSpan ts3 = start2 - start1;
                    Console.WriteLine("\tTime = {0} ms\n", ts3.Milliseconds);




                    Console.WriteLine("\tConnection is Ok!\n");
                }
                catch (Exception e)
                {
                    Console.WriteLine("\tError: " + e.Message);
                    Console.WriteLine("\t" + db.ConnectionString);
                    Console.WriteLine();
                }
                #endregion

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
            }

        }

        static void db_OnCommandExecuted(object sender, ExecuteEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\t回调db_OnCommandExecuted被触发：{0}", e);
            Console.WriteLine("\t\te.Command.CommandText：{0}", e.Command.CommandText);
            Console.WriteLine("\t\te.ElapsedMilliseconds：{0}", e.ElapsedMilliseconds);
            Console.WriteLine("\t\te.Errors：{0}", e.Errors);
            Console.WriteLine("\t\te.RecordsAffected：{0}", e.RecordsAffected);
            Console.ResetColor();
        }

    }
}
