﻿using System;
using System.Collections.Generic;
using System.Text;
using uoLib.Data.Singleton;
using System.Data;
using System.Data.Common;
using uoLib.Common;

namespace uoLib.example
{
    static class SqlClientSqlServerTest
    {
        public static void Start()
        {
            try
            {
                SqlClientSqlServer db = SqlClientSqlServer.Instance;

                #region 使用 SqlClientSqlServer 单件管理数据库
                Console.WriteLine("\n使用 SqlClientSqlServer 单件管理数据库:\n----------------------------------------------");
                try
                {
                    Console.Write("打开 conn ");
                    DbConnection conn = db.CreateConnection();
                    conn.Open();
                    Console.WriteLine("成功！");
                    //--------------------------------
                    Console.Write("将数据库切换为 master ");
                    conn.ChangeDatabase("master");
                    Console.WriteLine("成功！");
                    //--------------------------------

                    #region 数据库的添加删除测试
                    Console.Write("是否进行数据库的添加删除测试？Y/N： ");
                    string yn = Console.ReadLine();
                    if (yn.ToUpper() == "Y")
                    {
                        string dbname = uoLib.Common.Functions.GetRandomNum(6);
                        Console.Write("添加数据库 {0} ", dbname);
                        SqlClientSqlServer.CreateDatabase(db.ConnectionString,dbname);
                        Console.WriteLine("成功！");
                        //--------------------------------
                        Console.Write("检查数据库 {0} 是否已存在：", dbname);
                        bool isExist = SqlClientSqlServer.IsDatabaseExist(db.ConnectionString, dbname);
                        if (isExist)
                            Console.WriteLine("存在！");
                        else
                            Console.WriteLine("不存在！");
                        //--------------------------------
                        Console.Write("删除数据库 {0} ：", dbname);
                        bool isOk = SqlClientSqlServer.DeleteDatabase(db.ConnectionString, dbname);
                        if (isOk)
                            Console.WriteLine("成功！");
                        else
                            Console.WriteLine("不成功！");
                        //--------------------------------
                        string tmpDb = uoLib.Common.Functions.GetRandomNum(6);
                        Console.Write("删除不存在的数据库 {0} ：", tmpDb);
                        try
                        {
                            SqlClientSqlServer.DeleteDatabase(db.ConnectionString, tmpDb);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("输出异常：{0}", e.Message);
                        }
                    }
                    #endregion
                    //--------------------------------
                    #region 列出系统中所有数据库
                    Console.WriteLine("列出系统中所有数据库：");
                    DataSet ds = SqlClientSqlServer.ListDatabases(db.ConnectionString);
                    if (ds != null)
                    {
                        foreach (DataRow r in ds.Tables[0].Rows)
                        {
                            Console.WriteLine("\t{0}", r[0]);
                        }
                    }
                    else
                    {
                        Console.WriteLine("\t无用户数据库。");
                    }
                    #endregion
                    //--------------------------------
                    #region 数据库、数据表、字段信息详细检查
                    Console.WriteLine("数据库、数据表、字段信息详细检查：");
                    Console.Write("请输入要检索的数据库名：");
                    string tmp = Console.ReadLine();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        DataSet ds2 = SqlClientSqlServer.ListTables(db.ConnectionString, tmp);
                        if (ds2 != null)
                        {
                            Console.WriteLine("\t此数据库有如下表：");
                            foreach (DataRow r in ds2.Tables[0].Rows)
                            {
                                Console.WriteLine("\t{0}", r[0]);
                            }

                            Console.Write("请输入要检索的数据表名：");
                            string tmp1 = Console.ReadLine();
                            if (!string.IsNullOrEmpty(tmp1))
                            {
                                DataSet ds3 = SqlClientSqlServer.ListColumns(db.ConnectionString, tmp, tmp1);
                                if (ds3 != null)
                                {
                                    Console.WriteLine("此表有如下字段：");
                                    Console.WriteLine("ColumnName      PrimaryKey  Type     Length  NullAble Default     ");
                                    Console.WriteLine("-----------------------------------------------------------");
                                    foreach (DataRow r in ds3.Tables[0].Rows)
                                    {
                                        Console.WriteLine("{0,-15} {1,10}  {2,-8} {3,6}  {4,8} {5,12}"
                                            ,r["ColumnName"]
                                            , r["PrimaryKey"]
                                            , r["Type"]
                                            , r["Length"]
                                            , r["NullAble"]
                                            , r["Default"]
                                            );
                                    }

                                    DataSet trs = SqlClientSqlServer.ListTriggers(db.ConnectionString, tmp, tmp1);
                                    if (trs != null) {
                                        Console.WriteLine("此表有如下触发器：");
                                        foreach (DataRow r in trs.Tables[0].Rows)
                                        {
                                            Console.WriteLine("\t{0}", r[0]);
                                        }
                                    }


                                    Console.Write("判断字段列是否存在，请输入字段名：");
                                    string col = Console.ReadLine();
                                    bool isexsit = SqlClientSqlServer.IsColumnExists(db.ConnectionString, tmp, tmp1, col);
                                    if (isexsit)
                                        Console.WriteLine("该列存在！");
                                    else
                                        Console.WriteLine("该列不存在！");
                                }
                                else {
                                    Console.WriteLine("\t未找到指定数据表。");
                                }
                            }
                            else {
                                Console.WriteLine("\t未输入字段列。");
                            }
                        }
                        else
                        {
                            Console.WriteLine("\t无数据表。");
                        }
                    }
                    else
                    {
                        Console.WriteLine("\t未输入数据库名。");
                    }
                    #endregion
                    //--------------------------------
                    //--------------------------------
                    //--------------------------------
                }
                catch (Exception e)
                {
                    Console.WriteLine("\tError: " + e.ToString());
                    Console.WriteLine("\t" + db.ConnectionString);
                    Console.WriteLine();
                }
                #endregion

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
            }

        }
    }
}
