using System;
using System.Collections;
using System.Collections.Generic;
using uoLib.Common;

namespace uoLib.example
{
    static class showFunctions
    {
        public static void Start()
        {
            try
            {

                //------------------------------------------------
                Console.WriteLine("演示GetRadomNum()");
                for (int i = 5;i <= 9;i++)
                {
                    Console.WriteLine("\t{0} 位随机数: {1}", i, uoLib.Common.Functions.GetRandomNum(i));
                }
                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示FormatFileSize(),GetFileSizeFromString()");
                List<long> list1 = new List<long>();
                list1.Add(1073741824);
                list1.Add(1048576);
                list1.Add(1024);
                list1.Add(154);
                list1.Add(5464665464);
                list1.Add(5456564665464);
                IEnumerator ie = list1.GetEnumerator();
                string tmpSize;
                Console.WriteLine("\t{0,13}\t{1,15} -> {2,20}", " ", "FormatFileSize", "GetFileSizeFromString");
                while (ie.MoveNext())
                {
                    tmpSize = uoLib.Common.Functions.FormatFileSize((long)ie.Current);
                    Console.WriteLine("\t{0,13}:\t{1,15} -> {2,20}", ie.Current, tmpSize, uoLib.Common.Functions.GetFileSizeFromString(tmpSize));
                }
                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示IsNullOrEmptyStr()");
                Console.WriteLine("\tIsNullOrEmptyStr(String.Empty): {0}", uoLib.Common.Functions.IsNullOrEmptyStr(String.Empty));
                Console.WriteLine("\tIsNullOrEmptyStr(null): {0}", uoLib.Common.Functions.IsNullOrEmptyStr(null));
                Console.WriteLine("\tIsNullOrEmptyStr(' '): {0}", uoLib.Common.Functions.IsNullOrEmptyStr(" "));
                Console.WriteLine("\tIsNullOrEmptyStr('sdf'): {0}", uoLib.Common.Functions.IsNullOrEmptyStr("sdf"));
                Console.WriteLine("\tIsNullOrEmptyStr(1): {0}", uoLib.Common.Functions.IsNullOrEmptyStr(1.ToString()));
                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示HtmlToJs()");
                Console.WriteLine("\tHtmlToJs(String.Empty): {0}", uoLib.Common.Functions.HtmlToJs(String.Empty));
                Console.WriteLine("\tHtmlToJs(\" \" \"): {0}", uoLib.Common.Functions.HtmlToJs(Char.ConvertFromUtf32(34)));
                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示IsValidName()");
                Console.WriteLine("\tIsValidName(String.Empty): {0}", uoLib.Common.Functions.IsValidName(String.Empty));
                Console.WriteLine("\tIsValidName(null): {0}",uoLib.Common.Functions.IsValidName(null));
                Console.WriteLine("\tIsValidName(\"dffd.gfg\"): {0}", uoLib.Common.Functions.IsValidName("dffd.gfg"));
                Console.WriteLine("\tIsValidName(\"dffd*gfg\"): {0}", uoLib.Common.Functions.IsValidName("dffd*gfg"));
                Console.WriteLine("\tIsValidName(\"dffd gfg\"): {0}", uoLib.Common.Functions.IsValidName("dffd gfg"));
                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示IsValidEmail()");
                Console.WriteLine("\tIsValidEmail(String.Empty): {0}", uoLib.Common.Functions.IsEmail(String.Empty));
                Console.WriteLine("\tIsValidEmail(null): {0}", uoLib.Common.Functions.IsEmail(null));
                Console.WriteLine("\tIsValidEmail(\"sdfsdklgf\"): {0}", uoLib.Common.Functions.IsEmail("sdfsdklgf"));
                Console.WriteLine("\tIsValidEmail(\"sdfsd@klgf\"): {0}", uoLib.Common.Functions.IsEmail("sdfsd@klgf"));
                Console.WriteLine("\tIsValidEmail(\"sdfsd@.com\"): {0}", uoLib.Common.Functions.IsEmail("sdfsd@.com"));
                Console.WriteLine("\tIsValidEmail(\"sdfsd@aa.com.\"): {0}", uoLib.Common.Functions.IsEmail("sdfsd@aa.com."));
                Console.WriteLine("\tIsValidEmail(\"sdADsgGsd@klRf.com\"): {0}", uoLib.Common.Functions.IsEmail("sdADsgGsd@klRf.com"));

                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示CutStr()");
                Console.WriteLine("\tCutStr(\"<span>dfgdgs5d4fagd</span>gdfg\",5): {0}", uoLib.Common.Functions.CutStr("<span>dfgdgs5d4fagd</span>gdfg", 5));
                Console.WriteLine("\tCutStr(\"<span>dfgdgs5d4fagd</span>gdfg\",8,true): {0}", uoLib.Common.Functions.CutStr("<span>dfgdgs5d4fagd</span>gdfg", 8, true));
                Console.WriteLine("\tCutStr(\"中文按几个字符算的呢？\",5): {0}", uoLib.Common.Functions.CutStr("中文按几个字符算的呢？", 5));
                Console.WriteLine("\tCutStr(\"中文\",5): {0}", uoLib.Common.Functions.CutStr("中文", 5));
                Console.WriteLine("\tCutStr(\"五个中文字\",5): {0}", uoLib.Common.Functions.CutStr("五个中文字", 5));

                Console.WriteLine();

                //------------------------------------------------
                Console.WriteLine("演示GetSafeFileName()");
                Console.WriteLine("\tIsFileName('new file.abc.txt'): {0}", uoLib.Common.Functions.IsFileName("new file.abc.txt"));
                Console.WriteLine("\tIsFileName('new fi\\le?abc.txt'): {0}", uoLib.Common.Functions.IsFileName("new fi\\le?abc.txt"));
                Console.WriteLine("\tGetSafeFileName('new fi\\le?abc.txt'): {0}", uoLib.Common.Functions.GetSafeFolderName("new fi\\le?abc.txt"));
                Console.WriteLine();


                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        internal static void GetWebRequest()
        {

            //------------------------------------------------
            Console.WriteLine("演示GetWebRequest，GET 方式");
            Console.Write("请输入网址：");
            string url = Console.ReadLine();
            try
            {
                string htm = uoLib.Common.Functions.GetWebRequest(url, null, "GET", System.Text.Encoding.Default);
                Console.WriteLine("\t{0}", htm);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
            Console.WriteLine();
        }

        internal static void WriteEventLog()
        {

            //------------------------------------------------
            try
            {
                Console.WriteLine("演示WriteEventLog");
                Console.WriteLine("\tWriteEventLog(string,System.Diagnostics.EventLogEntryType)");
                Logger.WriteEventLog("感谢使用uoLib，这是演示程序产生的一次记录", System.Diagnostics.EventLogEntryType.Information);
                Console.WriteLine("\tWriteEventLog(string,string,System.Diagnostics.EventLogEntryType,string)");
                Logger.WriteEventLog("uoLib.DemoApp", "感谢使用uoLib，这是演示程序产生的一次记录", System.Diagnostics.EventLogEntryType.Information, "uoLib");
                Console.WriteLine("\t已写入系统日志，请打开计算机管理->系统工具->事件查看器");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
            Console.WriteLine();
        }
    }
}
