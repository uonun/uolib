using System;
using System.Collections;
using System.Collections.Generic;
using uoLib;
using uoLib.Authentication;
using uoLib.Common;

namespace uoLib.example
{
    static class Power
    {
        public static void Start()
        {

            //------------------------------------------------
            Console.WriteLine("演示FlagBehavior");

            PowerItemExample newPower = PowerItemExample.查看用户 | PowerItemExample.增改用户 | PowerItemExample.关闭网站;
            Console.WriteLine("\t初始化权限: {0}", newPower);

            FlagBehavior<PowerItemExample> member = new FlagBehavior<PowerItemExample>(newPower);
            Console.WriteLine("");
            Console.WriteLine("=============================\n\t权限ToString():\n{0}", member);
            Console.WriteLine("");

            PowerItemExample addedPower = PowerItemExample.查看用户 | PowerItemExample.删除用户;
            Console.WriteLine("=============================\n\t赋予权限：{0}", addedPower);
            member.Append(addedPower);
            Console.WriteLine("\t权限ToString():\n{0}", member);
            Console.WriteLine("");

            PowerItemExample deletedPower = PowerItemExample.查看用户 | PowerItemExample.管理其他管理员 | PowerItemExample.关闭网站;
            Console.WriteLine("=============================\n\t剥夺权限：{0}", deletedPower);
            member.Remove(deletedPower);
            Console.WriteLine("\t权限ToString():\n{0}", member);
            Console.WriteLine("");

            PowerItemExample togglePower = PowerItemExample.删除用户 | PowerItemExample.管理其他管理员;
            Console.WriteLine("=============================\n\t权限取反：{0}", togglePower);
            member.Toggle(togglePower);
            Console.WriteLine("\t权限ToString():\n{0}", member);
            Console.WriteLine("");

            Console.WriteLine("=============================\n\t赋予所有权限：{0}", "111111");
            member.Append((PowerItemExample)Enum.Parse(typeof(PowerItemExample), "63")); //63是二进制数111111的int形式
            Console.WriteLine("\t权限ToString():\n{0}", member);
            Console.WriteLine("");

            Console.WriteLine("=============================\n\t剥夺所有权限：{0}", "111111");
            member.Remove((PowerItemExample)Enum.Parse(typeof(PowerItemExample), "63")); //63是二进制数111111的int形式
            Console.WriteLine("\t权限ToString():\n{0}", member);
            Console.WriteLine("");

            Console.WriteLine();

            Console.WriteLine("=============================\n\t操作符连写：");
            FlagBehavior<PowerItemExample> temp = new FlagBehavior<PowerItemExample>();
            temp.Append(PowerItemExample.查看系统日志 | PowerItemExample.关闭网站)
                .Remove(PowerItemExample.关闭网站 | PowerItemExample.删除用户);
            Console.WriteLine("\ttempToString():\n{0}", temp);
            
            Console.WriteLine("=============================\n\t常量：");
            Console.WriteLine("\tFlagBehavior<PowerItemExample>.FullFlagU8 = {0}", FlagBehavior<PowerItemExample>.FullFlagU8);
            Console.WriteLine("\tFlagBehavior<PowerItemExample>.MaxFlagU8 = {0}", FlagBehavior<PowerItemExample>.MaxFlagU8);
        }
    }

    [Flags]
    public enum PowerItemExample : ulong
    {
        查看用户 = 1,
        增改用户 = 2,
        删除用户 = 4,

        查看系统日志 = 8,
        管理其他管理员 = 16,
        关闭网站 = 32
    }

}