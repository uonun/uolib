using System;
using System.Collections;
using System.Collections.Generic;

namespace uoLib.example
{
    static class Win32
    {
        public static void Start()
        {

            //------------------------------------------------
            Console.WriteLine("演示GetTotalMemory()");
            Console.WriteLine("\tGetTotalMemory: {0}", uoLib.Common.Functions.GetTotalPhysicalMemory());

            Console.WriteLine();
        }
    }
}
