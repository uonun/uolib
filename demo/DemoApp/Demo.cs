
using System;
using uoLib.example;

class Demo
{
    public static void Main(string[] args)
    {
        string act = Init();
        switch (act) { 
            case "1":
                showInfo.Start();
                break;
            case "2":
                showFunctions.Start();
                break;
            case "3":
                showConnection.Start();
                break;
            case "4":
                showDatabase.Start();
                break;
            case "5":
                SqlClientSqlServerTest.Start();
                break;
            case "6":
                Win32.Start();
                break;
            case "7":
                Power.Start();
                break;
            case "8":
                showFunctions.GetWebRequest();
                break;
            case "9":
                showFunctions.WriteEventLog();
                break;
            default:
                break;
        }
        if (act.ToLower() == "clr") { Console.Clear(); }
        if (act.ToLower() != "exit"){ Main(args); }
    }
    protected static string Init() {
        Console.Beep();
        Console.Title = "uoLib类库应用范例测试";
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("\n============uoLib类库测试：===========\n");
        Console.ResetColor();
        Console.WriteLine("\t1.显示基本信息。");
        Console.WriteLine("\t2.全局函数测试。");
        Console.WriteLine("\t3.数据库连接测试。");
        Console.WriteLine("\t4.数据读取测试。");
        Console.WriteLine("\t5.使用 SqlClientSqlServer 单件管理SQL数据库。");
        Console.WriteLine("\t6.获取系统信息。");
        Console.WriteLine("\t7.权限模块测试。");
        Console.WriteLine("\t8.演示 GetWebRequest。");
        Console.WriteLine("\t9.演示 WriteEventLog。");
        Console.WriteLine();
        Console.WriteLine("其他命令：");
        Console.WriteLine("\texit 退出测试。");
        Console.WriteLine("\tclr 清屏。");
        Console.WriteLine();
        Console.Write("请输入选项序号：");
        string act = Console.ReadLine();
        return act;
    }
}

