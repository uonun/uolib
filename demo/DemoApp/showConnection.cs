using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.Common;
using uoLib.Data;
using uoLib.Web.Debugger;
//using uoLib.Data.Advanced;

namespace uoLib.example
{
    static class showConnection
    {
        public static void Start()
        {

            Console.WriteLine("\n\nDatabase.TestConnection() 方法测试\n----------------------------------------------");
            Database db2 = new Database(uoLib.Data.ConnectionType.OleDb);
            db2.ConnectionString = Database.CreateExampleConnectionString(@"../../files/手机图片.xls", "df", "df", DatabaseType.Excel, db2.ConnectionType);
            Console.WriteLine("返回：" + db2.TestConnection().ToString());
            Console.WriteLine("----------------------------------------------\n\n");


            Console.WriteLine("\n====================== Error Testing Begin ====================\n");
            try { Database.CreateExampleConnectionString("../Access.mdb", null, null, DatabaseType.Access, ConnectionType.SqlClient); }
            catch (Exception e) { Console.WriteLine(e.Message); }
            try { Database.CreateExampleConnectionString("127.0.0.1", "NorthWind", "sa", "", ConnectionType.Unsupported); }
            catch (Exception e) { Console.WriteLine(e.Message); }
            Console.WriteLine("\n====================== Error Testing End ======================\n\n");

            Collection<object[]> Cons = new Collection<object[]>();

            Database Excel = new Database(ConnectionType.OleDb);
            Excel.ConnectionString = Database.CreateExampleConnectionString(@"../../files/手机图片.xls", "df", "df", DatabaseType.Excel, Excel.ConnectionType);
            Cons.Add(new object[] { "Excel", Excel.CreateConnection() });

            Database Excel2007 = new Database(ConnectionType.OleDb);
            Excel2007.ConnectionString = Database.CreateExampleConnectionString(@"../../files/MS 161 Clarksdale 2008 Study Reference Locations.xlsx", "", "", DatabaseType.Excel2007, Excel2007.ConnectionType);
            Cons.Add(new object[] { "Excel2007", Excel2007.CreateConnection() });

            Database Access = new Database(ConnectionType.OleDb);
            Access.ConnectionString = Database.CreateExampleConnectionString(@"../../files/test.mdb", "", "", DatabaseType.Access, Access.ConnectionType);
            Cons.Add(new object[] { "Access", Access.CreateConnection() });

            Database Access2007 = new Database(ConnectionType.OleDb);
            Access2007.ConnectionString = Database.CreateExampleConnectionString(@"../../files/Students.accdb", "", "", DatabaseType.Access2007, Access2007.ConnectionType);
            Cons.Add(new object[] { "Access2007", Access2007.CreateConnection() });

            Database SQLite = new Database(ConnectionType.Odbc);
            SQLite.ConnectionString = Database.CreateExampleConnectionString(@"../../files/BitracDB.db3", "", "", DatabaseType.SQLite, SQLite.ConnectionType);
            Cons.Add(new object[] { "SQLite", SQLite.CreateConnection() });

            Database DSN = new Database(ConnectionType.Odbc);
            DSN.ConnectionString = Database.CreateExampleConnectionString(@"uoBlog", "uonun", "psw", DatabaseType.DSN, DSN.ConnectionType);
            Cons.Add(new object[] { "DSN", DSN.CreateConnection() });

            Database DSNFile = new Database(ConnectionType.Odbc);
            DSNFile.ConnectionString = Database.CreateExampleConnectionString(@"../../files/uoBlog.dsn", "uonun", "psw", DatabaseType.DSN, DSNFile.ConnectionType);
            Cons.Add(new object[] { "DSN(*.dsn)", DSNFile.CreateConnection() });

            Database SQL = new Database(ConnectionType.SqlClient);
            SQL.ConnectionString = Database.CreateExampleConnectionString("udnz.com,2433", "ourome_GLOBAL", "uonun", "psw", SQL.ConnectionType);
            Cons.Add(new object[] { "SQL", SQL.CreateConnection() });

            #region 数据库连接测试
            Console.WriteLine("\n\n================= Connection Testing Begin!====================\n\n");
            foreach (object[] o in Cons)
            {
                DbConnection Conn = (DbConnection)o[1];
                try
                {
                    Conn.Open();
                    Console.WriteLine("{0}: Connection is Ok!", o[0]);
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine(Conn.ConnectionString);
                    Console.WriteLine("\n");
                    Conn.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0}: Error!", o[0]);
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine(Conn.ConnectionString);
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine(e.Message);
                    Console.WriteLine("\n");
                }
            }
            Console.WriteLine("\n\n================= Connection Testing End!======================\n\n");
            #endregion


            Console.WriteLine("Oracle:\n----------------------------------------------");
            Console.WriteLine("\t实在找不到数据库来测试…有Oracle数据库的朋友谁能提供个测试空间？或者测试了告诉我结果……" + Info.AdminEmail + "\n");

        }

    }
}
