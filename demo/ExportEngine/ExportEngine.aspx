﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="uoLib.Data" %>
<%@ Import Namespace="uoLib.Data.Export" %>
<%@ Import Namespace="System.Collections.Generic" %>
<script runat="server" language="c#">
    private Database Db;
    private ExportEngine<DataRowView> exp = new ExportEngine<DataRowView>();

    public void Page_Init(object obj, EventArgs e)
    {
        Db = new Database(ConnectionType.OleDb);
        Db.ConnectionString = Database.CreateExampleConnectionString(Server.MapPath("~/App_Data/Demodb.mdb"), "", "", DatabaseType.Access, ConnectionType.OleDb);
    }

    public void Page_Load(object obj, EventArgs e)
    {
        buExplor1.Click += new EventHandler(buExplor1_Click);
        buExplor2.Click += new EventHandler(buExplor2_Click);
        buExplor3.Click += new EventHandler(buExplor3_Click);

        Button1.Click += new EventHandler(Button1_Click);
        Button2.Click += new EventHandler(Button2_Click);
        Button3.Click += new EventHandler(Button3_Click);
    }

    void Button3_Click(object sender, EventArgs e)
    {
        exp.FileType = FileType.Htm;
        buExplor3_Click(buExplor3, null);
    }

    void Button2_Click(object sender, EventArgs e)
    {
        exp.FileType = FileType.Htm;
        buExplor2_Click(buExplor3, null);
    }

    void Button1_Click(object sender, EventArgs e)
    {
        exp.FileType = FileType.Htm;
        buExplor1_Click(buExplor3, null);
    }

    void buExplor3_Click(object sender, EventArgs e)
    {
        // 1.【可选】配置要导出的数据列
        SetColumes();

        // 2.【可选】配置导出表的标题、备注
        exp.Options.TitleText = "导出示例（指定数据列、标题、备注，使用默认样式）";
        exp.Options.RemarkHtml = "<span style=\"color:#FF0000; font-weight:bolder;\">备注支持Html标签，可任意发挥</span>，更多设置请见SDK："
        + "<a href=\"http://udnz.com/Works/uoLib/\" target=\"_blank\">http://udnz.com/Works/uoLib/</a>";


        // 3.设置样式
        SetStyle();
        
        // 4.获取数据源
        DataSet data = Db.SelectData("SELECT Products.ProductID, Products.ProductName,"
            + " Suppliers.CompanyName, Products.CategoryID, Products.UnitPrice,"
            + " Products.UnitsInStock FROM Products INNER JOIN Suppliers ON"
            + " Products.SupplierID = Suppliers.SupplierID;");

        // 5.导出Excel！
        exp.ExportToExcel(data);
    }

    private void SetStyle() {
        exp.Options.TitleFontName = DropDownList0.SelectedItem.Text;
        exp.Options.TitleFontSize = FontUnit.Parse(DropDownList1.SelectedItem.Text);
        exp.Options.TitleForeColor = Color.FromName(DropDownList2.SelectedItem.Text);
        exp.Options.TitleBackColor = Color.FromName(DropDownList3.SelectedItem.Text);
        exp.Options.TitleHeight = Unit.Parse(DropDownList4.SelectedItem.Text);

        exp.Options.HeaderFontName = DropDownList5.SelectedItem.Text;
        exp.Options.HeaderFontSize = FontUnit.Parse(DropDownList6.SelectedItem.Text);
        exp.Options.HeaderStyle.ForeColor = Color.FromName(DropDownList7.SelectedItem.Text);
        exp.Options.HeaderStyle.BackColor = Color.FromName(DropDownList8.SelectedItem.Text);
        exp.Options.HeaderStyle.Height = Unit.Parse(DropDownList9.SelectedItem.Text);

        exp.Options.RowFontName = DropDownList10.SelectedItem.Text;
        exp.Options.RowFontSize = FontUnit.Parse(DropDownList11.SelectedItem.Text);
        exp.Options.RowStyle.ForeColor = Color.FromName(DropDownList12.SelectedItem.Text);
        exp.Options.RowStyle.BackColor = Color.FromName(DropDownList13.SelectedItem.Text);
        exp.Options.RowStyle.Height = Unit.Parse(DropDownList14.SelectedItem.Text);

        exp.Options.AlternatingRowFontName = DropDownList15.SelectedItem.Text;
        exp.Options.AlternatingRowFontSize = FontUnit.Parse(DropDownList16.SelectedItem.Text);
        exp.Options.AlternatingRowStyle.ForeColor = Color.FromName(DropDownList17.SelectedItem.Text);
        exp.Options.AlternatingRowStyle.BackColor = Color.FromName(DropDownList18.SelectedItem.Text);
        exp.Options.AlternatingRowStyle.Height = Unit.Parse(DropDownList19.SelectedItem.Text);
    }

    /// <summary>
    /// 导出操作只需几个简单的步骤即可完成
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void buExplor2_Click(object sender, EventArgs e)
    {
        // 1.【可选】配置要导出的数据列
        SetColumes();

        // 2.【可选】配置导出表的标题、备注
        exp.Options.TitleText = "导出示例（指定数据列、标题、备注，使用默认样式）";
        exp.Options.RemarkHtml = "（这里是备注）此示例指定了导出的数据列、"
            + "表格标题、备注信息，使用了默认样式导出";

        
        // 3.获取数据源
        DataSet data = Db.SelectData("SELECT Products.ProductID, Products.ProductName,"
            + " Suppliers.CompanyName, Products.CategoryID, Products.UnitPrice,"
            + " Products.UnitsInStock FROM Products INNER JOIN Suppliers ON"
            + " Products.SupplierID = Suppliers.SupplierID;");
        
        // 4.导出Excel！
        exp.ExportToExcel(data);
    }

    /*** 以下代码，根据实际需要配置您要导出的数据列【可选】，此处仅用于演示 ***/
    
    /// <summary>
    /// 此方法中配置了若干个要导出的数据列
    /// </summary>
    private void SetColumes() {
        DataColumn<DataRowView> col;

        // 此列演示了 DataFormatString 的使用
        col = new DataColumn<DataRowView>();
        col.DataField = "ProductID";
        col.HeaderText = "商品编号";
        col.DataFormatString = "P{0:000000}";
        exp.Columns.Add(col);

        col = new DataColumn<DataRowView>();
        col.DataField = "ProductName";
        col.HeaderText = "商品名称";
        exp.Columns.Add(col);

        // 此列数据，通过SQL联表查询获得
        col = new DataColumn<DataRowView>();
        col.DataField = "CompanyName";
        col.HeaderText = "供货商";
        exp.Columns.Add(col);

        // 此列数据，演示通过自定义的数据处理程序，另外从数据库中查询
        // 此处仅作为演示，如果直接可以通过数据库联表查询，建议使用“供货商”一列的方式。
        // 自定义数据处理程序更大的用处见“库存”列的处理
        col = new DataColumn<DataRowView>();
        col.DataField = "CategoryID";
        col.HeaderText = "类别";
        col.DataFormater = GetCategory;
        exp.Columns.Add(col);

        col = new DataColumn<DataRowView>();
        col.DataField = "UnitPrice";
        col.HeaderText = "单价";
        col.DataFormatString = "${0:f2}";
        exp.Columns.Add(col);

        // 此列数据，演示通过自定义的数据处理程序，格式化数据显示
        // 简单地，如果仅将库存为 0 的记录显示为“缺货”，仍然可以通过SQL语句来完成。
        // 但这里主要演示SQL语句无法实现的部分，比如你需要结合当前数据行，根据其他的业务逻辑
        // 输出您想要的文本。此处示例仅表现为显示样式不同，您可以自由发挥。
        col = new DataColumn<DataRowView>();
        col.DataField = "UnitsInStock";
        col.HeaderText = "库存";
        col.DataFormater = GetUnitsInStock;
        exp.Columns.Add(col);
    }

    #region 自定义数据处理程序，可以自由发挥
    private string GetCategory(DataRowView r)
    {
        System.Data.Common.DbCommand cmd =
            Db.CreateCommand("SELECT TOP 1 CategoryName FROM Categories WHERE CategoryID = @cid");
        cmd.Parameters.Add(Db.CreateParameter("@cid", DbType.Int32, r["CategoryID"].ToString()));
        object cate = Db.ExecuteScalar(cmd);
        if (cate == null) return string.Empty;
        return cate.ToString();
    }

    private string GetUnitsInStock(DataRowView r)
    {
        int stock = 0, _tmp;
        if (int.TryParse(r["UnitsInStock"].ToString(), out _tmp))
        {
            stock = _tmp;
        }
        if (stock >= 50)
        {
            return string.Format("<span style=\"color:#00AA00\">{0}</span>", stock);
        }
        else if (stock > 0)
        {
            return stock.ToString();
        }
        else
        {
            return string.Format("<span style=\"color:#FF0000;"
            + "font-weight:bolder;\" title=\"缺货\">缺货</span>", stock);
        }
    }
    #endregion
    /*** 以上代码，根据实际需要配置您要导出的数据列，此处仅用于演示 ***/

    void buExplor1_Click(object sender, EventArgs e)
    {
        // 获取数据，这个操作您一定驾轻就熟。此处仅以DataSet为例。 
        DataSet data = Db.SelectData("SELECT ProductID AS 商品编号, ProductName AS 商品名称,"
             + " SupplierID AS 供应商, CategoryID AS 类别, QuantityPerUnit AS 单位数量,"
             + " UnitPrice AS 单价, UnitsInStock AS 库存, UnitsOnOrder AS 订购数,"
             + " ReorderLevel  FROM Products");

        // 一句话导出为Excel！
        exp.ExportToExcel(data);
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="zh-CN" />
    <meta name="Copyright" content="udnz.com,uonun" />
    <meta name="Keywords" content="uolib,uolib.dll,Excel,导入,导出,Debug,Function,C#,Csharp,.NET,asp.net,framework,RecordErrors,
    ErrorReporter,Debugger,ShowServerError,开源,开源项目,错误日志列表,函数库,数据库对象" />
    <meta name="Description" content="Excel导出利器。数据导出引擎演示，支持Excel导出，支持导出为网页，支持智能导出，支持自动绑定，
    支持手动指定数据列，支持自定义处理程序，支持文档标题、文档备注，支持自定义设置Excel表格的样式，一行代码即可导出。" />
    <style type="text/css" title="">
        body { color: #000; background-color: #FFF; width: 776px; margin: auto; }
        body, td, input, textarea { font-size: 12px; line-height: 150%; font-family: Arial, Helvetica, sans-serif; }
        .att { color: #F00; }
        a img { border: 0; }
        form{ margin: 0; padding: 0; }
        .note { color:#999;}
        .u { text-decoration:underline;}
        .code { font-family: Courier New; }
        h1 { line-height: 200%; font-family:微软雅黑}
        h2 { background-color:#aaffaa; padding:12px 20px;font-family:微软雅黑; font-weight:normal;}
        h3 { font-size:22px;font-family:微软雅黑; font-weight:normal;}
    </style>
    <title>数据导出引擎——Excel导出利器。支持智能导出，支持指定数据列、自定义处理程序、备注、标题、自定义样式设置</title>
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-2135848-6']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <h1>数据导出引擎<span style="font-size:18px;">——Excel导出利器，支持智能导出，支持样式设置，支持导出为网页</span></h1>
    <p>
        <a href="../../uoLib/">uoLib</a> 中有一个类：<a href="../../uolib/SDK/T_uoLib_Data_Export_ExportEngine%601.htm" target="_blank">uoLib.Data.Export.ExportEngine&lt;T&gt;</a>，专用于数据导出。
        <strong>它可以将数据对象导出到Excel中，数据源广泛支持DataTable、DataSet、List&lt;T&gt;、Collection&lt;T&gt;……</strong>，准确地说，只要实现了IListSource、IEnumerable
        或 IDataSource任意一个接口的对象，均可以通过此引擎导出到Excel中。</p>
        
        <h3>很好用？</h3>
        <ul>
        <li>广泛支持多种数据源，支持智能导出，自动绑定数据列，实现一行代码即可导出</li>
        <li>支持手动指定数据列</li>
        <li>支持自定义处理程序，让数据导出更智能</li>
        <li>支持文档标题、文档备注</li>
        <li>支持自定义设置Excel表格的样式</li>
        <li>支持.NET Framework 2.0 及其以上</li>
        <li>支持导出为Excel、网页</li>
        </ul>
        
        <h3>都可以绑定什么样的数据源？</h3>
        <ul>
        <li>DataSet</li>
        <li>DataTable</li>
        <li>List</li>
        <li>List&lt;T&gt; （T可以是任何自定义实例类）</li>
        <li>Collection</li>
        <li>Collection&lt;T&gt; （T可以是任何自定义实例类）</li>
        <li>……任何实现了IListSource、IEnumerable
        或 IDataSource中任意一个接口的对象！</li>
        <li>还不清楚？简言之，<span class="att">任何可用绑定到GridView的数据源都可以直接绑定导出</span>！</li>
        </ul>
    <p>
        下面具体拿SQL 2000 中的Northwind数据库为例（<span class="u">此引擎与数据源没有任何关系，此处只是用这个数据库演示！</span>），通过实际操作，让您了解该引擎能够实现的功能及其具体的实现方法。</p>
    <h2>示例一：仅指定数据源，不做任何配置</h2>
    <p>
        最简单的数据导出！您可以只指定数据源就可以一行代码导出为Excel表了。不做任何设置，此引擎将自动绑定数据源的所有列，无须人工干预。</p>
    
<pre class="code"><span style="color: blue">void </span>buExplor1_Click(<span style="color: blue">object </span>sender, <span style="color: #2b91af">EventArgs </span>e)
{
    <span style="color: green">// 获取数据，这个操作您一定驾轻就熟。此处仅以DataSet为例。 
    </span><span style="color: #2b91af">DataSet </span>data = Db.SelectData(<span style="color: #a31515">&quot;SELECT ProductID AS 商品编号, ProductName AS 商品名称,&quot;
         </span>+ <span style="color: #a31515">&quot; SupplierID AS 供应商, CategoryID AS 类别, QuantityPerUnit AS 单位数量,&quot;
         </span>+ <span style="color: #a31515">&quot; UnitPrice AS 单价, UnitsInStock AS 库存, UnitsOnOrder AS 订购数,&quot;
         </span>+ <span style="color: #a31515">&quot; ReorderLevel  FROM Products&quot;</span>);

    <span style="color: green">// 一句话导出为Excel！
    </span>exp.ExportToExcel(data);
}</pre>
    
    <div>
        <asp:Button runat="server" ID="buExplor1" Text="点击导出Excel" />
        <asp:Button runat="server" ID="Button1" Text="点击导出Htm" />
        <br />
    </div>
      <h2>示例二：指定数据列、表格标题、备注信息，使用默认样式</h2>
  <p>在此例中，用到了一些高级功能，比如格式化字符串、自定义数据处理程序等。
  更多详细配置可见：<a href="../../uolib/SDK/AllMembers_T_uoLib_Data_Export_DataColumn%601.htm" target="_blank">uoLib.Data.Export.DataColumn&lt;T&gt;</a></p>
  
<pre class="code">    <span style="color: gray">/// &lt;summary&gt;
    /// </span><span style="color: green">导出操作只需几个简单的步骤即可完成
    </span><span style="color: gray">/// &lt;/summary&gt;
    /// &lt;param name=&quot;sender&quot;&gt;&lt;/param&gt;
    /// &lt;param name=&quot;e&quot;&gt;&lt;/param&gt;
    </span><span style="color: blue">void </span>buExplor2_Click(<span style="color: blue">object </span>sender, <span style="color: #2b91af">EventArgs </span>e)
    {
        <span style="color: green">// 1.【可选】配置要导出的数据列
        </span>SetColumes();

        <span style="color: green">// 2.【可选】配置导出表的标题、备注
        </span>exp.Options.TitleText = <span style="color: #a31515">&quot;导出示例（指定数据列、标题、备注，使用默认样式）&quot;</span>;
        exp.Options.RemarkHtml = <span style="color: #a31515">&quot;（这里是备注）此示例指定了导出的数据列、&quot;
            </span>+ <span style="color: #a31515">&quot;表格标题、备注信息，使用了默认样式导出&quot;</span>;

        
        <span style="color: green">// 3.获取数据源
        </span><span style="color: #2b91af">DataSet </span>data = Db.SelectData(<span style="color: #a31515">&quot;SELECT Products.ProductID, Products.ProductName,&quot;
            </span>+ <span style="color: #a31515">&quot; Suppliers.CompanyName, Products.CategoryID, Products.UnitPrice,&quot;
            </span>+ <span style="color: #a31515">&quot; Products.UnitsInStock FROM Products INNER JOIN Suppliers ON&quot;
            </span>+ <span style="color: #a31515">&quot; Products.SupplierID = Suppliers.SupplierID;&quot;</span>);
        
        <span style="color: green">// 4.导出Excel！
        </span>exp.ExportToExcel(data);
    }

    <span style="color: green">/*** 以下代码，根据实际需要配置您要导出的数据列【可选】，此处仅用于演示 ***/
    
    </span><span style="color: gray">/// &lt;summary&gt;
    /// </span><span style="color: green">此方法中配置了若干个要导出的数据列
    </span><span style="color: gray">/// &lt;/summary&gt;
    </span><span style="color: blue">private void </span>SetColumes() {
        <span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt; col;

        <span style="color: green">// 此列演示了 DataFormatString 的使用
        </span>col = <span style="color: blue">new </span><span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt;();
        col.DataField = <span style="color: #a31515">&quot;ProductID&quot;</span>;
        col.HeaderText = <span style="color: #a31515">&quot;商品编号&quot;</span>;
        col.DataFormatString = <span style="color: #a31515">&quot;P{0:000000}&quot;</span>;
        exp.Columns.Add(col);

        col = <span style="color: blue">new </span><span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt;();
        col.DataField = <span style="color: #a31515">&quot;ProductName&quot;</span>;
        col.HeaderText = <span style="color: #a31515">&quot;商品名称&quot;</span>;
        exp.Columns.Add(col);

        <span style="color: green">// 此列数据，通过SQL联表查询获得
        </span>col = <span style="color: blue">new </span><span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt;();
        col.DataField = <span style="color: #a31515">&quot;CompanyName&quot;</span>;
        col.HeaderText = <span style="color: #a31515">&quot;供货商&quot;</span>;
        exp.Columns.Add(col);

        <span style="color: green">// 此列数据，演示通过自定义的数据处理程序，另外从数据库中查询
        // 此处仅作为演示，如果直接可以通过数据库联表查询，建议使用“供货商”一列的方式。
        // 自定义数据处理程序更大的用处见“库存”列的处理
        </span>col = <span style="color: blue">new </span><span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt;();
        col.DataField = <span style="color: #a31515">&quot;CategoryID&quot;</span>;
        col.HeaderText = <span style="color: #a31515">&quot;类别&quot;</span>;
        col.DataFormater = GetCategory;
        exp.Columns.Add(col);

        col = <span style="color: blue">new </span><span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt;();
        col.DataField = <span style="color: #a31515">&quot;UnitPrice&quot;</span>;
        col.HeaderText = <span style="color: #a31515">&quot;单价&quot;</span>;
        col.DataFormatString = <span style="color: #a31515">&quot;${0:f2}&quot;</span>;
        exp.Columns.Add(col);

        <span style="color: green">// 此列数据，演示通过自定义的数据处理程序，格式化数据显示
        // 简单地，如果仅将库存为 0 的记录显示为“缺货”，仍然可以通过SQL语句来完成。
        // 但这里主要演示SQL语句无法实现的部分，比如你需要结合当前数据行，根据其他的业务逻辑
        // 输出您想要的文本。此处示例仅表现为显示样式不同，您可以自由发挥。
        </span>col = <span style="color: blue">new </span><span style="color: #2b91af">DataColumn</span>&lt;<span style="color: #2b91af">DataRowView</span>&gt;();
        col.DataField = <span style="color: #a31515">&quot;UnitsInStock&quot;</span>;
        col.HeaderText = <span style="color: #a31515">&quot;库存&quot;</span>;
        col.DataFormater = GetUnitsInStock;
        exp.Columns.Add(col);
    }

    <span style="color: blue">#region </span>自定义数据处理程序，可以自由发挥
    <span style="color: blue">private string </span>GetCategory(<span style="color: #2b91af">DataRowView </span>r)
    {
        System.Data.Common.<span style="color: #2b91af">DbCommand </span>cmd =
            Db.CreateCommand(<span style="color: #a31515">&quot;SELECT TOP 1 CategoryName FROM Categories WHERE CategoryID = @cid&quot;</span>);
        cmd.Parameters.Add(Db.CreateParameter(<span style="color: #a31515">&quot;@cid&quot;</span>, <span style="color: #2b91af">DbType</span>.Int32, r[<span style="color: #a31515">&quot;CategoryID&quot;</span>].ToString()));
        <span style="color: blue">object </span>cate = Db.ExecuteScalar(cmd);
        <span style="color: blue">if </span>(cate == <span style="color: blue">null</span>) <span style="color: blue">return string</span>.Empty;
        <span style="color: blue">return </span>cate.ToString();
    }

    <span style="color: blue">private string </span>GetUnitsInStock(<span style="color: #2b91af">DataRowView </span>r)
    {
        <span style="color: blue">int </span>stock = 0, _tmp;
        <span style="color: blue">if </span>(<span style="color: blue">int</span>.TryParse(r[<span style="color: #a31515">&quot;UnitsInStock&quot;</span>].ToString(), <span style="color: blue">out </span>_tmp))
        {
            stock = _tmp;
        }
        <span style="color: blue">if </span>(stock &gt;= 50)
        {
            <span style="color: blue">return string</span>.Format(<span style="color: #a31515">&quot;&lt;span style=\&quot;color:#00AA00\&quot;&gt;{0}&lt;/span&gt;&quot;</span>, stock);
        }
        <span style="color: blue">else if </span>(stock &gt; 0)
        {
            <span style="color: blue">return </span>stock.ToString();
        }
        <span style="color: blue">else
        </span>{
            <span style="color: blue">return string</span>.Format(<span style="color: #a31515">&quot;&lt;span style=\&quot;color:#FF0000;&quot;
            </span>+ <span style="color: #a31515">&quot;font-weight:bolder;\&quot; title=\&quot;缺货\&quot;&gt;缺货&lt;/span&gt;&quot;</span>, stock);
        }
    }
    <span style="color: blue">#endregion
    </span><span style="color: green">/*** 以上代码，根据实际需要配置您要导出的数据列，此处仅用于演示 ***/
</span></pre>  
  
  <div>
        <asp:Button runat="server" ID="buExplor2" Text="点击导出Excel" />
        <asp:Button runat="server" ID="Button2" Text="点击导出Htm" />
    </div>
          <h2>示例三：自定义导出样式</h2>
  <p>在上例中，我们演示了如何自定义数据列，如何使用自定义处理程序等。同时，我们还可以自定义表格的样式，
  该导出引擎支持大量的样式设置，一下仅举部分样式进行演示。</p>

<h3>请设置样式：</h3><span class="note">* 支持任意可用字体、大小、颜色，不一一列出</span>
<table cellspacing="0" cellpadding="5" border="1" style=" border-collapse:collapse;">
	<tr>
		<td>标题文字</td>
		<td>
		<asp:DropDownList runat="server" ID="DropDownList0">
		<asp:ListItem Text="黑体"></asp:ListItem>
		<asp:ListItem Text="宋体"></asp:ListItem>
		<asp:ListItem Text="华文新魏"></asp:ListItem>
		<asp:ListItem Text="微软雅黑"></asp:ListItem>
		<asp:ListItem Text="方正姚体" Selected="True"></asp:ListItem>
		<asp:ListItem Text="幼圆"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList1">
		<asp:ListItem Text="9pt"></asp:ListItem>
		<asp:ListItem Text="12pt"></asp:ListItem>
		<asp:ListItem Text="18pt" Selected="True"></asp:ListItem>
		<asp:ListItem Text="smaller"></asp:ListItem>
		<asp:ListItem Text="x-large"></asp:ListItem>
		<asp:ListItem Text="xx-large"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList2">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" Selected="True" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		</td>
	</tr>
	<tr>
		<td>标题行</td>
		<td>
		背景：
		<asp:DropDownList runat="server" ID="DropDownList3">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="PeachPuff" Selected="True" style="background-color:PeachPuff"></asp:ListItem>
		</asp:DropDownList>
		高度：
		<asp:DropDownList runat="server" ID="DropDownList4">
		<asp:ListItem Text="50"></asp:ListItem>
		<asp:ListItem Text="80"></asp:ListItem>
		<asp:ListItem Text="100" Selected="True"></asp:ListItem>
		<asp:ListItem Text="120"></asp:ListItem>
		</asp:DropDownList>px
    </td>
	</tr>
	<tr>
		<td>列标题字体</td>
		<td>
		<asp:DropDownList runat="server" ID="DropDownList5">
		<asp:ListItem Text="黑体"></asp:ListItem>
		<asp:ListItem Text="宋体"></asp:ListItem>
		<asp:ListItem Text="华文仿宋" Selected="True"></asp:ListItem>
		<asp:ListItem Text="微软雅黑"></asp:ListItem>
		<asp:ListItem Text="仿宋"></asp:ListItem>
		<asp:ListItem Text="幼圆"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList6">
		<asp:ListItem Text="9pt"></asp:ListItem>
		<asp:ListItem Text="12pt" Selected="True"></asp:ListItem>
		<asp:ListItem Text="18pt"></asp:ListItem>
		<asp:ListItem Text="smaller"></asp:ListItem>
		<asp:ListItem Text="x-large"></asp:ListItem>
		<asp:ListItem Text="xx-large"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList7">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" Selected="True" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		</td>
	</tr>
	<tr>
		<td>列标题行</td>
		<td>
		背景：
		<asp:DropDownList runat="server" ID="DropDownList8">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" Selected="True" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		高度：
		<asp:DropDownList runat="server" ID="DropDownList9">
		<asp:ListItem Text="30" Selected="True"></asp:ListItem>
		<asp:ListItem Text="50"></asp:ListItem>
		<asp:ListItem Text="80"></asp:ListItem>
		<asp:ListItem Text="100" ></asp:ListItem>
		<asp:ListItem Text="120"></asp:ListItem>
		</asp:DropDownList>px
		</td>
	</tr>
	<tr>
		<td>正文行字体</td>
		<td>
		<asp:DropDownList runat="server" ID="DropDownList10">
		<asp:ListItem Text="黑体"></asp:ListItem>
		<asp:ListItem Text="宋体" Selected="True"></asp:ListItem>
		<asp:ListItem Text="华文新魏"></asp:ListItem>
		<asp:ListItem Text="微软雅黑"></asp:ListItem>
		<asp:ListItem Text="仿宋"></asp:ListItem>
		<asp:ListItem Text="幼圆"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList11">
		<asp:ListItem Text="9pt" Selected="True"></asp:ListItem>
		<asp:ListItem Text="12pt"></asp:ListItem>
		<asp:ListItem Text="18pt"></asp:ListItem>
		<asp:ListItem Text="smaller"></asp:ListItem>
		<asp:ListItem Text="x-large"></asp:ListItem>
		<asp:ListItem Text="xx-large"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList12">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" Selected="True" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		</td>
	</tr>
	<tr>
		<td>正文行</td>
		<td>
		背景：
		<asp:DropDownList runat="server" ID="DropDownList13">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" Selected="True" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		高度：
		<asp:DropDownList runat="server" ID="DropDownList14">
		<asp:ListItem Text="20" Selected="True"></asp:ListItem>
		<asp:ListItem Text="80"></asp:ListItem>
		<asp:ListItem Text="100"></asp:ListItem>
		<asp:ListItem Text="120"></asp:ListItem>
		</asp:DropDownList>px
		</td>
	</tr>
	<tr>
		<td>正文交替行字体</td>
		<td>
		<asp:DropDownList runat="server" ID="DropDownList15">
		<asp:ListItem Text="黑体"></asp:ListItem>
		<asp:ListItem Text="Lucida Fax" Selected="True"></asp:ListItem>
		<asp:ListItem Text="华文新魏"></asp:ListItem>
		<asp:ListItem Text="微软雅黑"></asp:ListItem>
		<asp:ListItem Text="仿宋"></asp:ListItem>
		<asp:ListItem Text="幼圆"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList16">
		<asp:ListItem Text="9pt" Selected="True"></asp:ListItem>
		<asp:ListItem Text="12pt"></asp:ListItem>
		<asp:ListItem Text="18pt"></asp:ListItem>
		<asp:ListItem Text="smaller"></asp:ListItem>
		<asp:ListItem Text="x-large"></asp:ListItem>
		<asp:ListItem Text="xx-large"></asp:ListItem>
		</asp:DropDownList>
		<asp:DropDownList runat="server" ID="DropDownList17">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="Maroon" Selected="True" style="background-color:Maroon"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="DarkBlue" style="background-color:DarkBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		</td>
	</tr>
	<tr>
		<td>正文交替行</td>
		<td>
		背景：
		<asp:DropDownList runat="server" ID="DropDownList18">
		<asp:ListItem Text="Black" style="background-color:Black"></asp:ListItem>
		<asp:ListItem Text="White" style="background-color:White"></asp:ListItem>
		<asp:ListItem Text="Red" style="background-color:Red"></asp:ListItem>
		<asp:ListItem Text="LightBlue" Selected="True" style="background-color:LightBlue"></asp:ListItem>
		<asp:ListItem Text="Gray" style="background-color:Gray"></asp:ListItem>
		<asp:ListItem Text="Orange" style="background-color:Orange"></asp:ListItem>
		</asp:DropDownList>
		高度：
		<asp:DropDownList runat="server" ID="DropDownList19">
		<asp:ListItem Text="20" Selected="True"></asp:ListItem>
		<asp:ListItem Text="80"></asp:ListItem>
		<asp:ListItem Text="100"></asp:ListItem>
		<asp:ListItem Text="120"></asp:ListItem>
		</asp:DropDownList>px
		</td>
	</tr>
	<tr>
	<td>备注行</td>
	<td><span class="note">* 备注支持HTML标签，可任意发挥</span></td>
	</tr>
</table>
     <div>
     <br />
         <asp:Button runat="server" ID="buExplor3" Text="点击导出Excel" />
        <asp:Button runat="server" ID="Button3" Text="点击导出Htm" />
        <br /><br />
     更多详细配置可见：<a href="../../uolib/SDK/AllMembers_T_uoLib_Data_Export_Options.htm" target="_blank">uoLib.Data.Export.Options</a>
    </div>

    <h2>Q & A</h2>
    <p>Q: <strong>我在哪里可以下载到这个数据导出引擎？</strong><br />
    A: 这个模块在 uoLib 中，请见：<a href="http://work.udnz.com/uoLib/" target="_blank">http://work.udnz.com/uoLib/</a>
    </p>
    <p>Q: <strong>它支持哪个.NET版本？</strong><br />
    A: 支持.NET Framework 2.0 及其以上。（目前仅用于Webform）
    </p>

    <h2>结束语</h2>
    <p>感谢使用。如果您有任何意见或建议，欢迎<a href="../../Talk.aspx" target="_blank">反馈给我</a>。</p>
 <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
    </form>
</body>
</html>
