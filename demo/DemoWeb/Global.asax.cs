﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using uoLib.Web.Debugger;

namespace DemoWeb
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Configuration.LogType = LogType.Html;
            Configuration.LogPath = "~/Errors/";
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (HttpContext.Current.AllErrors.Length > 0)
            {
                foreach (Exception ex in HttpContext.Current.AllErrors)
                {
                    //uoLib.Web.Debugger.ErrorReporter.ShowServerError(ex, true);
                    uoLib.Web.Debugger.ErrorReporter.RecordErrors(ex);
                }
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                //HttpContext.Current.ApplicationInstance.Dispose();
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}