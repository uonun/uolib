﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ControlTest.aspx.cs" Inherits="DemoWeb.ControlTest" %>

<%@ Register Assembly="uoLib" Namespace="uoLib.Web.Controls" TagPrefix="uo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        pre { background-color: #FFFFAA; font-family: Courier New; font-size: 12px; padding: 10px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    静态加载：<br />
    <pre>
<uo:Loader runat="server" ID="loader1" Src="~/ControlTest.aspx" HtmlEncode="True">
</uo:Loader></pre>
    动态加载：<br />
    <pre>
<uo:Loader runat="server" ID="loader2" Src="~/" LoadMethod="DynamicExecutive" Encoding="UTF8"
        HtmlEncode="True"></uo:Loader>
</pre>
    </form>
</body>
</html>
