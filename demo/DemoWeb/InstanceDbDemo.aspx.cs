﻿using System;
using System.Data;
using System.Data.Common;
using uoLib.Data.Singleton;

namespace DemoWeb
{
    public partial class Demo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // 使用 SqlClient 链接字符串的 Sql Server 数据库
            // 2000/2005/2008均可。
            // 链接字符串配置名称：uoLib.SqlClientSqlServer
            SqlClientSqlServer SQL = SqlClientSqlServer.Instance;
            const string TABLENAME = "Products";
            DbCommand cmd = SQL.CreateCommand("SELECT TOP 1 ID FROM {0}", TABLENAME);
            DataSet ds = SQL.SelectData(cmd);
            if (ds == null)
                Response.Write("数据库为空！");
            else
                Response.Write(string.Format("共 {0} 行记录。", ds.Tables[0].Rows.Count));

            // 使用 OleDb 链接字符串的 Access 数据库
            // 97/2000/2007均可。
            // 链接字符串配置名称：uoLib.OleDbAccess
            OleDbAccess Access = OleDbAccess.Instance;
            Access.CreateConnection(
                OleDbAccess.CreateExampleConnectionString(
                    Server.MapPath("../files/Database/access.mdb"), "uonun", "udnz.com")
                );
            Response.Write(Access.ConnectionString);

            // 使用 Odbc 链接字符串的 SQLite 数据库。
            // 链接字符串配置名称：uoLib.OdbcSQLite
            OdbcSQLite SQLite = OdbcSQLite.Instance;
            DbConnection conn = SQLite.CreateConnection();
            conn.Open();
            //do something..
            conn.Close();
        }
    }
}
