﻿using System;
using System.Data;
using System.Data.Common;
using uoLib.Data;

namespace DemoWeb
{
    public partial class Demo2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //声明数据库实例
            Database db = new Database(ConnectionType.SqlClient);
            //初始化数据库连接
            db.CreateConnection(
                Database.CreateExampleConnectionString(
                    "127.0.0.1", "NorthWind", "sa", "",
                    ConnectionType.SqlClient)
            );
            Response.Write(db.ConnectionString);

            //示例：执行 DbCommand 
            //可执行包括SELECT/INSERT/UPDATE/DELETE、存储过程等任何DbCommand命令
            int _id = 10;
            DbCommand cmd = db.CreateCommand("DELETE FROM [TABLE] WHERE ID=@id");
            cmd.Parameters.Add(db.CreateParameter("@id", System.Data.DbType.Int32,_id));
            db.ExecuteNonQuery(cmd);

            //示例：使用存储过程、查询数据库
            cmd = db.CreateCommand("myProcedure");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(db.CreateParameter("@id", System.Data.DbType.Int32, _id));

            DataSet ds = db.SelectData(cmd);
            if (ds == null)
            {
                Response.Write("数据库为空！");
            }
            else { 
                //do something...
            }
        }
    }
}
