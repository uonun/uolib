﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using uoLib.Data;
using uoLib;
using System.Collections.Specialized;

namespace DemoWeb
{
    public partial class _Default : System.Web.UI.Page
    {
        private string connStr = ConfigurationManager.ConnectionStrings["demodb"].ConnectionString;
        private Database db = new uoLib.Data.Database(ConnectionType.OleDb);
        private DbConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(Request.Url);

            Conn = db.CreateConnection(connStr);
            showInfo();
            ConnectDB();
            ShowPager();
            showServerVariables();

            if (Session["ValidateCode"] != null)
                code.Text = Session["ValidateCode"].ToString();
        }
        private void showInfo()
        {
            AttachTxt("<h2>当前程序库信息</h2>");
            AttachTxt(Info.Name + ": " + Info.Version);
            AttachTxt("<br />");
            AttachTxt("AdminEmail: " + Info.AdminEmail);
            AttachTxt("<br />");
            AttachTxt("HelpLink: " + Info.HelpLink);
            AttachTxt("<br />");
            AttachTxt("Path: " + Info.Path);
            AttachTxt("<br />");
            AttachTxt("CodeBase: " + Info.CodeBase);
        }
        private void showServerVariables()
        {
            AttachTxt("<h2>ServerVariables</h2>");
            AttachTxt("<br />");

            int loop1, loop2;
            NameValueCollection coll;
            // Load ServerVariable collection into NameValueCollection object.
            coll = Request.ServerVariables;
            // Get names of all keys into a string array. 
            String[] arr1 = coll.AllKeys;
            for (loop1 = 0;loop1 < arr1.Length;loop1++)
            {
                AttachTxt("Key: " + arr1[loop1] + "<br>");
                String[] arr2 = coll.GetValues(arr1[loop1]);
                for (loop2 = 0;loop2 < arr2.Length;loop2++)
                {
                    AttachTxt("Value " + loop2 + ": " + Server.HtmlEncode(arr2[loop2]) + "<br>");
                }
            }

        }

        private void ConnectDB()
        {
            AttachTxt("<h2>连接数据库</h2>");
            AttachTxt("连接字符串：" + connStr + "<br />");


            Conn.ConnectionString = connStr;
            AttachTxt("打开数据库连接：");
            try
            {
                Conn.Open();
                AttachTxt("成功！<br />");

                AttachTxt("关闭数据库连接：");
                Conn.Close();
                AttachTxt(Conn.State.ToString());
            }
            catch (Exception e)
            {
                AttachTxt(string.Format("<span class=\"att\">失败！{0}<span><br />", e.Message));
            }


        }

        private void ShowPager()
        {

            AttachTxt("<h2>显示分页数据</h2>");

            //PageCute p = new PageCute(db);
            //p.SqlCommand = db.CreateCommand();
            //p.SqlCommand.CommandText = "select content_id,content_Title from BlogContent order by content_id desc";
            //p.SqlCommand.Connection = Conn;
            //p.PageSize = 5;

            //GridView1.DataSource = p.ReturnPagedData();
            //GridView1.DataBind();

            //AttachTxt(p.GetPagerString(true,true,true));
        }


        private void AttachTxt(string t, params string[] str)
        {
            Txt.Text += string.Format(t, str);
        }
    }
}


